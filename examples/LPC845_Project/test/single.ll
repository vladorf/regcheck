; ModuleID = 'llvm-link'
source_filename = "llvm-link"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.SYSCON_Type = type { i32, [4 x i8], i32, i32, [16 x i8], i32, i32, i32, [4 x i8], i32, [4 x i8], i32, [4 x i8], i32, i32, i32, i32, i32, i32, i32, [4 x i8], i32, i32, i32, i32, i32, i32, [8 x i8], i32, i32, i32, i32, [11 x i32], [20 x i8], [2 x %struct.anon], i32, i32, [4 x i8], i32, [2 x i32], [44 x i8], i32, i32, i32, i32, i32, i32, i32, i32, i32, [24 x i8], i32, i32, [8 x i32], [108 x i8], i32, [12 x i8], i32, [24 x i8], i32, i32, i32, [444 x i8], i32 }
%struct.anon = type { i32, i32, i32, [4 x i8] }
%struct._clock_sys_pll = type { i32, i32 }
%struct.IOCON_Type = type { [56 x i32] }
%struct.SWM_Type = type { %union.anon, [388 x i8], i32, i32 }
%union.anon = type { %struct.anon.0 }
%struct.anon.0 = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32 }

@g_Ext_Clk_Freq = dso_local global i32 0, align 4
@g_Wdt_Osc_Freq = dso_local global i32 0, align 4
@.str.5 = private unnamed_addr constant [5 x i8] c"freq\00", align 1
@.str.1 = private unnamed_addr constant [12 x i8] c"fsl_clock.c\00", align 1
@__PRETTY_FUNCTION__.CLOCK_SetFRGClkFreq = private unnamed_addr constant [48 x i8] c"_Bool CLOCK_SetFRGClkFreq(uint32_t *, uint32_t)\00", align 1
@.str = private unnamed_addr constant [48 x i8] c"config->targetFreq <= SYSPLL_MAX_OUTPUT_FREQ_HZ\00", align 1
@__PRETTY_FUNCTION__.CLOCK_InitSystemPll = private unnamed_addr constant [50 x i8] c"void CLOCK_InitSystemPll(const clock_sys_pll_t *)\00", align 1
@.str.2 = private unnamed_addr constant [16 x i8] c"inputFreq != 0U\00", align 1
@.str.6 = private unnamed_addr constant [5 x i8] c"base\00", align 1
@__PRETTY_FUNCTION__.CLOCK_UpdateClkSrc = private unnamed_addr constant [55 x i8] c"void CLOCK_UpdateClkSrc(volatile uint32_t *, uint32_t)\00", align 1
@.str.3 = private unnamed_addr constant [16 x i8] c"wdtOscDiv >= 2U\00", align 1
@__PRETTY_FUNCTION__.CLOCK_InitWdtOsc = private unnamed_addr constant [57 x i8] c"void CLOCK_InitWdtOsc(clock_wdt_analog_freq_t, uint32_t)\00", align 1
@.str.4 = private unnamed_addr constant [15 x i8] c"0U != delay_us\00", align 1
@__PRETTY_FUNCTION__.SDK_DelayAtLeastUs = private unnamed_addr constant [34 x i8] c"void SDK_DelayAtLeastUs(uint32_t)\00", align 1
@SystemCoreClock = external global i32, align 4
@.str.7 = private unnamed_addr constant [12 x i8] c"value != 0U\00", align 1
@.str.1.8 = private unnamed_addr constant [14 x i8] c"./fsl_clock.h\00", align 1
@__PRETTY_FUNCTION__.CLOCK_SetCoreSysClkDiv = private unnamed_addr constant [38 x i8] c"void CLOCK_SetCoreSysClkDiv(uint32_t)\00", align 1

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local zeroext i1 @CLOCK_SetFRG0ClkFreq(i32) #0 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load i32, i32* %2, align 4
  %4 = call zeroext i1 @CLOCK_SetFRGClkFreq(i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 34, i64 0, i32 0), i32 %3)
  ret i1 %4
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal zeroext i1 @CLOCK_SetFRGClkFreq(i32*, i32) #0 {
  %3 = alloca i1, align 1
  %4 = alloca i32*, align 8
  %5 = alloca i32, align 4
  %6 = alloca i32, align 4
  %7 = alloca i32, align 4
  store i32* %0, i32** %4, align 8
  store i32 %1, i32* %5, align 4
  %8 = load i32, i32* %5, align 4
  %9 = icmp ne i32 %8, 0
  br i1 %9, label %10, label %11

10:                                               ; preds = %2
  br label %12

11:                                               ; preds = %2
  call void @__assert_fail(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.5, i64 0, i64 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0), i32 81, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @__PRETTY_FUNCTION__.CLOCK_SetFRGClkFreq, i64 0, i64 0)) #2
  unreachable

12:                                               ; preds = %10
  %13 = load i32*, i32** %4, align 8
  %14 = call i32 @CLOCK_GetFRGInputClkFreq(i32* %13)
  store i32 %14, i32* %6, align 4
  %15 = load i32, i32* %5, align 4
  %16 = load i32, i32* %6, align 4
  %17 = icmp ugt i32 %15, %16
  br i1 %17, label %23, label %18

18:                                               ; preds = %12
  %19 = load i32, i32* %6, align 4
  %20 = load i32, i32* %5, align 4
  %21 = udiv i32 %19, %20
  %22 = icmp uge i32 %21, 2
  br i1 %22, label %23, label %24

23:                                               ; preds = %18, %12
  store i1 false, i1* %3, align 1
  br label %43

24:                                               ; preds = %18
  %25 = load i32, i32* %6, align 4
  %26 = load i32, i32* %5, align 4
  %27 = sub i32 %25, %26
  %28 = zext i32 %27 to i64
  %29 = shl i64 %28, 8
  %30 = load i32, i32* %5, align 4
  %31 = zext i32 %30 to i64
  %32 = udiv i64 %29, %31
  %33 = trunc i64 %32 to i32
  store i32 %33, i32* %7, align 4
  %34 = load i32*, i32** %4, align 8
  store i32 255, i32* %34, align 4
  %35 = load i32, i32* %7, align 4
  %36 = shl i32 %35, 0
  %37 = and i32 %36, 255
  %38 = load i32*, i32** %4, align 8
  %39 = ptrtoint i32* %38 to i32
  %40 = add i32 %39, 4
  %41 = zext i32 %40 to i64
  %42 = inttoptr i64 %41 to i32*
  store i32 %37, i32* %42, align 4
  store i1 true, i1* %3, align 1
  br label %43

43:                                               ; preds = %24, %23
  %44 = load i1, i1* %3, align 1
  ret i1 %44
}

; Function Attrs: noreturn nounwind
declare void @__assert_fail(i8*, i8*, i32, i8*) #1

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal i32 @CLOCK_GetFRGInputClkFreq(i32*) #0 {
  %2 = alloca i32, align 4
  %3 = alloca i32*, align 8
  %4 = alloca i32, align 4
  store i32* %0, i32** %3, align 8
  %5 = load i32*, i32** %3, align 8
  %6 = ptrtoint i32* %5 to i32
  %7 = add i32 %6, 8
  %8 = zext i32 %7 to i64
  %9 = inttoptr i64 %8 to i32*
  %10 = load i32, i32* %9, align 4
  %11 = and i32 %10, 3
  store i32 %11, i32* %4, align 4
  %12 = load i32, i32* %4, align 4
  %13 = icmp eq i32 %12, 0
  br i1 %13, label %14, label %16

14:                                               ; preds = %1
  %15 = call i32 @CLOCK_GetFroFreq()
  store i32 %15, i32* %2, align 4
  br label %23

16:                                               ; preds = %1
  %17 = load i32, i32* %4, align 4
  %18 = icmp eq i32 %17, 1
  br i1 %18, label %19, label %21

19:                                               ; preds = %16
  %20 = call i32 @CLOCK_GetMainClkFreq()
  store i32 %20, i32* %2, align 4
  br label %23

21:                                               ; preds = %16
  %22 = call i32 @CLOCK_GetSystemPLLFreq()
  store i32 %22, i32* %2, align 4
  br label %23

23:                                               ; preds = %21, %19, %14
  %24 = load i32, i32* %2, align 4
  ret i32 %24
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local i32 @CLOCK_GetFroFreq() #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 7), align 4
  %4 = and i32 %3, 3
  store i32 %4, i32* %1, align 4
  store i32 0, i32* %2, align 4
  %5 = load i32, i32* %1, align 4
  %6 = icmp eq i32 %5, 0
  br i1 %6, label %7, label %8

7:                                                ; preds = %0
  store i32 18000000, i32* %2, align 4
  br label %14

8:                                                ; preds = %0
  %9 = load i32, i32* %1, align 4
  %10 = icmp eq i32 %9, 1
  br i1 %10, label %11, label %12

11:                                               ; preds = %8
  store i32 24000000, i32* %2, align 4
  br label %13

12:                                               ; preds = %8
  store i32 30000000, i32* %2, align 4
  br label %13

13:                                               ; preds = %12, %11
  br label %14

14:                                               ; preds = %13, %7
  %15 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 7), align 4
  %16 = and i32 %15, 131072
  %17 = lshr i32 %16, 17
  %18 = icmp eq i32 %17, 0
  br i1 %18, label %19, label %27

19:                                               ; preds = %14
  %20 = load volatile i32, i32* inttoptr (i64 1342242816 to i32*), align 4
  %21 = and i32 %20, 2
  %22 = icmp ne i32 %21, 0
  %23 = zext i1 %22 to i64
  %24 = select i1 %22, i32 16, i32 2
  %25 = load i32, i32* %2, align 4
  %26 = udiv i32 %25, %24
  store i32 %26, i32* %2, align 4
  br label %27

27:                                               ; preds = %19, %14
  %28 = load i32, i32* %2, align 4
  ret i32 %28
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local i32 @CLOCK_GetMainClkFreq() #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  store i32 0, i32* %2, align 4
  %3 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 15), align 4
  %4 = and i32 %3, 3
  %5 = icmp eq i32 %4, 1
  br i1 %5, label %6, label %8

6:                                                ; preds = %0
  %7 = call i32 @CLOCK_GetSystemPLLFreq()
  store i32 %7, i32* %1, align 4
  br label %22

8:                                                ; preds = %0
  %9 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 17), align 4
  switch i32 %9, label %19 [
    i32 0, label %10
    i32 1, label %12
    i32 2, label %14
    i32 3, label %16
  ]

10:                                               ; preds = %8
  %11 = call i32 @CLOCK_GetFroFreq()
  store i32 %11, i32* %2, align 4
  br label %20

12:                                               ; preds = %8
  %13 = call i32 @CLOCK_GetExtClkFreq()
  store i32 %13, i32* %2, align 4
  br label %20

14:                                               ; preds = %8
  %15 = call i32 @CLOCK_GetWdtOscFreq()
  store i32 %15, i32* %2, align 4
  br label %20

16:                                               ; preds = %8
  %17 = call i32 @CLOCK_GetFroFreq()
  %18 = lshr i32 %17, 1
  store i32 %18, i32* %2, align 4
  br label %20

19:                                               ; preds = %8
  br label %20

20:                                               ; preds = %19, %16, %14, %12, %10
  %21 = load i32, i32* %2, align 4
  store i32 %21, i32* %1, align 4
  br label %22

22:                                               ; preds = %20, %6
  %23 = load i32, i32* %1, align 4
  ret i32 %23
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal i32 @CLOCK_GetSystemPLLFreq() #0 {
  %1 = call i32 @CLOCK_GetSystemPLLInClockRate()
  %2 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 2), align 4
  %3 = and i32 %2, 31
  %4 = add i32 %3, 1
  %5 = mul i32 %1, %4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local i32 @CLOCK_GetSystemPLLInClockRate() #0 {
  %1 = alloca i32, align 4
  store i32 0, i32* %1, align 4
  %2 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 13), align 4
  switch i32 %2, label %12 [
    i32 3, label %3
    i32 0, label %6
    i32 1, label %8
    i32 2, label %10
  ]

3:                                                ; preds = %0
  %4 = call i32 @CLOCK_GetFroFreq()
  %5 = lshr i32 %4, 1
  store i32 %5, i32* %1, align 4
  br label %13

6:                                                ; preds = %0
  %7 = call i32 @CLOCK_GetFroFreq()
  store i32 %7, i32* %1, align 4
  br label %13

8:                                                ; preds = %0
  %9 = call i32 @CLOCK_GetExtClkFreq()
  store i32 %9, i32* %1, align 4
  br label %13

10:                                               ; preds = %0
  %11 = call i32 @CLOCK_GetWdtOscFreq()
  store i32 %11, i32* %1, align 4
  br label %13

12:                                               ; preds = %0
  br label %13

13:                                               ; preds = %12, %10, %8, %6, %3
  %14 = load i32, i32* %1, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal i32 @CLOCK_GetExtClkFreq() #0 {
  %1 = load volatile i32, i32* @g_Ext_Clk_Freq, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal i32 @CLOCK_GetWdtOscFreq() #0 {
  %1 = load volatile i32, i32* @g_Wdt_Osc_Freq, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local zeroext i1 @CLOCK_SetFRG1ClkFreq(i32) #0 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load i32, i32* %2, align 4
  %4 = call zeroext i1 @CLOCK_SetFRGClkFreq(i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 34, i64 1, i32 0), i32 %3)
  ret i1 %4
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local i32 @CLOCK_GetFRG0ClkFreq() #0 {
  %1 = call i32 @CLOCK_GetFRGInputClkFreq(i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 34, i64 0, i32 0))
  %2 = shl i32 %1, 8
  %3 = zext i32 %2 to i64
  %4 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 34, i64 0, i32 1), align 4
  %5 = and i32 %4, 255
  %6 = add i32 %5, 256
  %7 = zext i32 %6 to i64
  %8 = udiv i64 %3, %7
  %9 = trunc i64 %8 to i32
  ret i32 %9
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local i32 @CLOCK_GetFRG1ClkFreq() #0 {
  %1 = call i32 @CLOCK_GetFRGInputClkFreq(i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 34, i64 1, i32 0))
  %2 = shl i32 %1, 8
  %3 = zext i32 %2 to i64
  %4 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 34, i64 1, i32 1), align 4
  %5 = and i32 %4, 255
  %6 = add i32 %5, 256
  %7 = zext i32 %6 to i64
  %8 = udiv i64 %3, %7
  %9 = trunc i64 %8 to i32
  ret i32 %9
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local i32 @CLOCK_GetClockOutClkFreq() #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 36), align 4
  %4 = and i32 %3, 255
  store i32 %4, i32* %1, align 4
  store i32 0, i32* %2, align 4
  %5 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 35), align 4
  switch i32 %5, label %16 [
    i32 0, label %6
    i32 1, label %8
    i32 2, label %10
    i32 3, label %12
    i32 4, label %14
  ]

6:                                                ; preds = %0
  %7 = call i32 @CLOCK_GetFroFreq()
  store i32 %7, i32* %2, align 4
  br label %17

8:                                                ; preds = %0
  %9 = call i32 @CLOCK_GetMainClkFreq()
  store i32 %9, i32* %2, align 4
  br label %17

10:                                               ; preds = %0
  %11 = call i32 @CLOCK_GetSystemPLLFreq()
  store i32 %11, i32* %2, align 4
  br label %17

12:                                               ; preds = %0
  %13 = call i32 @CLOCK_GetExtClkFreq()
  store i32 %13, i32* %2, align 4
  br label %17

14:                                               ; preds = %0
  %15 = call i32 @CLOCK_GetWdtOscFreq()
  store i32 %15, i32* %2, align 4
  br label %17

16:                                               ; preds = %0
  br label %17

17:                                               ; preds = %16, %14, %12, %10, %8, %6
  %18 = load i32, i32* %1, align 4
  %19 = icmp eq i32 %18, 0
  br i1 %19, label %20, label %21

20:                                               ; preds = %17
  br label %25

21:                                               ; preds = %17
  %22 = load i32, i32* %2, align 4
  %23 = load i32, i32* %1, align 4
  %24 = udiv i32 %22, %23
  br label %25

25:                                               ; preds = %21, %20
  %26 = phi i32 [ 0, %20 ], [ %24, %21 ]
  ret i32 %26
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local i32 @CLOCK_GetUart0ClkFreq() #0 {
  %1 = alloca i32, align 4
  store i32 0, i32* %1, align 4
  %2 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 32, i64 0), align 4
  switch i32 %2, label %14 [
    i32 0, label %3
    i32 1, label %5
    i32 2, label %7
    i32 3, label %9
    i32 4, label %11
  ]

3:                                                ; preds = %0
  %4 = call i32 @CLOCK_GetFroFreq()
  store i32 %4, i32* %1, align 4
  br label %15

5:                                                ; preds = %0
  %6 = call i32 @CLOCK_GetMainClkFreq()
  store i32 %6, i32* %1, align 4
  br label %15

7:                                                ; preds = %0
  %8 = call i32 @CLOCK_GetFRG0ClkFreq()
  store i32 %8, i32* %1, align 4
  br label %15

9:                                                ; preds = %0
  %10 = call i32 @CLOCK_GetFRG1ClkFreq()
  store i32 %10, i32* %1, align 4
  br label %15

11:                                               ; preds = %0
  %12 = call i32 @CLOCK_GetFroFreq()
  %13 = lshr i32 %12, 1
  store i32 %13, i32* %1, align 4
  br label %15

14:                                               ; preds = %0
  br label %15

15:                                               ; preds = %14, %11, %9, %7, %5, %3
  %16 = load i32, i32* %1, align 4
  ret i32 %16
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local i32 @CLOCK_GetUart1ClkFreq() #0 {
  %1 = alloca i32, align 4
  store i32 0, i32* %1, align 4
  %2 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 32, i64 1), align 4
  switch i32 %2, label %14 [
    i32 0, label %3
    i32 1, label %5
    i32 2, label %7
    i32 3, label %9
    i32 4, label %11
  ]

3:                                                ; preds = %0
  %4 = call i32 @CLOCK_GetFroFreq()
  store i32 %4, i32* %1, align 4
  br label %15

5:                                                ; preds = %0
  %6 = call i32 @CLOCK_GetMainClkFreq()
  store i32 %6, i32* %1, align 4
  br label %15

7:                                                ; preds = %0
  %8 = call i32 @CLOCK_GetFRG0ClkFreq()
  store i32 %8, i32* %1, align 4
  br label %15

9:                                                ; preds = %0
  %10 = call i32 @CLOCK_GetFRG1ClkFreq()
  store i32 %10, i32* %1, align 4
  br label %15

11:                                               ; preds = %0
  %12 = call i32 @CLOCK_GetFroFreq()
  %13 = lshr i32 %12, 1
  store i32 %13, i32* %1, align 4
  br label %15

14:                                               ; preds = %0
  br label %15

15:                                               ; preds = %14, %11, %9, %7, %5, %3
  %16 = load i32, i32* %1, align 4
  ret i32 %16
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local i32 @CLOCK_GetUart2ClkFreq() #0 {
  %1 = alloca i32, align 4
  store i32 0, i32* %1, align 4
  %2 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 32, i64 2), align 4
  switch i32 %2, label %14 [
    i32 0, label %3
    i32 1, label %5
    i32 2, label %7
    i32 3, label %9
    i32 4, label %11
  ]

3:                                                ; preds = %0
  %4 = call i32 @CLOCK_GetFroFreq()
  store i32 %4, i32* %1, align 4
  br label %15

5:                                                ; preds = %0
  %6 = call i32 @CLOCK_GetMainClkFreq()
  store i32 %6, i32* %1, align 4
  br label %15

7:                                                ; preds = %0
  %8 = call i32 @CLOCK_GetFRG0ClkFreq()
  store i32 %8, i32* %1, align 4
  br label %15

9:                                                ; preds = %0
  %10 = call i32 @CLOCK_GetFRG1ClkFreq()
  store i32 %10, i32* %1, align 4
  br label %15

11:                                               ; preds = %0
  %12 = call i32 @CLOCK_GetFroFreq()
  %13 = lshr i32 %12, 1
  store i32 %13, i32* %1, align 4
  br label %15

14:                                               ; preds = %0
  br label %15

15:                                               ; preds = %14, %11, %9, %7, %5, %3
  %16 = load i32, i32* %1, align 4
  ret i32 %16
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local i32 @CLOCK_GetUart3ClkFreq() #0 {
  %1 = alloca i32, align 4
  store i32 0, i32* %1, align 4
  %2 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 32, i64 3), align 4
  switch i32 %2, label %14 [
    i32 0, label %3
    i32 1, label %5
    i32 2, label %7
    i32 3, label %9
    i32 4, label %11
  ]

3:                                                ; preds = %0
  %4 = call i32 @CLOCK_GetFroFreq()
  store i32 %4, i32* %1, align 4
  br label %15

5:                                                ; preds = %0
  %6 = call i32 @CLOCK_GetMainClkFreq()
  store i32 %6, i32* %1, align 4
  br label %15

7:                                                ; preds = %0
  %8 = call i32 @CLOCK_GetFRG0ClkFreq()
  store i32 %8, i32* %1, align 4
  br label %15

9:                                                ; preds = %0
  %10 = call i32 @CLOCK_GetFRG1ClkFreq()
  store i32 %10, i32* %1, align 4
  br label %15

11:                                               ; preds = %0
  %12 = call i32 @CLOCK_GetFroFreq()
  %13 = lshr i32 %12, 1
  store i32 %13, i32* %1, align 4
  br label %15

14:                                               ; preds = %0
  br label %15

15:                                               ; preds = %14, %11, %9, %7, %5, %3
  %16 = load i32, i32* %1, align 4
  ret i32 %16
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local i32 @CLOCK_GetUart4ClkFreq() #0 {
  %1 = alloca i32, align 4
  store i32 0, i32* %1, align 4
  %2 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 32, i64 4), align 4
  switch i32 %2, label %14 [
    i32 0, label %3
    i32 1, label %5
    i32 2, label %7
    i32 3, label %9
    i32 4, label %11
  ]

3:                                                ; preds = %0
  %4 = call i32 @CLOCK_GetFroFreq()
  store i32 %4, i32* %1, align 4
  br label %15

5:                                                ; preds = %0
  %6 = call i32 @CLOCK_GetMainClkFreq()
  store i32 %6, i32* %1, align 4
  br label %15

7:                                                ; preds = %0
  %8 = call i32 @CLOCK_GetFRG0ClkFreq()
  store i32 %8, i32* %1, align 4
  br label %15

9:                                                ; preds = %0
  %10 = call i32 @CLOCK_GetFRG1ClkFreq()
  store i32 %10, i32* %1, align 4
  br label %15

11:                                               ; preds = %0
  %12 = call i32 @CLOCK_GetFroFreq()
  %13 = lshr i32 %12, 1
  store i32 %13, i32* %1, align 4
  br label %15

14:                                               ; preds = %0
  br label %15

15:                                               ; preds = %14, %11, %9, %7, %5, %3
  %16 = load i32, i32* %1, align 4
  ret i32 %16
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local i32 @CLOCK_GetFreq(i32) #0 {
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %4 = load i32, i32* %2, align 4
  switch i32 %4, label %24 [
    i32 0, label %5
    i32 1, label %7
    i32 2, label %9
    i32 3, label %11
    i32 4, label %14
    i32 6, label %16
    i32 5, label %18
    i32 7, label %20
    i32 8, label %22
  ]

5:                                                ; preds = %1
  %6 = call i32 @CLOCK_GetCoreSysClkFreq()
  store i32 %6, i32* %3, align 4
  br label %25

7:                                                ; preds = %1
  %8 = call i32 @CLOCK_GetMainClkFreq()
  store i32 %8, i32* %3, align 4
  br label %25

9:                                                ; preds = %1
  %10 = call i32 @CLOCK_GetFroFreq()
  store i32 %10, i32* %3, align 4
  br label %25

11:                                               ; preds = %1
  %12 = call i32 @CLOCK_GetFroFreq()
  %13 = lshr i32 %12, 1
  store i32 %13, i32* %3, align 4
  br label %25

14:                                               ; preds = %1
  %15 = call i32 @CLOCK_GetExtClkFreq()
  store i32 %15, i32* %3, align 4
  br label %25

16:                                               ; preds = %1
  %17 = call i32 @CLOCK_GetWdtOscFreq()
  store i32 %17, i32* %3, align 4
  br label %25

18:                                               ; preds = %1
  %19 = call i32 @CLOCK_GetSystemPLLFreq()
  store i32 %19, i32* %3, align 4
  br label %25

20:                                               ; preds = %1
  %21 = call i32 @CLOCK_GetFRG0ClkFreq()
  store i32 %21, i32* %3, align 4
  br label %25

22:                                               ; preds = %1
  %23 = call i32 @CLOCK_GetFRG1ClkFreq()
  store i32 %23, i32* %3, align 4
  br label %25

24:                                               ; preds = %1
  store i32 0, i32* %3, align 4
  br label %25

25:                                               ; preds = %24, %22, %20, %18, %16, %14, %11, %9, %7, %5
  %26 = load i32, i32* %3, align 4
  ret i32 %26
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal i32 @CLOCK_GetCoreSysClkFreq() #0 {
  %1 = call i32 @CLOCK_GetMainClkFreq()
  %2 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 19), align 4
  %3 = and i32 %2, 255
  %4 = udiv i32 %1, %3
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @CLOCK_InitSystemPll(%struct._clock_sys_pll*) #0 {
  %2 = alloca %struct._clock_sys_pll*, align 8
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  store %struct._clock_sys_pll* %0, %struct._clock_sys_pll** %2, align 8
  %6 = load %struct._clock_sys_pll*, %struct._clock_sys_pll** %2, align 8
  %7 = getelementptr inbounds %struct._clock_sys_pll, %struct._clock_sys_pll* %6, i32 0, i32 0
  %8 = load i32, i32* %7, align 4
  %9 = icmp ule i32 %8, 100000000
  br i1 %9, label %10, label %11

10:                                               ; preds = %1
  br label %12

11:                                               ; preds = %1
  call void @__assert_fail(i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str, i64 0, i64 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0), i32 517, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @__PRETTY_FUNCTION__.CLOCK_InitSystemPll, i64 0, i64 0)) #2
  unreachable

12:                                               ; preds = %10
  store i32 0, i32* %3, align 4
  store i32 0, i32* %4, align 4
  store i32 0, i32* %5, align 4
  %13 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 61), align 4
  %14 = or i32 %13, 128
  store volatile i32 %14, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 61), align 4
  %15 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 13), align 4
  %16 = and i32 %15, -4
  %17 = load %struct._clock_sys_pll*, %struct._clock_sys_pll** %2, align 8
  %18 = getelementptr inbounds %struct._clock_sys_pll, %struct._clock_sys_pll* %17, i32 0, i32 1
  %19 = load i32, i32* %18, align 4
  %20 = or i32 %16, %19
  %21 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 13), align 4
  %22 = or i32 %21, %20
  store volatile i32 %22, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 13), align 4
  call void @CLOCK_UpdateClkSrc(i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 14), i32 3)
  %23 = call i32 @CLOCK_GetSystemPLLInClockRate()
  store i32 %23, i32* %5, align 4
  %24 = load i32, i32* %5, align 4
  %25 = icmp ne i32 %24, 0
  br i1 %25, label %26, label %27

26:                                               ; preds = %12
  br label %28

27:                                               ; preds = %12
  call void @__assert_fail(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.2, i64 0, i64 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0), i32 530, i8* getelementptr inbounds ([50 x i8], [50 x i8]* @__PRETTY_FUNCTION__.CLOCK_InitSystemPll, i64 0, i64 0)) #2
  unreachable

28:                                               ; preds = %26
  %29 = load %struct._clock_sys_pll*, %struct._clock_sys_pll** %2, align 8
  %30 = getelementptr inbounds %struct._clock_sys_pll, %struct._clock_sys_pll* %29, i32 0, i32 0
  %31 = load i32, i32* %30, align 4
  %32 = load i32, i32* %5, align 4
  %33 = udiv i32 %31, %32
  store i32 %33, i32* %3, align 4
  %34 = load %struct._clock_sys_pll*, %struct._clock_sys_pll** %2, align 8
  %35 = getelementptr inbounds %struct._clock_sys_pll, %struct._clock_sys_pll* %34, i32 0, i32 0
  %36 = load i32, i32* %35, align 4
  %37 = call i32 @findSyestemPllPsel(i32 %36)
  store i32 %37, i32* %4, align 4
  %38 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 2), align 4
  %39 = and i32 %38, -128
  %40 = load i32, i32* %3, align 4
  %41 = icmp eq i32 %40, 0
  br i1 %41, label %42, label %43

42:                                               ; preds = %28
  br label %46

43:                                               ; preds = %28
  %44 = load i32, i32* %3, align 4
  %45 = sub i32 %44, 1
  br label %46

46:                                               ; preds = %43, %42
  %47 = phi i32 [ 0, %42 ], [ %45, %43 ]
  %48 = shl i32 %47, 0
  %49 = and i32 %48, 31
  %50 = or i32 %39, %49
  %51 = load i32, i32* %4, align 4
  %52 = shl i32 %51, 5
  %53 = and i32 %52, 96
  %54 = or i32 %50, %53
  store volatile i32 %54, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 2), align 4
  %55 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 61), align 4
  %56 = and i32 %55, -129
  store volatile i32 %56, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 61), align 4
  br label %57

57:                                               ; preds = %61, %46
  %58 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 3), align 4
  %59 = and i32 %58, 1
  %60 = icmp eq i32 %59, 0
  br i1 %60, label %61, label %62

61:                                               ; preds = %57
  br label %57

62:                                               ; preds = %57
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal void @CLOCK_UpdateClkSrc(i32*, i32) #0 {
  %3 = alloca i32*, align 8
  %4 = alloca i32, align 4
  store i32* %0, i32** %3, align 8
  store i32 %1, i32* %4, align 4
  %5 = load i32*, i32** %3, align 8
  %6 = icmp ne i32* %5, null
  br i1 %6, label %7, label %8

7:                                                ; preds = %2
  br label %9

8:                                                ; preds = %2
  call void @__assert_fail(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.6, i64 0, i64 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0), i32 101, i8* getelementptr inbounds ([55 x i8], [55 x i8]* @__PRETTY_FUNCTION__.CLOCK_UpdateClkSrc, i64 0, i64 0)) #2
  unreachable

9:                                                ; preds = %7
  %10 = load i32, i32* %4, align 4
  %11 = xor i32 %10, -1
  %12 = load i32*, i32** %3, align 8
  %13 = load volatile i32, i32* %12, align 4
  %14 = and i32 %13, %11
  store volatile i32 %14, i32* %12, align 4
  %15 = load i32, i32* %4, align 4
  %16 = load i32*, i32** %3, align 8
  %17 = load volatile i32, i32* %16, align 4
  %18 = or i32 %17, %15
  store volatile i32 %18, i32* %16, align 4
  br label %19

19:                                               ; preds = %25, %9
  %20 = load i32*, i32** %3, align 8
  %21 = load volatile i32, i32* %20, align 4
  %22 = load i32, i32* %4, align 4
  %23 = and i32 %21, %22
  %24 = icmp eq i32 %23, 0
  br i1 %24, label %25, label %26

25:                                               ; preds = %19
  br label %19

26:                                               ; preds = %19
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal i32 @findSyestemPllPsel(i32) #0 {
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  store i32 0, i32* %3, align 4
  %4 = load i32, i32* %2, align 4
  %5 = icmp ugt i32 %4, 78000000
  br i1 %5, label %6, label %7

6:                                                ; preds = %1
  store i32 0, i32* %3, align 4
  br label %18

7:                                                ; preds = %1
  %8 = load i32, i32* %2, align 4
  %9 = icmp ugt i32 %8, 39000000
  br i1 %9, label %10, label %11

10:                                               ; preds = %7
  store i32 1, i32* %3, align 4
  br label %17

11:                                               ; preds = %7
  %12 = load i32, i32* %2, align 4
  %13 = icmp ugt i32 %12, 19500000
  br i1 %13, label %14, label %15

14:                                               ; preds = %11
  store i32 2, i32* %3, align 4
  br label %16

15:                                               ; preds = %11
  store i32 3, i32* %3, align 4
  br label %16

16:                                               ; preds = %15, %14
  br label %17

17:                                               ; preds = %16, %10
  br label %18

18:                                               ; preds = %17, %6
  %19 = load i32, i32* %3, align 4
  ret i32 %19
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @CLOCK_InitExtClkin(i32) #0 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load volatile i32, i32* getelementptr inbounds (%struct.IOCON_Type, %struct.IOCON_Type* inttoptr (i64 1074020352 to %struct.IOCON_Type*), i32 0, i32 0, i64 11), align 4
  %4 = and i32 %3, -25
  store volatile i32 %4, i32* getelementptr inbounds (%struct.IOCON_Type, %struct.IOCON_Type* inttoptr (i64 1074020352 to %struct.IOCON_Type*), i32 0, i32 0, i64 11), align 4
  %5 = load volatile i32, i32* getelementptr inbounds (%struct.SWM_Type, %struct.SWM_Type* inttoptr (i64 1073790976 to %struct.SWM_Type*), i32 0, i32 2), align 4
  %6 = and i32 %5, -1025
  store volatile i32 %6, i32* getelementptr inbounds (%struct.SWM_Type, %struct.SWM_Type* inttoptr (i64 1073790976 to %struct.SWM_Type*), i32 0, i32 2), align 4
  %7 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 26), align 4
  %8 = or i32 %7, 1
  store volatile i32 %8, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 26), align 4
  %9 = load i32, i32* %2, align 4
  store volatile i32 %9, i32* @g_Ext_Clk_Freq, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @CLOCK_InitXtalin(i32) #0 {
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  store volatile i32 0, i32* %3, align 4
  %4 = load volatile i32, i32* getelementptr inbounds (%struct.IOCON_Type, %struct.IOCON_Type* inttoptr (i64 1074020352 to %struct.IOCON_Type*), i32 0, i32 0, i64 14), align 4
  %5 = and i32 %4, -25
  store volatile i32 %5, i32* getelementptr inbounds (%struct.IOCON_Type, %struct.IOCON_Type* inttoptr (i64 1074020352 to %struct.IOCON_Type*), i32 0, i32 0, i64 14), align 4
  %6 = load volatile i32, i32* getelementptr inbounds (%struct.SWM_Type, %struct.SWM_Type* inttoptr (i64 1073790976 to %struct.SWM_Type*), i32 0, i32 2), align 4
  %7 = and i32 %6, -129
  store volatile i32 %7, i32* getelementptr inbounds (%struct.SWM_Type, %struct.SWM_Type* inttoptr (i64 1073790976 to %struct.SWM_Type*), i32 0, i32 2), align 4
  %8 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 5), align 4
  %9 = or i32 %8, 1
  store volatile i32 %9, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 5), align 4
  %10 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 26), align 4
  %11 = and i32 %10, -2
  store volatile i32 %11, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 26), align 4
  %12 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 61), align 4
  %13 = and i32 %12, -33
  store volatile i32 %13, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 61), align 4
  store volatile i32 0, i32* %3, align 4
  br label %14

14:                                               ; preds = %18, %1
  %15 = load volatile i32, i32* %3, align 4
  %16 = icmp ult i32 %15, 1500
  br i1 %16, label %17, label %21

17:                                               ; preds = %14
  call void asm sideeffect "nop", "~{dirflag},~{fpsr},~{flags}"() #3, !srcloc !4
  br label %18

18:                                               ; preds = %17
  %19 = load volatile i32, i32* %3, align 4
  %20 = add i32 %19, 1
  store volatile i32 %20, i32* %3, align 4
  br label %14

21:                                               ; preds = %14
  %22 = load i32, i32* %2, align 4
  store volatile i32 %22, i32* @g_Ext_Clk_Freq, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @CLOCK_InitSysOsc(i32) #0 {
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  store volatile i32 0, i32* %3, align 4
  %4 = load volatile i32, i32* getelementptr inbounds (%struct.IOCON_Type, %struct.IOCON_Type* inttoptr (i64 1074020352 to %struct.IOCON_Type*), i32 0, i32 0, i64 13), align 4
  %5 = and i32 %4, -25
  store volatile i32 %5, i32* getelementptr inbounds (%struct.IOCON_Type, %struct.IOCON_Type* inttoptr (i64 1074020352 to %struct.IOCON_Type*), i32 0, i32 0, i64 13), align 4
  %6 = load volatile i32, i32* getelementptr inbounds (%struct.IOCON_Type, %struct.IOCON_Type* inttoptr (i64 1074020352 to %struct.IOCON_Type*), i32 0, i32 0, i64 14), align 4
  %7 = and i32 %6, -25
  store volatile i32 %7, i32* getelementptr inbounds (%struct.IOCON_Type, %struct.IOCON_Type* inttoptr (i64 1074020352 to %struct.IOCON_Type*), i32 0, i32 0, i64 14), align 4
  %8 = load volatile i32, i32* getelementptr inbounds (%struct.SWM_Type, %struct.SWM_Type* inttoptr (i64 1073790976 to %struct.SWM_Type*), i32 0, i32 2), align 4
  %9 = and i32 %8, -385
  store volatile i32 %9, i32* getelementptr inbounds (%struct.SWM_Type, %struct.SWM_Type* inttoptr (i64 1073790976 to %struct.SWM_Type*), i32 0, i32 2), align 4
  %10 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 5), align 4
  %11 = and i32 %10, -4
  %12 = load i32, i32* %2, align 4
  %13 = icmp ugt i32 %12, 15000000
  %14 = zext i1 %13 to i64
  %15 = select i1 %13, i32 2, i32 0
  %16 = or i32 %11, %15
  %17 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 5), align 4
  %18 = or i32 %17, %16
  store volatile i32 %18, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 5), align 4
  %19 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 26), align 4
  %20 = and i32 %19, -2
  store volatile i32 %20, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 26), align 4
  %21 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 61), align 4
  %22 = and i32 %21, -33
  store volatile i32 %22, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 61), align 4
  store volatile i32 0, i32* %3, align 4
  br label %23

23:                                               ; preds = %27, %1
  %24 = load volatile i32, i32* %3, align 4
  %25 = icmp ult i32 %24, 1500
  br i1 %25, label %26, label %30

26:                                               ; preds = %23
  call void asm sideeffect "nop", "~{dirflag},~{fpsr},~{flags}"() #3, !srcloc !5
  br label %27

27:                                               ; preds = %26
  %28 = load volatile i32, i32* %3, align 4
  %29 = add i32 %28, 1
  store volatile i32 %29, i32* %3, align 4
  br label %23

30:                                               ; preds = %23
  %31 = load i32, i32* %2, align 4
  store volatile i32 %31, i32* @g_Ext_Clk_Freq, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @CLOCK_InitWdtOsc(i32, i32) #0 {
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  %5 = alloca i32, align 4
  store i32 %0, i32* %3, align 4
  store i32 %1, i32* %4, align 4
  %6 = load i32, i32* %4, align 4
  %7 = icmp uge i32 %6, 2
  br i1 %7, label %8, label %9

8:                                                ; preds = %2
  br label %10

9:                                                ; preds = %2
  call void @__assert_fail(i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.3, i64 0, i64 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0), i32 640, i8* getelementptr inbounds ([57 x i8], [57 x i8]* @__PRETTY_FUNCTION__.CLOCK_InitWdtOsc, i64 0, i64 0)) #2
  unreachable

10:                                               ; preds = %8
  %11 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 6), align 4
  store i32 %11, i32* %5, align 4
  %12 = load i32, i32* %5, align 4
  %13 = and i32 %12, -512
  store i32 %13, i32* %5, align 4
  %14 = load i32, i32* %4, align 4
  %15 = lshr i32 %14, 1
  %16 = sub i32 %15, 1
  %17 = shl i32 %16, 0
  %18 = and i32 %17, 31
  %19 = load i32, i32* %3, align 4
  %20 = lshr i32 %19, 24
  %21 = and i32 %20, 255
  %22 = shl i32 %21, 5
  %23 = and i32 %22, 480
  %24 = or i32 %18, %23
  %25 = load i32, i32* %5, align 4
  %26 = or i32 %25, %24
  store i32 %26, i32* %5, align 4
  %27 = load i32, i32* %5, align 4
  store volatile i32 %27, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 6), align 4
  %28 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 61), align 4
  %29 = and i32 %28, -65
  store volatile i32 %29, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 61), align 4
  %30 = load i32, i32* %3, align 4
  %31 = and i32 %30, 16777215
  %32 = load i32, i32* %4, align 4
  %33 = udiv i32 %31, %32
  store volatile i32 %33, i32* @g_Wdt_Osc_Freq, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @CLOCK_SetMainClkSrc(i32) #0 {
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  %4 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %5 = load i32, i32* %2, align 4
  %6 = and i32 %5, 255
  store i32 %6, i32* %3, align 4
  %7 = load i32, i32* %2, align 4
  %8 = lshr i32 %7, 8
  %9 = and i32 %8, 255
  store i32 %9, i32* %4, align 4
  %10 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 17), align 4
  %11 = and i32 %10, 3
  %12 = load i32, i32* %4, align 4
  %13 = icmp ne i32 %11, %12
  br i1 %13, label %14, label %24

14:                                               ; preds = %1
  %15 = load i32, i32* %3, align 4
  %16 = icmp eq i32 %15, 0
  br i1 %16, label %17, label %24

17:                                               ; preds = %14
  %18 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 17), align 4
  %19 = and i32 %18, -4
  %20 = load i32, i32* %4, align 4
  %21 = shl i32 %20, 0
  %22 = and i32 %21, 3
  %23 = or i32 %19, %22
  store volatile i32 %23, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 17), align 4
  call void @CLOCK_UpdateClkSrc(i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 18), i32 1)
  br label %24

24:                                               ; preds = %17, %14, %1
  %25 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 15), align 4
  %26 = and i32 %25, 3
  %27 = load i32, i32* %3, align 4
  %28 = icmp ne i32 %26, %27
  br i1 %28, label %29, label %36

29:                                               ; preds = %24
  %30 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 15), align 4
  %31 = and i32 %30, -4
  %32 = load i32, i32* %3, align 4
  %33 = shl i32 %32, 0
  %34 = and i32 %33, 3
  %35 = or i32 %31, %34
  store volatile i32 %35, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 15), align 4
  call void @CLOCK_UpdateClkSrc(i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 16), i32 1)
  br label %36

36:                                               ; preds = %29, %24
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @CLOCK_SetFroOutClkSrc(i32) #0 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load i32, i32* %2, align 4
  %4 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 7), align 4
  %5 = and i32 %4, 131072
  %6 = icmp ne i32 %3, %5
  br i1 %6, label %7, label %12

7:                                                ; preds = %1
  %8 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 7), align 4
  %9 = and i32 %8, -131073
  %10 = load i32, i32* %2, align 4
  %11 = or i32 %9, %10
  store volatile i32 %11, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 7), align 4
  call void @CLOCK_UpdateClkSrc(i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 9), i32 1)
  br label %12

12:                                               ; preds = %7, %1
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define weak dso_local void @SDK_DelayAtLeastUs(i32) #0 {
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %4 = load i32, i32* %2, align 4
  %5 = icmp ne i32 0, %4
  br i1 %5, label %6, label %7

6:                                                ; preds = %1
  br label %8

7:                                                ; preds = %1
  call void @__assert_fail(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.4, i64 0, i64 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0), i32 702, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @__PRETTY_FUNCTION__.SDK_DelayAtLeastUs, i64 0, i64 0)) #2
  unreachable

8:                                                ; preds = %6
  %9 = load i32, i32* %2, align 4
  %10 = zext i32 %9 to i64
  %11 = mul i64 %10, 30000000
  %12 = udiv i64 %11, 1000000
  %13 = trunc i64 %12 to i32
  store i32 %13, i32* %3, align 4
  %14 = load i32, i32* %3, align 4
  %15 = udiv i32 %14, 4
  store i32 %15, i32* %3, align 4
  br label %16

16:                                               ; preds = %21, %8
  %17 = load i32, i32* %3, align 4
  %18 = zext i32 %17 to i64
  %19 = icmp ugt i64 %18, 0
  br i1 %19, label %20, label %24

20:                                               ; preds = %16
  call void asm sideeffect "nop", "~{dirflag},~{fpsr},~{flags}"() #3, !srcloc !6
  br label %21

21:                                               ; preds = %20
  %22 = load i32, i32* %3, align 4
  %23 = add i32 %22, -1
  store i32 %23, i32* %3, align 4
  br label %16

24:                                               ; preds = %16
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @BOARD_InitBootClocks() #0 {
  call void @BOARD_BootClockFRO18M()
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @BOARD_BootClockFRO18M() #0 {
  call void @POWER_DisablePD(i32 1)
  call void @POWER_DisablePD(i32 2)
  call void @CLOCK_SetFroOscFreq(i32 18000)
  call void @CLOCK_SetFroOutClkSrc(i32 131072)
  call void @POWER_DisablePD(i32 32)
  call void @CLOCK_Select(i32 29696)
  call void @CLOCK_SetMainClkSrc(i32 0)
  call void @CLOCK_SetCoreSysClkDiv(i32 1)
  store i32 18000000, i32* @SystemCoreClock, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal void @POWER_DisablePD(i32) #0 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load i32, i32* %2, align 4
  %4 = xor i32 %3, -1
  %5 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 61), align 4
  %6 = and i32 %5, %4
  store volatile i32 %6, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 61), align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal void @CLOCK_SetFroOscFreq(i32) #0 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load i32, i32* %2, align 4
  call void inttoptr (i64 251668213 to void (i32)*)(i32 %3)
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal void @CLOCK_Select(i32) #0 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load i32, i32* %2, align 4
  %4 = and i32 %3, 255
  %5 = load i32, i32* %2, align 4
  %6 = lshr i32 %5, 8
  %7 = and i32 %6, 255
  %8 = add i32 ptrtoint (%struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*) to i32), %7
  %9 = zext i32 %8 to i64
  %10 = inttoptr i64 %9 to i32*
  store volatile i32 %4, i32* %10, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal void @CLOCK_SetCoreSysClkDiv(i32) #0 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load i32, i32* %2, align 4
  %4 = icmp ne i32 %3, 0
  br i1 %4, label %5, label %6

5:                                                ; preds = %1
  br label %7

6:                                                ; preds = %1
  call void @__assert_fail(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.7, i64 0, i64 0), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.1.8, i64 0, i64 0), i32 497, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @__PRETTY_FUNCTION__.CLOCK_SetCoreSysClkDiv, i64 0, i64 0)) #2
  unreachable

7:                                                ; preds = %5
  %8 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 19), align 4
  %9 = and i32 %8, -256
  %10 = load i32, i32* %2, align 4
  %11 = shl i32 %10, 0
  %12 = and i32 %11, 255
  %13 = or i32 %9, %12
  store volatile i32 %13, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 19), align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @BOARD_BootClockFRO24M() #0 {
  call void @POWER_DisablePD(i32 1)
  call void @POWER_DisablePD(i32 2)
  call void @CLOCK_SetFroOscFreq(i32 24000)
  call void @CLOCK_SetFroOutClkSrc(i32 131072)
  call void @POWER_DisablePD(i32 32)
  call void @CLOCK_Select(i32 29696)
  call void @CLOCK_SetMainClkSrc(i32 0)
  call void @CLOCK_SetCoreSysClkDiv(i32 1)
  store i32 24000000, i32* @SystemCoreClock, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @BOARD_BootClockFRO30M() #0 {
  call void @POWER_DisablePD(i32 1)
  call void @POWER_DisablePD(i32 2)
  call void @CLOCK_SetFroOscFreq(i32 30000)
  call void @CLOCK_SetFroOutClkSrc(i32 131072)
  call void @POWER_DisablePD(i32 32)
  call void @CLOCK_Select(i32 29696)
  call void @CLOCK_SetMainClkSrc(i32 0)
  call void @CLOCK_SetCoreSysClkDiv(i32 1)
  store i32 30000000, i32* @SystemCoreClock, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @BOARD_BootClockPll24M() #0 {
  %1 = alloca %struct._clock_sys_pll, align 4
  call void @POWER_DisablePD(i32 1)
  call void @POWER_DisablePD(i32 2)
  call void @CLOCK_SetFroOscFreq(i32 24000)
  call void @CLOCK_SetFroOutClkSrc(i32 131072)
  call void @POWER_DisablePD(i32 32)
  call void @CLOCK_Select(i32 29696)
  %2 = getelementptr inbounds %struct._clock_sys_pll, %struct._clock_sys_pll* %1, i32 0, i32 1
  store i32 0, i32* %2, align 4
  %3 = getelementptr inbounds %struct._clock_sys_pll, %struct._clock_sys_pll* %1, i32 0, i32 0
  store i32 24000000, i32* %3, align 4
  call void @CLOCK_InitSystemPll(%struct._clock_sys_pll* %1)
  call void @CLOCK_SetMainClkSrc(i32 1)
  call void @CLOCK_SetCoreSysClkDiv(i32 1)
  store i32 24000000, i32* @SystemCoreClock, align 4
  ret void
}

attributes #0 = { noinline nounwind optnone sspstrong uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noreturn nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn nounwind }
attributes #3 = { nounwind }

!llvm.ident = !{!0, !0}
!llvm.module.flags = !{!1, !2, !3}

!0 = !{!"clang version 9.0.0 (tags/RELEASE_900/final)"}
!1 = !{i32 1, !"wchar_size", i32 4}
!2 = !{i32 7, !"PIC Level", i32 2}
!3 = !{i32 7, !"PIE Level", i32 2}
!4 = !{i32 14709}
!5 = !{i32 15802}
!6 = !{i32 -2146462338}
