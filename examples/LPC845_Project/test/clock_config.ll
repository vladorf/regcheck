; ModuleID = 'clock_config.c'
source_filename = "clock_config.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.SYSCON_Type = type { i32, [4 x i8], i32, i32, [16 x i8], i32, i32, i32, [4 x i8], i32, [4 x i8], i32, [4 x i8], i32, i32, i32, i32, i32, i32, i32, [4 x i8], i32, i32, i32, i32, i32, i32, [8 x i8], i32, i32, i32, i32, [11 x i32], [20 x i8], [2 x %struct.anon], i32, i32, [4 x i8], i32, [2 x i32], [44 x i8], i32, i32, i32, i32, i32, i32, i32, i32, i32, [24 x i8], i32, i32, [8 x i32], [108 x i8], i32, [12 x i8], i32, [24 x i8], i32, i32, i32, [444 x i8], i32 }
%struct.anon = type { i32, i32, i32, [4 x i8] }
%struct._clock_sys_pll = type { i32, i32 }

@SystemCoreClock = external global i32, align 4
@.str = private unnamed_addr constant [12 x i8] c"value != 0U\00", align 1
@.str.1 = private unnamed_addr constant [14 x i8] c"./fsl_clock.h\00", align 1
@__PRETTY_FUNCTION__.CLOCK_SetCoreSysClkDiv = private unnamed_addr constant [38 x i8] c"void CLOCK_SetCoreSysClkDiv(uint32_t)\00", align 1

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @BOARD_InitBootClocks() #0 {
  call void @BOARD_BootClockFRO18M()
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @BOARD_BootClockFRO18M() #0 {
  call void @POWER_DisablePD(i32 1)
  call void @POWER_DisablePD(i32 2)
  call void @CLOCK_SetFroOscFreq(i32 18000)
  call void @CLOCK_SetFroOutClkSrc(i32 131072)
  call void @POWER_DisablePD(i32 32)
  call void @CLOCK_Select(i32 29696)
  call void @CLOCK_SetMainClkSrc(i32 0)
  call void @CLOCK_SetCoreSysClkDiv(i32 1)
  store i32 18000000, i32* @SystemCoreClock, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal void @POWER_DisablePD(i32) #0 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load i32, i32* %2, align 4
  %4 = xor i32 %3, -1
  %5 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 61), align 4
  %6 = and i32 %5, %4
  store volatile i32 %6, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 61), align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal void @CLOCK_SetFroOscFreq(i32) #0 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load i32, i32* %2, align 4
  call void inttoptr (i64 251668213 to void (i32)*)(i32 %3)
  ret void
}

declare void @CLOCK_SetFroOutClkSrc(i32) #1

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal void @CLOCK_Select(i32) #0 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load i32, i32* %2, align 4
  %4 = and i32 %3, 255
  %5 = load i32, i32* %2, align 4
  %6 = lshr i32 %5, 8
  %7 = and i32 %6, 255
  %8 = add i32 ptrtoint (%struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*) to i32), %7
  %9 = zext i32 %8 to i64
  %10 = inttoptr i64 %9 to i32*
  store volatile i32 %4, i32* %10, align 4
  ret void
}

declare void @CLOCK_SetMainClkSrc(i32) #1

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define internal void @CLOCK_SetCoreSysClkDiv(i32) #0 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  %3 = load i32, i32* %2, align 4
  %4 = icmp ne i32 %3, 0
  br i1 %4, label %5, label %6

5:                                                ; preds = %1
  br label %7

6:                                                ; preds = %1
  call void @__assert_fail(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str, i64 0, i64 0), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.1, i64 0, i64 0), i32 497, i8* getelementptr inbounds ([38 x i8], [38 x i8]* @__PRETTY_FUNCTION__.CLOCK_SetCoreSysClkDiv, i64 0, i64 0)) #3
  unreachable

7:                                                ; preds = %5
  %8 = load volatile i32, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 19), align 4
  %9 = and i32 %8, -256
  %10 = load i32, i32* %2, align 4
  %11 = shl i32 %10, 0
  %12 = and i32 %11, 255
  %13 = or i32 %9, %12
  store volatile i32 %13, i32* getelementptr inbounds (%struct.SYSCON_Type, %struct.SYSCON_Type* inttoptr (i64 1074036736 to %struct.SYSCON_Type*), i32 0, i32 19), align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @BOARD_BootClockFRO24M() #0 {
  call void @POWER_DisablePD(i32 1)
  call void @POWER_DisablePD(i32 2)
  call void @CLOCK_SetFroOscFreq(i32 24000)
  call void @CLOCK_SetFroOutClkSrc(i32 131072)
  call void @POWER_DisablePD(i32 32)
  call void @CLOCK_Select(i32 29696)
  call void @CLOCK_SetMainClkSrc(i32 0)
  call void @CLOCK_SetCoreSysClkDiv(i32 1)
  store i32 24000000, i32* @SystemCoreClock, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @BOARD_BootClockFRO30M() #0 {
  call void @POWER_DisablePD(i32 1)
  call void @POWER_DisablePD(i32 2)
  call void @CLOCK_SetFroOscFreq(i32 30000)
  call void @CLOCK_SetFroOutClkSrc(i32 131072)
  call void @POWER_DisablePD(i32 32)
  call void @CLOCK_Select(i32 29696)
  call void @CLOCK_SetMainClkSrc(i32 0)
  call void @CLOCK_SetCoreSysClkDiv(i32 1)
  store i32 30000000, i32* @SystemCoreClock, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone sspstrong uwtable
define dso_local void @BOARD_BootClockPll24M() #0 {
  %1 = alloca %struct._clock_sys_pll, align 4
  call void @POWER_DisablePD(i32 1)
  call void @POWER_DisablePD(i32 2)
  call void @CLOCK_SetFroOscFreq(i32 24000)
  call void @CLOCK_SetFroOutClkSrc(i32 131072)
  call void @POWER_DisablePD(i32 32)
  call void @CLOCK_Select(i32 29696)
  %2 = getelementptr inbounds %struct._clock_sys_pll, %struct._clock_sys_pll* %1, i32 0, i32 1
  store i32 0, i32* %2, align 4
  %3 = getelementptr inbounds %struct._clock_sys_pll, %struct._clock_sys_pll* %1, i32 0, i32 0
  store i32 24000000, i32* %3, align 4
  call void @CLOCK_InitSystemPll(%struct._clock_sys_pll* %1)
  call void @CLOCK_SetMainClkSrc(i32 1)
  call void @CLOCK_SetCoreSysClkDiv(i32 1)
  store i32 24000000, i32* @SystemCoreClock, align 4
  ret void
}

declare void @CLOCK_InitSystemPll(%struct._clock_sys_pll*) #1

; Function Attrs: noreturn nounwind
declare void @__assert_fail(i8*, i8*, i32, i8*) #2

attributes #0 = { noinline nounwind optnone sspstrong uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn nounwind }

!llvm.module.flags = !{!0, !1, !2}
!llvm.ident = !{!3}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{i32 7, !"PIE Level", i32 2}
!3 = !{!"clang version 9.0.0 (tags/RELEASE_900/final)"}
