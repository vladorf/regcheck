	.text
	.file	"llvm-link"
	.globl	CLOCK_SetFRG0ClkFreq    # -- Begin function CLOCK_SetFRG0ClkFreq
	.p2align	4, 0x90
	.type	CLOCK_SetFRG0ClkFreq,@function
CLOCK_SetFRG0ClkFreq:                   # @CLOCK_SetFRG0ClkFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	$1074036736, %eax       # imm = 0x40048000
	addq	$208, %rax
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %esi
	movq	%rax, %rdi
	callq	CLOCK_SetFRGClkFreq
	andb	$1, %al
	movzbl	%al, %eax
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end0:
	.size	CLOCK_SetFRG0ClkFreq, .Lfunc_end0-CLOCK_SetFRG0ClkFreq
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90         # -- Begin function CLOCK_SetFRGClkFreq
	.type	CLOCK_SetFRGClkFreq,@function
CLOCK_SetFRGClkFreq:                    # @CLOCK_SetFRGClkFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -8(%rbp)
	cmpl	$0, -8(%rbp)
	je	.LBB1_2
# %bb.1:
	jmp	.LBB1_3
.LBB1_2:
	movabsq	$.L.str.5, %rdi
	movabsq	$.L.str.1, %rsi
	movl	$81, %edx
	movabsq	$.L__PRETTY_FUNCTION__.CLOCK_SetFRGClkFreq, %rcx
	callq	__assert_fail
.LBB1_3:
	movq	-24(%rbp), %rdi
	callq	CLOCK_GetFRGInputClkFreq
	movl	%eax, -12(%rbp)
	movl	-8(%rbp), %eax
	cmpl	-12(%rbp), %eax
	ja	.LBB1_5
# %bb.4:
	movl	-12(%rbp), %eax
	xorl	%edx, %edx
	divl	-8(%rbp)
	cmpl	$2, %eax
	jb	.LBB1_6
.LBB1_5:
	movb	$0, -1(%rbp)
	jmp	.LBB1_7
.LBB1_6:
	movl	-12(%rbp), %eax
	subl	-8(%rbp), %eax
	movl	%eax, %eax
	shlq	$8, %rax
	movl	-8(%rbp), %ecx
	xorl	%edx, %edx
	divq	%rcx
	movl	%eax, -28(%rbp)
	movq	-24(%rbp), %rax
	movl	$255, (%rax)
	movl	-28(%rbp), %eax
	shll	$0, %eax
	andl	$255, %eax
	movq	-24(%rbp), %rcx
	addl	$4, %ecx
	movl	%ecx, %ecx
	movl	%eax, (%rcx)
	movb	$1, -1(%rbp)
.LBB1_7:
	movb	-1(%rbp), %al
	andb	$1, %al
	movzbl	%al, %eax
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end1:
	.size	CLOCK_SetFRGClkFreq, .Lfunc_end1-CLOCK_SetFRGClkFreq
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90         # -- Begin function CLOCK_GetFRGInputClkFreq
	.type	CLOCK_GetFRGInputClkFreq,@function
CLOCK_GetFRGInputClkFreq:               # @CLOCK_GetFRGInputClkFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rax
	addl	$8, %eax
	movl	%eax, %eax
	movl	(%rax), %eax
	andl	$3, %eax
	movl	%eax, -8(%rbp)
	cmpl	$0, -8(%rbp)
	jne	.LBB2_2
# %bb.1:
	callq	CLOCK_GetFroFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB2_5
.LBB2_2:
	cmpl	$1, -8(%rbp)
	jne	.LBB2_4
# %bb.3:
	callq	CLOCK_GetMainClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB2_5
.LBB2_4:
	callq	CLOCK_GetSystemPLLFreq
	movl	%eax, -4(%rbp)
.LBB2_5:
	movl	-4(%rbp), %eax
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end2:
	.size	CLOCK_GetFRGInputClkFreq, .Lfunc_end2-CLOCK_GetFRGInputClkFreq
	.cfi_endproc
                                        # -- End function
	.globl	CLOCK_GetFroFreq        # -- Begin function CLOCK_GetFroFreq
	.p2align	4, 0x90
	.type	CLOCK_GetFroFreq,@function
CLOCK_GetFroFreq:                       # @CLOCK_GetFroFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	40(%rax), %eax
	andl	$3, %eax
	movl	%eax, -8(%rbp)
	movl	$0, -4(%rbp)
	cmpl	$0, -8(%rbp)
	jne	.LBB3_2
# %bb.1:
	movl	$18000000, -4(%rbp)     # imm = 0x112A880
	jmp	.LBB3_6
.LBB3_2:
	cmpl	$1, -8(%rbp)
	jne	.LBB3_4
# %bb.3:
	movl	$24000000, -4(%rbp)     # imm = 0x16E3600
	jmp	.LBB3_5
.LBB3_4:
	movl	$30000000, -4(%rbp)     # imm = 0x1C9C380
.LBB3_5:
	jmp	.LBB3_6
.LBB3_6:
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	40(%rax), %eax
	andl	$131072, %eax           # imm = 0x20000
	shrl	$17, %eax
	cmpl	$0, %eax
	jne	.LBB3_8
# %bb.7:
	movl	$1342242816, %eax       # imm = 0x50010000
	movl	(%rax), %eax
	andl	$2, %eax
	cmpl	$0, %eax
	movl	$16, %eax
	movl	$2, %ecx
	cmovnel	%eax, %ecx
	movl	-4(%rbp), %eax
	xorl	%edx, %edx
	divl	%ecx
	movl	%eax, -4(%rbp)
.LBB3_8:
	movl	-4(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end3:
	.size	CLOCK_GetFroFreq, .Lfunc_end3-CLOCK_GetFroFreq
	.cfi_endproc
                                        # -- End function
	.globl	CLOCK_GetMainClkFreq    # -- Begin function CLOCK_GetMainClkFreq
	.p2align	4, 0x90
	.type	CLOCK_GetMainClkFreq,@function
CLOCK_GetMainClkFreq:                   # @CLOCK_GetMainClkFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	$0, -4(%rbp)
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	72(%rax), %eax
	andl	$3, %eax
	cmpl	$1, %eax
	jne	.LBB4_2
# %bb.1:
	callq	CLOCK_GetSystemPLLFreq
	movl	%eax, -8(%rbp)
	jmp	.LBB4_10
.LBB4_2:
	movl	1074036816, %eax
	movq	%rax, %rcx
	subq	$3, %rcx
	ja	.LBB4_8
# %bb.3:
	movq	.LJTI4_0(,%rax,8), %rax
	jmpq	*%rax
.LBB4_4:
	callq	CLOCK_GetFroFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB4_9
.LBB4_5:
	callq	CLOCK_GetExtClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB4_9
.LBB4_6:
	callq	CLOCK_GetWdtOscFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB4_9
.LBB4_7:
	callq	CLOCK_GetFroFreq
	shrl	$1, %eax
	movl	%eax, -4(%rbp)
	jmp	.LBB4_9
.LBB4_8:
	jmp	.LBB4_9
.LBB4_9:
	movl	-4(%rbp), %eax
	movl	%eax, -8(%rbp)
.LBB4_10:
	movl	-8(%rbp), %eax
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end4:
	.size	CLOCK_GetMainClkFreq, .Lfunc_end4-CLOCK_GetMainClkFreq
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_4
	.quad	.LBB4_5
	.quad	.LBB4_6
	.quad	.LBB4_7
                                        # -- End function
	.text
	.p2align	4, 0x90         # -- Begin function CLOCK_GetSystemPLLFreq
	.type	CLOCK_GetSystemPLLFreq,@function
CLOCK_GetSystemPLLFreq:                 # @CLOCK_GetSystemPLLFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	CLOCK_GetSystemPLLInClockRate
	movl	$1074036736, %ecx       # imm = 0x40048000
	movl	8(%rcx), %ecx
	andl	$31, %ecx
	addl	$1, %ecx
	imull	%ecx, %eax
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end5:
	.size	CLOCK_GetSystemPLLFreq, .Lfunc_end5-CLOCK_GetSystemPLLFreq
	.cfi_endproc
                                        # -- End function
	.globl	CLOCK_GetSystemPLLInClockRate # -- Begin function CLOCK_GetSystemPLLInClockRate
	.p2align	4, 0x90
	.type	CLOCK_GetSystemPLLInClockRate,@function
CLOCK_GetSystemPLLInClockRate:          # @CLOCK_GetSystemPLLInClockRate
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	$0, -4(%rbp)
	movl	1074036800, %eax
	movq	%rax, %rcx
	subq	$3, %rcx
	ja	.LBB6_6
# %bb.1:
	movq	.LJTI6_0(,%rax,8), %rax
	jmpq	*%rax
.LBB6_2:
	callq	CLOCK_GetFroFreq
	shrl	$1, %eax
	movl	%eax, -4(%rbp)
	jmp	.LBB6_7
.LBB6_3:
	callq	CLOCK_GetFroFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB6_7
.LBB6_4:
	callq	CLOCK_GetExtClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB6_7
.LBB6_5:
	callq	CLOCK_GetWdtOscFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB6_7
.LBB6_6:
	jmp	.LBB6_7
.LBB6_7:
	movl	-4(%rbp), %eax
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end6:
	.size	CLOCK_GetSystemPLLInClockRate, .Lfunc_end6-CLOCK_GetSystemPLLInClockRate
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_3
	.quad	.LBB6_4
	.quad	.LBB6_5
	.quad	.LBB6_2
                                        # -- End function
	.text
	.p2align	4, 0x90         # -- Begin function CLOCK_GetExtClkFreq
	.type	CLOCK_GetExtClkFreq,@function
CLOCK_GetExtClkFreq:                    # @CLOCK_GetExtClkFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movl	g_Ext_Clk_Freq, %eax
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end7:
	.size	CLOCK_GetExtClkFreq, .Lfunc_end7-CLOCK_GetExtClkFreq
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90         # -- Begin function CLOCK_GetWdtOscFreq
	.type	CLOCK_GetWdtOscFreq,@function
CLOCK_GetWdtOscFreq:                    # @CLOCK_GetWdtOscFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movl	g_Wdt_Osc_Freq, %eax
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end8:
	.size	CLOCK_GetWdtOscFreq, .Lfunc_end8-CLOCK_GetWdtOscFreq
	.cfi_endproc
                                        # -- End function
	.globl	CLOCK_SetFRG1ClkFreq    # -- Begin function CLOCK_SetFRG1ClkFreq
	.p2align	4, 0x90
	.type	CLOCK_SetFRG1ClkFreq,@function
CLOCK_SetFRG1ClkFreq:                   # @CLOCK_SetFRG1ClkFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	$1074036736, %eax       # imm = 0x40048000
	addq	$224, %rax
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %esi
	movq	%rax, %rdi
	callq	CLOCK_SetFRGClkFreq
	andb	$1, %al
	movzbl	%al, %eax
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end9:
	.size	CLOCK_SetFRG1ClkFreq, .Lfunc_end9-CLOCK_SetFRG1ClkFreq
	.cfi_endproc
                                        # -- End function
	.globl	CLOCK_GetFRG0ClkFreq    # -- Begin function CLOCK_GetFRG0ClkFreq
	.p2align	4, 0x90
	.type	CLOCK_GetFRG0ClkFreq,@function
CLOCK_GetFRG0ClkFreq:                   # @CLOCK_GetFRG0ClkFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movl	$1074036736, %edi       # imm = 0x40048000
	addq	$208, %rdi
	callq	CLOCK_GetFRGInputClkFreq
	shll	$8, %eax
	movl	%eax, %eax
	movl	$1074036736, %ecx       # imm = 0x40048000
	movl	212(%rcx), %ecx
	andl	$255, %ecx
	addl	$256, %ecx              # imm = 0x100
	movl	%ecx, %ecx
	xorl	%edx, %edx
	divq	%rcx
                                        # kill: def $eax killed $eax killed $rax
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end10:
	.size	CLOCK_GetFRG0ClkFreq, .Lfunc_end10-CLOCK_GetFRG0ClkFreq
	.cfi_endproc
                                        # -- End function
	.globl	CLOCK_GetFRG1ClkFreq    # -- Begin function CLOCK_GetFRG1ClkFreq
	.p2align	4, 0x90
	.type	CLOCK_GetFRG1ClkFreq,@function
CLOCK_GetFRG1ClkFreq:                   # @CLOCK_GetFRG1ClkFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movl	$1074036736, %edi       # imm = 0x40048000
	addq	$224, %rdi
	callq	CLOCK_GetFRGInputClkFreq
	shll	$8, %eax
	movl	%eax, %eax
	movl	$1074036736, %ecx       # imm = 0x40048000
	movl	228(%rcx), %ecx
	andl	$255, %ecx
	addl	$256, %ecx              # imm = 0x100
	movl	%ecx, %ecx
	xorl	%edx, %edx
	divq	%rcx
                                        # kill: def $eax killed $eax killed $rax
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end11:
	.size	CLOCK_GetFRG1ClkFreq, .Lfunc_end11-CLOCK_GetFRG1ClkFreq
	.cfi_endproc
                                        # -- End function
	.globl	CLOCK_GetClockOutClkFreq # -- Begin function CLOCK_GetClockOutClkFreq
	.p2align	4, 0x90
	.type	CLOCK_GetClockOutClkFreq,@function
CLOCK_GetClockOutClkFreq:               # @CLOCK_GetClockOutClkFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	1074036980, %eax
	movzbl	%al, %eax
	movl	%eax, -8(%rbp)
	movl	$0, -4(%rbp)
	movl	1074036976, %eax
	movq	%rax, %rcx
	subq	$4, %rcx
	ja	.LBB12_7
# %bb.1:
	movq	.LJTI12_0(,%rax,8), %rax
	jmpq	*%rax
.LBB12_2:
	callq	CLOCK_GetFroFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB12_8
.LBB12_3:
	callq	CLOCK_GetMainClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB12_8
.LBB12_4:
	callq	CLOCK_GetSystemPLLFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB12_8
.LBB12_5:
	callq	CLOCK_GetExtClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB12_8
.LBB12_6:
	callq	CLOCK_GetWdtOscFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB12_8
.LBB12_7:
	jmp	.LBB12_8
.LBB12_8:
	cmpl	$0, -8(%rbp)
	jne	.LBB12_10
# %bb.9:
	xorl	%eax, %eax
	jmp	.LBB12_11
.LBB12_10:
	movl	-4(%rbp), %eax
	xorl	%edx, %edx
	divl	-8(%rbp)
.LBB12_11:
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end12:
	.size	CLOCK_GetClockOutClkFreq, .Lfunc_end12-CLOCK_GetClockOutClkFreq
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI12_0:
	.quad	.LBB12_2
	.quad	.LBB12_3
	.quad	.LBB12_4
	.quad	.LBB12_5
	.quad	.LBB12_6
                                        # -- End function
	.text
	.globl	CLOCK_GetUart0ClkFreq   # -- Begin function CLOCK_GetUart0ClkFreq
	.p2align	4, 0x90
	.type	CLOCK_GetUart0ClkFreq,@function
CLOCK_GetUart0ClkFreq:                  # @CLOCK_GetUart0ClkFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	$0, -4(%rbp)
	movl	1074036880, %eax
	movq	%rax, %rcx
	subq	$4, %rcx
	ja	.LBB13_7
# %bb.1:
	movq	.LJTI13_0(,%rax,8), %rax
	jmpq	*%rax
.LBB13_2:
	callq	CLOCK_GetFroFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB13_8
.LBB13_3:
	callq	CLOCK_GetMainClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB13_8
.LBB13_4:
	callq	CLOCK_GetFRG0ClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB13_8
.LBB13_5:
	callq	CLOCK_GetFRG1ClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB13_8
.LBB13_6:
	callq	CLOCK_GetFroFreq
	shrl	$1, %eax
	movl	%eax, -4(%rbp)
	jmp	.LBB13_8
.LBB13_7:
	jmp	.LBB13_8
.LBB13_8:
	movl	-4(%rbp), %eax
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end13:
	.size	CLOCK_GetUart0ClkFreq, .Lfunc_end13-CLOCK_GetUart0ClkFreq
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI13_0:
	.quad	.LBB13_2
	.quad	.LBB13_3
	.quad	.LBB13_4
	.quad	.LBB13_5
	.quad	.LBB13_6
                                        # -- End function
	.text
	.globl	CLOCK_GetUart1ClkFreq   # -- Begin function CLOCK_GetUart1ClkFreq
	.p2align	4, 0x90
	.type	CLOCK_GetUart1ClkFreq,@function
CLOCK_GetUart1ClkFreq:                  # @CLOCK_GetUart1ClkFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	$0, -4(%rbp)
	movl	1074036884, %eax
	movq	%rax, %rcx
	subq	$4, %rcx
	ja	.LBB14_7
# %bb.1:
	movq	.LJTI14_0(,%rax,8), %rax
	jmpq	*%rax
.LBB14_2:
	callq	CLOCK_GetFroFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB14_8
.LBB14_3:
	callq	CLOCK_GetMainClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB14_8
.LBB14_4:
	callq	CLOCK_GetFRG0ClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB14_8
.LBB14_5:
	callq	CLOCK_GetFRG1ClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB14_8
.LBB14_6:
	callq	CLOCK_GetFroFreq
	shrl	$1, %eax
	movl	%eax, -4(%rbp)
	jmp	.LBB14_8
.LBB14_7:
	jmp	.LBB14_8
.LBB14_8:
	movl	-4(%rbp), %eax
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end14:
	.size	CLOCK_GetUart1ClkFreq, .Lfunc_end14-CLOCK_GetUart1ClkFreq
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI14_0:
	.quad	.LBB14_2
	.quad	.LBB14_3
	.quad	.LBB14_4
	.quad	.LBB14_5
	.quad	.LBB14_6
                                        # -- End function
	.text
	.globl	CLOCK_GetUart2ClkFreq   # -- Begin function CLOCK_GetUart2ClkFreq
	.p2align	4, 0x90
	.type	CLOCK_GetUart2ClkFreq,@function
CLOCK_GetUart2ClkFreq:                  # @CLOCK_GetUart2ClkFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	$0, -4(%rbp)
	movl	1074036888, %eax
	movq	%rax, %rcx
	subq	$4, %rcx
	ja	.LBB15_7
# %bb.1:
	movq	.LJTI15_0(,%rax,8), %rax
	jmpq	*%rax
.LBB15_2:
	callq	CLOCK_GetFroFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB15_8
.LBB15_3:
	callq	CLOCK_GetMainClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB15_8
.LBB15_4:
	callq	CLOCK_GetFRG0ClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB15_8
.LBB15_5:
	callq	CLOCK_GetFRG1ClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB15_8
.LBB15_6:
	callq	CLOCK_GetFroFreq
	shrl	$1, %eax
	movl	%eax, -4(%rbp)
	jmp	.LBB15_8
.LBB15_7:
	jmp	.LBB15_8
.LBB15_8:
	movl	-4(%rbp), %eax
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end15:
	.size	CLOCK_GetUart2ClkFreq, .Lfunc_end15-CLOCK_GetUart2ClkFreq
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI15_0:
	.quad	.LBB15_2
	.quad	.LBB15_3
	.quad	.LBB15_4
	.quad	.LBB15_5
	.quad	.LBB15_6
                                        # -- End function
	.text
	.globl	CLOCK_GetUart3ClkFreq   # -- Begin function CLOCK_GetUart3ClkFreq
	.p2align	4, 0x90
	.type	CLOCK_GetUart3ClkFreq,@function
CLOCK_GetUart3ClkFreq:                  # @CLOCK_GetUart3ClkFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	$0, -4(%rbp)
	movl	1074036892, %eax
	movq	%rax, %rcx
	subq	$4, %rcx
	ja	.LBB16_7
# %bb.1:
	movq	.LJTI16_0(,%rax,8), %rax
	jmpq	*%rax
.LBB16_2:
	callq	CLOCK_GetFroFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB16_8
.LBB16_3:
	callq	CLOCK_GetMainClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB16_8
.LBB16_4:
	callq	CLOCK_GetFRG0ClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB16_8
.LBB16_5:
	callq	CLOCK_GetFRG1ClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB16_8
.LBB16_6:
	callq	CLOCK_GetFroFreq
	shrl	$1, %eax
	movl	%eax, -4(%rbp)
	jmp	.LBB16_8
.LBB16_7:
	jmp	.LBB16_8
.LBB16_8:
	movl	-4(%rbp), %eax
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end16:
	.size	CLOCK_GetUart3ClkFreq, .Lfunc_end16-CLOCK_GetUart3ClkFreq
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI16_0:
	.quad	.LBB16_2
	.quad	.LBB16_3
	.quad	.LBB16_4
	.quad	.LBB16_5
	.quad	.LBB16_6
                                        # -- End function
	.text
	.globl	CLOCK_GetUart4ClkFreq   # -- Begin function CLOCK_GetUart4ClkFreq
	.p2align	4, 0x90
	.type	CLOCK_GetUart4ClkFreq,@function
CLOCK_GetUart4ClkFreq:                  # @CLOCK_GetUart4ClkFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	$0, -4(%rbp)
	movl	1074036896, %eax
	movq	%rax, %rcx
	subq	$4, %rcx
	ja	.LBB17_7
# %bb.1:
	movq	.LJTI17_0(,%rax,8), %rax
	jmpq	*%rax
.LBB17_2:
	callq	CLOCK_GetFroFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB17_8
.LBB17_3:
	callq	CLOCK_GetMainClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB17_8
.LBB17_4:
	callq	CLOCK_GetFRG0ClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB17_8
.LBB17_5:
	callq	CLOCK_GetFRG1ClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB17_8
.LBB17_6:
	callq	CLOCK_GetFroFreq
	shrl	$1, %eax
	movl	%eax, -4(%rbp)
	jmp	.LBB17_8
.LBB17_7:
	jmp	.LBB17_8
.LBB17_8:
	movl	-4(%rbp), %eax
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end17:
	.size	CLOCK_GetUart4ClkFreq, .Lfunc_end17-CLOCK_GetUart4ClkFreq
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI17_0:
	.quad	.LBB17_2
	.quad	.LBB17_3
	.quad	.LBB17_4
	.quad	.LBB17_5
	.quad	.LBB17_6
                                        # -- End function
	.text
	.globl	CLOCK_GetFreq           # -- Begin function CLOCK_GetFreq
	.p2align	4, 0x90
	.type	CLOCK_GetFreq,@function
CLOCK_GetFreq:                          # @CLOCK_GetFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	%edi, -8(%rbp)
	movl	-8(%rbp), %eax
	movq	%rax, %rcx
	subq	$8, %rcx
	ja	.LBB18_11
# %bb.1:
	movq	.LJTI18_0(,%rax,8), %rax
	jmpq	*%rax
.LBB18_2:
	callq	CLOCK_GetCoreSysClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB18_12
.LBB18_3:
	callq	CLOCK_GetMainClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB18_12
.LBB18_4:
	callq	CLOCK_GetFroFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB18_12
.LBB18_5:
	callq	CLOCK_GetFroFreq
	shrl	$1, %eax
	movl	%eax, -4(%rbp)
	jmp	.LBB18_12
.LBB18_6:
	callq	CLOCK_GetExtClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB18_12
.LBB18_7:
	callq	CLOCK_GetWdtOscFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB18_12
.LBB18_8:
	callq	CLOCK_GetSystemPLLFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB18_12
.LBB18_9:
	callq	CLOCK_GetFRG0ClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB18_12
.LBB18_10:
	callq	CLOCK_GetFRG1ClkFreq
	movl	%eax, -4(%rbp)
	jmp	.LBB18_12
.LBB18_11:
	movl	$0, -4(%rbp)
.LBB18_12:
	movl	-4(%rbp), %eax
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end18:
	.size	CLOCK_GetFreq, .Lfunc_end18-CLOCK_GetFreq
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI18_0:
	.quad	.LBB18_2
	.quad	.LBB18_3
	.quad	.LBB18_4
	.quad	.LBB18_5
	.quad	.LBB18_6
	.quad	.LBB18_8
	.quad	.LBB18_7
	.quad	.LBB18_9
	.quad	.LBB18_10
                                        # -- End function
	.text
	.p2align	4, 0x90         # -- Begin function CLOCK_GetCoreSysClkFreq
	.type	CLOCK_GetCoreSysClkFreq,@function
CLOCK_GetCoreSysClkFreq:                # @CLOCK_GetCoreSysClkFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	CLOCK_GetMainClkFreq
	movl	$1074036736, %ecx       # imm = 0x40048000
	movl	88(%rcx), %ecx
	andl	$255, %ecx
	xorl	%edx, %edx
	divl	%ecx
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end19:
	.size	CLOCK_GetCoreSysClkFreq, .Lfunc_end19-CLOCK_GetCoreSysClkFreq
	.cfi_endproc
                                        # -- End function
	.globl	CLOCK_InitSystemPll     # -- Begin function CLOCK_InitSystemPll
	.p2align	4, 0x90
	.type	CLOCK_InitSystemPll,@function
CLOCK_InitSystemPll:                    # @CLOCK_InitSystemPll
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	cmpl	$100000000, (%rax)      # imm = 0x5F5E100
	ja	.LBB20_2
# %bb.1:
	jmp	.LBB20_3
.LBB20_2:
	movabsq	$.L.str, %rdi
	movabsq	$.L.str.1, %rsi
	movl	$517, %edx              # imm = 0x205
	movabsq	$.L__PRETTY_FUNCTION__.CLOCK_InitSystemPll, %rcx
	callq	__assert_fail
.LBB20_3:
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	$1074036736, %edi       # imm = 0x40048000
	addq	$68, %rdi
	movl	$0, -8(%rbp)
	movl	$0, -12(%rbp)
	movl	$0, -4(%rbp)
	movl	568(%rax), %ecx
	orl	$128, %ecx
	movl	%ecx, 568(%rax)
	movl	64(%rax), %ecx
	andl	$-4, %ecx
	movq	-24(%rbp), %rdx
	orl	4(%rdx), %ecx
	movl	64(%rax), %edx
	orl	%ecx, %edx
	movl	%edx, 64(%rax)
	movl	$3, %esi
	callq	CLOCK_UpdateClkSrc
	callq	CLOCK_GetSystemPLLInClockRate
	movl	%eax, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.LBB20_5
# %bb.4:
	jmp	.LBB20_6
.LBB20_5:
	movabsq	$.L.str.2, %rdi
	movabsq	$.L.str.1, %rsi
	movl	$530, %edx              # imm = 0x212
	movabsq	$.L__PRETTY_FUNCTION__.CLOCK_InitSystemPll, %rcx
	callq	__assert_fail
.LBB20_6:
	movq	-24(%rbp), %rax
	movl	(%rax), %eax
	xorl	%edx, %edx
	divl	-4(%rbp)
	movl	%eax, -8(%rbp)
	movq	-24(%rbp), %rax
	movl	(%rax), %edi
	callq	findSyestemPllPsel
	movl	%eax, -12(%rbp)
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	8(%rax), %eax
	andl	$-128, %eax
	cmpl	$0, -8(%rbp)
	jne	.LBB20_8
# %bb.7:
	xorl	%ecx, %ecx
	jmp	.LBB20_9
.LBB20_8:
	movl	-8(%rbp), %ecx
	subl	$1, %ecx
.LBB20_9:
	shll	$0, %ecx
	andl	$31, %ecx
	orl	%ecx, %eax
	movl	-12(%rbp), %ecx
	shll	$5, %ecx
	andl	$96, %ecx
	orl	%ecx, %eax
	movl	$1074036736, %ecx       # imm = 0x40048000
	movl	%eax, 8(%rcx)
	movl	568(%rcx), %eax
	andl	$-129, %eax
	movl	%eax, 568(%rcx)
.LBB20_10:                              # =>This Inner Loop Header: Depth=1
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	12(%rax), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	jne	.LBB20_12
# %bb.11:                               #   in Loop: Header=BB20_10 Depth=1
	jmp	.LBB20_10
.LBB20_12:
	addq	$32, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end20:
	.size	CLOCK_InitSystemPll, .Lfunc_end20-CLOCK_InitSystemPll
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90         # -- Begin function CLOCK_UpdateClkSrc
	.type	CLOCK_UpdateClkSrc,@function
CLOCK_UpdateClkSrc:                     # @CLOCK_UpdateClkSrc
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -16(%rbp)
	movl	%esi, -4(%rbp)
	cmpq	$0, -16(%rbp)
	je	.LBB21_2
# %bb.1:
	jmp	.LBB21_3
.LBB21_2:
	movabsq	$.L.str.6, %rdi
	movabsq	$.L.str.1, %rsi
	movl	$101, %edx
	movabsq	$.L__PRETTY_FUNCTION__.CLOCK_UpdateClkSrc, %rcx
	callq	__assert_fail
.LBB21_3:
	movl	-4(%rbp), %eax
	xorl	$-1, %eax
	movq	-16(%rbp), %rcx
	movl	(%rcx), %edx
	andl	%eax, %edx
	movl	%edx, (%rcx)
	movl	-4(%rbp), %eax
	movq	-16(%rbp), %rcx
	movl	(%rcx), %edx
	orl	%eax, %edx
	movl	%edx, (%rcx)
.LBB21_4:                               # =>This Inner Loop Header: Depth=1
	movq	-16(%rbp), %rax
	movl	(%rax), %eax
	andl	-4(%rbp), %eax
	cmpl	$0, %eax
	jne	.LBB21_6
# %bb.5:                                #   in Loop: Header=BB21_4 Depth=1
	jmp	.LBB21_4
.LBB21_6:
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end21:
	.size	CLOCK_UpdateClkSrc, .Lfunc_end21-CLOCK_UpdateClkSrc
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90         # -- Begin function findSyestemPllPsel
	.type	findSyestemPllPsel,@function
findSyestemPllPsel:                     # @findSyestemPllPsel
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movl	%edi, -8(%rbp)
	movl	$0, -4(%rbp)
	cmpl	$78000000, -8(%rbp)     # imm = 0x4A62F80
	jbe	.LBB22_2
# %bb.1:
	movl	$0, -4(%rbp)
	jmp	.LBB22_9
.LBB22_2:
	cmpl	$39000000, -8(%rbp)     # imm = 0x25317C0
	jbe	.LBB22_4
# %bb.3:
	movl	$1, -4(%rbp)
	jmp	.LBB22_8
.LBB22_4:
	cmpl	$19500000, -8(%rbp)     # imm = 0x1298BE0
	jbe	.LBB22_6
# %bb.5:
	movl	$2, -4(%rbp)
	jmp	.LBB22_7
.LBB22_6:
	movl	$3, -4(%rbp)
.LBB22_7:
	jmp	.LBB22_8
.LBB22_8:
	jmp	.LBB22_9
.LBB22_9:
	movl	-4(%rbp), %eax
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end22:
	.size	findSyestemPllPsel, .Lfunc_end22-findSyestemPllPsel
	.cfi_endproc
                                        # -- End function
	.globl	CLOCK_InitExtClkin      # -- Begin function CLOCK_InitExtClkin
	.p2align	4, 0x90
	.type	CLOCK_InitExtClkin,@function
CLOCK_InitExtClkin:                     # @CLOCK_InitExtClkin
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	$1074020352, %eax       # imm = 0x40044000
	movl	44(%rax), %ecx
	andl	$-25, %ecx
	movl	%ecx, 44(%rax)
	movl	$1073790976, %eax       # imm = 0x4000C000
	movl	448(%rax), %ecx
	andl	$-1025, %ecx            # imm = 0xFBFF
	movl	%ecx, 448(%rax)
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	116(%rax), %ecx
	orl	$1, %ecx
	movl	%ecx, 116(%rax)
	movl	-4(%rbp), %eax
	movl	%eax, g_Ext_Clk_Freq
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end23:
	.size	CLOCK_InitExtClkin, .Lfunc_end23-CLOCK_InitExtClkin
	.cfi_endproc
                                        # -- End function
	.globl	CLOCK_InitXtalin        # -- Begin function CLOCK_InitXtalin
	.p2align	4, 0x90
	.type	CLOCK_InitXtalin,@function
CLOCK_InitXtalin:                       # @CLOCK_InitXtalin
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	movl	%edi, -16(%rbp)
	movl	$0, -12(%rbp)
	movl	$1074020352, %eax       # imm = 0x40044000
	movl	56(%rax), %ecx
	andl	$-25, %ecx
	movl	%ecx, 56(%rax)
	movl	$1073790976, %eax       # imm = 0x4000C000
	movl	448(%rax), %ecx
	andl	$-129, %ecx
	movl	%ecx, 448(%rax)
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	32(%rax), %ecx
	orl	$1, %ecx
	movl	%ecx, 32(%rax)
	movl	116(%rax), %ecx
	andl	$-2, %ecx
	movl	%ecx, 116(%rax)
	movl	568(%rax), %ecx
	andl	$-33, %ecx
	movl	%ecx, 568(%rax)
	movl	$0, -12(%rbp)
.LBB24_1:                               # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %eax
	cmpl	$1500, %eax             # imm = 0x5DC
	jae	.LBB24_4
# %bb.2:                                #   in Loop: Header=BB24_1 Depth=1
	#APP
	nop
	#NO_APP
# %bb.3:                                #   in Loop: Header=BB24_1 Depth=1
	movl	-12(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -12(%rbp)
	jmp	.LBB24_1
.LBB24_4:
	movl	-16(%rbp), %eax
	movl	%eax, g_Ext_Clk_Freq(%rip)
	movq	%fs:40, %rax
	movq	-8(%rbp), %rcx
	cmpq	%rcx, %rax
	jne	.LBB24_6
# %bb.5:                                # %SP_return
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.LBB24_6:                               # %CallStackCheckFailBlk
	.cfi_def_cfa %rbp, 16
	callq	__stack_chk_fail
.Lfunc_end24:
	.size	CLOCK_InitXtalin, .Lfunc_end24-CLOCK_InitXtalin
	.cfi_endproc
                                        # -- End function
	.globl	CLOCK_InitSysOsc        # -- Begin function CLOCK_InitSysOsc
	.p2align	4, 0x90
	.type	CLOCK_InitSysOsc,@function
CLOCK_InitSysOsc:                       # @CLOCK_InitSysOsc
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	xorl	%eax, %eax
	movq	%fs:40, %rcx
	movq	%rcx, -8(%rbp)
	movl	%edi, -16(%rbp)
	movl	$0, -12(%rbp)
	movl	$1074020352, %ecx       # imm = 0x40044000
	movl	52(%rcx), %edx
	andl	$-25, %edx
	movl	%edx, 52(%rcx)
	movl	56(%rcx), %edx
	andl	$-25, %edx
	movl	%edx, 56(%rcx)
	movl	$1073790976, %ecx       # imm = 0x4000C000
	movl	448(%rcx), %edx
	andl	$-385, %edx             # imm = 0xFE7F
	movl	%edx, 448(%rcx)
	movl	$1074036736, %ecx       # imm = 0x40048000
	movl	32(%rcx), %edx
	andl	$-4, %edx
	movl	-16(%rbp), %esi
	cmpl	$15000000, %esi         # imm = 0xE4E1C0
	movl	$2, %esi
	cmoval	%esi, %eax
	orl	%eax, %edx
	movl	32(%rcx), %eax
	orl	%edx, %eax
	movl	%eax, 32(%rcx)
	movl	116(%rcx), %eax
	andl	$-2, %eax
	movl	%eax, 116(%rcx)
	movl	568(%rcx), %eax
	andl	$-33, %eax
	movl	%eax, 568(%rcx)
	movl	$0, -12(%rbp)
.LBB25_1:                               # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp), %eax
	cmpl	$1500, %eax             # imm = 0x5DC
	jae	.LBB25_4
# %bb.2:                                #   in Loop: Header=BB25_1 Depth=1
	#APP
	nop
	#NO_APP
# %bb.3:                                #   in Loop: Header=BB25_1 Depth=1
	movl	-12(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -12(%rbp)
	jmp	.LBB25_1
.LBB25_4:
	movl	-16(%rbp), %eax
	movl	%eax, g_Ext_Clk_Freq(%rip)
	movq	%fs:40, %rax
	movq	-8(%rbp), %rcx
	cmpq	%rcx, %rax
	jne	.LBB25_6
# %bb.5:                                # %SP_return
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.LBB25_6:                               # %CallStackCheckFailBlk
	.cfi_def_cfa %rbp, 16
	callq	__stack_chk_fail
.Lfunc_end25:
	.size	CLOCK_InitSysOsc, .Lfunc_end25-CLOCK_InitSysOsc
	.cfi_endproc
                                        # -- End function
	.globl	CLOCK_InitWdtOsc        # -- Begin function CLOCK_InitWdtOsc
	.p2align	4, 0x90
	.type	CLOCK_InitWdtOsc,@function
CLOCK_InitWdtOsc:                       # @CLOCK_InitWdtOsc
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	%edi, -12(%rbp)
	movl	%esi, -8(%rbp)
	cmpl	$2, -8(%rbp)
	jb	.LBB26_2
# %bb.1:
	jmp	.LBB26_3
.LBB26_2:
	movabsq	$.L.str.3, %rdi
	movabsq	$.L.str.1, %rsi
	movl	$640, %edx              # imm = 0x280
	movabsq	$.L__PRETTY_FUNCTION__.CLOCK_InitWdtOsc, %rcx
	callq	__assert_fail
.LBB26_3:
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	36(%rax), %ecx
	movl	%ecx, -4(%rbp)
	movl	-4(%rbp), %ecx
	andl	$-512, %ecx             # imm = 0xFE00
	movl	%ecx, -4(%rbp)
	movl	-8(%rbp), %ecx
	shrl	$1, %ecx
	subl	$1, %ecx
	shll	$0, %ecx
	andl	$31, %ecx
	movl	-12(%rbp), %edx
	shrl	$24, %edx
	andl	$255, %edx
	shll	$5, %edx
	andl	$480, %edx              # imm = 0x1E0
	orl	%edx, %ecx
	orl	-4(%rbp), %ecx
	movl	%ecx, -4(%rbp)
	movl	-4(%rbp), %ecx
	movl	%ecx, 36(%rax)
	movl	568(%rax), %ecx
	andl	$-65, %ecx
	movl	%ecx, 568(%rax)
	movl	-12(%rbp), %eax
	andl	$16777215, %eax         # imm = 0xFFFFFF
	xorl	%edx, %edx
	divl	-8(%rbp)
	movl	%eax, g_Wdt_Osc_Freq
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end26:
	.size	CLOCK_InitWdtOsc, .Lfunc_end26-CLOCK_InitWdtOsc
	.cfi_endproc
                                        # -- End function
	.globl	CLOCK_SetMainClkSrc     # -- Begin function CLOCK_SetMainClkSrc
	.p2align	4, 0x90
	.type	CLOCK_SetMainClkSrc,@function
CLOCK_SetMainClkSrc:                    # @CLOCK_SetMainClkSrc
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	%edi, -12(%rbp)
	movl	-12(%rbp), %eax
	andl	$255, %eax
	movl	%eax, -4(%rbp)
	movl	-12(%rbp), %eax
	shrl	$8, %eax
	andl	$255, %eax
	movl	%eax, -8(%rbp)
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	80(%rax), %eax
	andl	$3, %eax
	cmpl	-8(%rbp), %eax
	je	.LBB27_3
# %bb.1:
	cmpl	$0, -4(%rbp)
	jne	.LBB27_3
# %bb.2:
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	$1074036736, %edi       # imm = 0x40048000
	addq	$84, %rdi
	movl	80(%rax), %ecx
	andl	$-4, %ecx
	movl	-8(%rbp), %edx
	shll	$0, %edx
	andl	$3, %edx
	orl	%edx, %ecx
	movl	%ecx, 80(%rax)
	movl	$1, %esi
	callq	CLOCK_UpdateClkSrc
.LBB27_3:
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	72(%rax), %eax
	andl	$3, %eax
	cmpl	-4(%rbp), %eax
	je	.LBB27_5
# %bb.4:
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	$1074036736, %edi       # imm = 0x40048000
	addq	$76, %rdi
	movl	72(%rax), %ecx
	andl	$-4, %ecx
	movl	-4(%rbp), %edx
	shll	$0, %edx
	andl	$3, %edx
	orl	%edx, %ecx
	movl	%ecx, 72(%rax)
	movl	$1, %esi
	callq	CLOCK_UpdateClkSrc
.LBB27_5:
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end27:
	.size	CLOCK_SetMainClkSrc, .Lfunc_end27-CLOCK_SetMainClkSrc
	.cfi_endproc
                                        # -- End function
	.globl	CLOCK_SetFroOutClkSrc   # -- Begin function CLOCK_SetFroOutClkSrc
	.p2align	4, 0x90
	.type	CLOCK_SetFroOutClkSrc,@function
CLOCK_SetFroOutClkSrc:                  # @CLOCK_SetFroOutClkSrc
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %eax
	movl	$1074036736, %ecx       # imm = 0x40048000
	movl	40(%rcx), %ecx
	andl	$131072, %ecx           # imm = 0x20000
	cmpl	%ecx, %eax
	je	.LBB28_2
# %bb.1:
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	$1074036736, %edi       # imm = 0x40048000
	addq	$48, %rdi
	movl	40(%rax), %ecx
	andl	$-131073, %ecx          # imm = 0xFFFDFFFF
	orl	-4(%rbp), %ecx
	movl	%ecx, 40(%rax)
	movl	$1, %esi
	callq	CLOCK_UpdateClkSrc
.LBB28_2:
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end28:
	.size	CLOCK_SetFroOutClkSrc, .Lfunc_end28-CLOCK_SetFroOutClkSrc
	.cfi_endproc
                                        # -- End function
	.weak	SDK_DelayAtLeastUs      # -- Begin function SDK_DelayAtLeastUs
	.p2align	4, 0x90
	.type	SDK_DelayAtLeastUs,@function
SDK_DelayAtLeastUs:                     # @SDK_DelayAtLeastUs
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	xorl	%eax, %eax
	movl	%edi, -8(%rbp)
	cmpl	-8(%rbp), %eax
	je	.LBB29_2
# %bb.1:
	jmp	.LBB29_3
.LBB29_2:
	movabsq	$.L.str.4, %rdi
	movabsq	$.L.str.1, %rsi
	movl	$702, %edx              # imm = 0x2BE
	movabsq	$.L__PRETTY_FUNCTION__.SDK_DelayAtLeastUs, %rcx
	callq	__assert_fail
.LBB29_3:
	movl	-8(%rbp), %eax
	imulq	$30000000, %rax, %rax   # imm = 0x1C9C380
	xorl	%edx, %edx
	movl	$1000000, %ecx          # imm = 0xF4240
	divq	%rcx
	movl	%eax, -4(%rbp)
	movl	-4(%rbp), %eax
	shrl	$2, %eax
	movl	%eax, -4(%rbp)
.LBB29_4:                               # =>This Inner Loop Header: Depth=1
	movl	-4(%rbp), %eax
	cmpq	$0, %rax
	jbe	.LBB29_7
# %bb.5:                                #   in Loop: Header=BB29_4 Depth=1
	#APP
	nop
	#NO_APP
# %bb.6:                                #   in Loop: Header=BB29_4 Depth=1
	movl	-4(%rbp), %eax
	addl	$-1, %eax
	movl	%eax, -4(%rbp)
	jmp	.LBB29_4
.LBB29_7:
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end29:
	.size	SDK_DelayAtLeastUs, .Lfunc_end29-SDK_DelayAtLeastUs
	.cfi_endproc
                                        # -- End function
	.globl	BOARD_InitBootClocks    # -- Begin function BOARD_InitBootClocks
	.p2align	4, 0x90
	.type	BOARD_InitBootClocks,@function
BOARD_InitBootClocks:                   # @BOARD_InitBootClocks
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	callq	BOARD_BootClockFRO18M
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end30:
	.size	BOARD_InitBootClocks, .Lfunc_end30-BOARD_InitBootClocks
	.cfi_endproc
                                        # -- End function
	.globl	BOARD_BootClockFRO18M   # -- Begin function BOARD_BootClockFRO18M
	.p2align	4, 0x90
	.type	BOARD_BootClockFRO18M,@function
BOARD_BootClockFRO18M:                  # @BOARD_BootClockFRO18M
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movl	$1, %edi
	callq	POWER_DisablePD
	movl	$2, %edi
	callq	POWER_DisablePD
	movl	$18000, %edi            # imm = 0x4650
	callq	CLOCK_SetFroOscFreq
	movl	$131072, %edi           # imm = 0x20000
	callq	CLOCK_SetFroOutClkSrc
	movl	$32, %edi
	callq	POWER_DisablePD
	movl	$29696, %edi            # imm = 0x7400
	callq	CLOCK_Select
	xorl	%edi, %edi
	callq	CLOCK_SetMainClkSrc
	movl	$1, %edi
	callq	CLOCK_SetCoreSysClkDiv
	movl	$18000000, SystemCoreClock # imm = 0x112A880
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end31:
	.size	BOARD_BootClockFRO18M, .Lfunc_end31-BOARD_BootClockFRO18M
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90         # -- Begin function POWER_DisablePD
	.type	POWER_DisablePD,@function
POWER_DisablePD:                        # @POWER_DisablePD
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %eax
	xorl	$-1, %eax
	movl	$1074036736, %ecx       # imm = 0x40048000
	movl	568(%rcx), %edx
	andl	%eax, %edx
	movl	%edx, 568(%rcx)
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end32:
	.size	POWER_DisablePD, .Lfunc_end32-POWER_DisablePD
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90         # -- Begin function CLOCK_SetFroOscFreq
	.type	CLOCK_SetFroOscFreq,@function
CLOCK_SetFroOscFreq:                    # @CLOCK_SetFroOscFreq
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %edi
	movl	$251668213, %eax        # imm = 0xF0026F5
	callq	*%rax
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end33:
	.size	CLOCK_SetFroOscFreq, .Lfunc_end33-CLOCK_SetFroOscFreq
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90         # -- Begin function CLOCK_Select
	.type	CLOCK_Select,@function
CLOCK_Select:                           # @CLOCK_Select
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %ecx
	andl	$255, %ecx
	movl	-4(%rbp), %edx
	shrl	$8, %edx
	andl	$255, %edx
	addl	%edx, %eax
	movl	%eax, %eax
	movl	%ecx, (%rax)
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end34:
	.size	CLOCK_Select, .Lfunc_end34-CLOCK_Select
	.cfi_endproc
                                        # -- End function
	.p2align	4, 0x90         # -- Begin function CLOCK_SetCoreSysClkDiv
	.type	CLOCK_SetCoreSysClkDiv,@function
CLOCK_SetCoreSysClkDiv:                 # @CLOCK_SetCoreSysClkDiv
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.LBB35_2
# %bb.1:
	jmp	.LBB35_3
.LBB35_2:
	movabsq	$.L.str.7, %rdi
	movabsq	$.L.str.1.8, %rsi
	movl	$497, %edx              # imm = 0x1F1
	movabsq	$.L__PRETTY_FUNCTION__.CLOCK_SetCoreSysClkDiv, %rcx
	callq	__assert_fail
.LBB35_3:
	movl	$1074036736, %eax       # imm = 0x40048000
	movl	88(%rax), %ecx
	andl	$-256, %ecx
	movl	-4(%rbp), %edx
	shll	$0, %edx
	andl	$255, %edx
	orl	%edx, %ecx
	movl	%ecx, 88(%rax)
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end35:
	.size	CLOCK_SetCoreSysClkDiv, .Lfunc_end35-CLOCK_SetCoreSysClkDiv
	.cfi_endproc
                                        # -- End function
	.globl	BOARD_BootClockFRO24M   # -- Begin function BOARD_BootClockFRO24M
	.p2align	4, 0x90
	.type	BOARD_BootClockFRO24M,@function
BOARD_BootClockFRO24M:                  # @BOARD_BootClockFRO24M
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movl	$1, %edi
	callq	POWER_DisablePD
	movl	$2, %edi
	callq	POWER_DisablePD
	movl	$24000, %edi            # imm = 0x5DC0
	callq	CLOCK_SetFroOscFreq
	movl	$131072, %edi           # imm = 0x20000
	callq	CLOCK_SetFroOutClkSrc
	movl	$32, %edi
	callq	POWER_DisablePD
	movl	$29696, %edi            # imm = 0x7400
	callq	CLOCK_Select
	xorl	%edi, %edi
	callq	CLOCK_SetMainClkSrc
	movl	$1, %edi
	callq	CLOCK_SetCoreSysClkDiv
	movl	$24000000, SystemCoreClock # imm = 0x16E3600
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end36:
	.size	BOARD_BootClockFRO24M, .Lfunc_end36-BOARD_BootClockFRO24M
	.cfi_endproc
                                        # -- End function
	.globl	BOARD_BootClockFRO30M   # -- Begin function BOARD_BootClockFRO30M
	.p2align	4, 0x90
	.type	BOARD_BootClockFRO30M,@function
BOARD_BootClockFRO30M:                  # @BOARD_BootClockFRO30M
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movl	$1, %edi
	callq	POWER_DisablePD
	movl	$2, %edi
	callq	POWER_DisablePD
	movl	$30000, %edi            # imm = 0x7530
	callq	CLOCK_SetFroOscFreq
	movl	$131072, %edi           # imm = 0x20000
	callq	CLOCK_SetFroOutClkSrc
	movl	$32, %edi
	callq	POWER_DisablePD
	movl	$29696, %edi            # imm = 0x7400
	callq	CLOCK_Select
	xorl	%edi, %edi
	callq	CLOCK_SetMainClkSrc
	movl	$1, %edi
	callq	CLOCK_SetCoreSysClkDiv
	movl	$30000000, SystemCoreClock # imm = 0x1C9C380
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.Lfunc_end37:
	.size	BOARD_BootClockFRO30M, .Lfunc_end37-BOARD_BootClockFRO30M
	.cfi_endproc
                                        # -- End function
	.globl	BOARD_BootClockPll24M   # -- Begin function BOARD_BootClockPll24M
	.p2align	4, 0x90
	.type	BOARD_BootClockPll24M,@function
BOARD_BootClockPll24M:                  # @BOARD_BootClockPll24M
	.cfi_startproc
# %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	movl	$1, %edi
	callq	POWER_DisablePD
	movl	$2, %edi
	callq	POWER_DisablePD
	movl	$24000, %edi            # imm = 0x5DC0
	callq	CLOCK_SetFroOscFreq
	movl	$131072, %edi           # imm = 0x20000
	callq	CLOCK_SetFroOutClkSrc
	movl	$32, %edi
	callq	POWER_DisablePD
	movl	$29696, %edi            # imm = 0x7400
	callq	CLOCK_Select
	movl	$0, -12(%rbp)
	movl	$24000000, -16(%rbp)    # imm = 0x16E3600
	leaq	-16(%rbp), %rdi
	callq	CLOCK_InitSystemPll
	movl	$1, %edi
	callq	CLOCK_SetMainClkSrc
	movl	$1, %edi
	callq	CLOCK_SetCoreSysClkDiv
	movl	$24000000, SystemCoreClock(%rip) # imm = 0x16E3600
	movq	%fs:40, %rax
	movq	-8(%rbp), %rcx
	cmpq	%rcx, %rax
	jne	.LBB38_2
# %bb.1:                                # %SP_return
	addq	$16, %rsp
	popq	%rbp
	.cfi_def_cfa %rsp, 8
	retq
.LBB38_2:                               # %CallStackCheckFailBlk
	.cfi_def_cfa %rbp, 16
	callq	__stack_chk_fail
.Lfunc_end38:
	.size	BOARD_BootClockPll24M, .Lfunc_end38-BOARD_BootClockPll24M
	.cfi_endproc
                                        # -- End function
	.type	g_Ext_Clk_Freq,@object  # @g_Ext_Clk_Freq
	.bss
	.globl	g_Ext_Clk_Freq
	.p2align	2
g_Ext_Clk_Freq:
	.long	0                       # 0x0
	.size	g_Ext_Clk_Freq, 4

	.type	g_Wdt_Osc_Freq,@object  # @g_Wdt_Osc_Freq
	.globl	g_Wdt_Osc_Freq
	.p2align	2
g_Wdt_Osc_Freq:
	.long	0                       # 0x0
	.size	g_Wdt_Osc_Freq, 4

	.type	.L.str.5,@object        # @.str.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.5:
	.asciz	"freq"
	.size	.L.str.5, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"fsl_clock.c"
	.size	.L.str.1, 12

	.type	.L__PRETTY_FUNCTION__.CLOCK_SetFRGClkFreq,@object # @__PRETTY_FUNCTION__.CLOCK_SetFRGClkFreq
.L__PRETTY_FUNCTION__.CLOCK_SetFRGClkFreq:
	.asciz	"_Bool CLOCK_SetFRGClkFreq(uint32_t *, uint32_t)"
	.size	.L__PRETTY_FUNCTION__.CLOCK_SetFRGClkFreq, 48

	.type	.L.str,@object          # @.str
.L.str:
	.asciz	"config->targetFreq <= SYSPLL_MAX_OUTPUT_FREQ_HZ"
	.size	.L.str, 48

	.type	.L__PRETTY_FUNCTION__.CLOCK_InitSystemPll,@object # @__PRETTY_FUNCTION__.CLOCK_InitSystemPll
.L__PRETTY_FUNCTION__.CLOCK_InitSystemPll:
	.asciz	"void CLOCK_InitSystemPll(const clock_sys_pll_t *)"
	.size	.L__PRETTY_FUNCTION__.CLOCK_InitSystemPll, 50

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"inputFreq != 0U"
	.size	.L.str.2, 16

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"base"
	.size	.L.str.6, 5

	.type	.L__PRETTY_FUNCTION__.CLOCK_UpdateClkSrc,@object # @__PRETTY_FUNCTION__.CLOCK_UpdateClkSrc
.L__PRETTY_FUNCTION__.CLOCK_UpdateClkSrc:
	.asciz	"void CLOCK_UpdateClkSrc(volatile uint32_t *, uint32_t)"
	.size	.L__PRETTY_FUNCTION__.CLOCK_UpdateClkSrc, 55

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"wdtOscDiv >= 2U"
	.size	.L.str.3, 16

	.type	.L__PRETTY_FUNCTION__.CLOCK_InitWdtOsc,@object # @__PRETTY_FUNCTION__.CLOCK_InitWdtOsc
.L__PRETTY_FUNCTION__.CLOCK_InitWdtOsc:
	.asciz	"void CLOCK_InitWdtOsc(clock_wdt_analog_freq_t, uint32_t)"
	.size	.L__PRETTY_FUNCTION__.CLOCK_InitWdtOsc, 57

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"0U != delay_us"
	.size	.L.str.4, 15

	.type	.L__PRETTY_FUNCTION__.SDK_DelayAtLeastUs,@object # @__PRETTY_FUNCTION__.SDK_DelayAtLeastUs
.L__PRETTY_FUNCTION__.SDK_DelayAtLeastUs:
	.asciz	"void SDK_DelayAtLeastUs(uint32_t)"
	.size	.L__PRETTY_FUNCTION__.SDK_DelayAtLeastUs, 34

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"value != 0U"
	.size	.L.str.7, 12

	.type	.L.str.1.8,@object      # @.str.1.8
.L.str.1.8:
	.asciz	"./fsl_clock.h"
	.size	.L.str.1.8, 14

	.type	.L__PRETTY_FUNCTION__.CLOCK_SetCoreSysClkDiv,@object # @__PRETTY_FUNCTION__.CLOCK_SetCoreSysClkDiv
.L__PRETTY_FUNCTION__.CLOCK_SetCoreSysClkDiv:
	.asciz	"void CLOCK_SetCoreSysClkDiv(uint32_t)"
	.size	.L__PRETTY_FUNCTION__.CLOCK_SetCoreSysClkDiv, 38


	.ident	"clang version 9.0.0 (tags/RELEASE_900/final)"
	.ident	"clang version 9.0.0 (tags/RELEASE_900/final)"
	.section	".note.GNU-stack","",@progbits
