################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../startup/startup_mk22f51212.c 

OBJS += \
./startup/startup_mk22f51212.o 

C_DEPS += \
./startup/startup_mk22f51212.d 


# Each subdirectory must supply rules for building sources it contributes
startup/%.o: ../startup/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DCPU_MK22FN512VLH12 -DCPU_MK22FN512VLH12_cm4 -DFSL_RTOS_BM -DSDK_OS_BAREMETAL -DSDK_DEBUGCONSOLE=0 -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -DSERIAL_PORT_TYPE_UART=1 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"/home/vladorf/Documents/MCUXpresso_11.1.0_3209/workspace/MK22FN512xxx12_Project/board" -I"/home/vladorf/Documents/MCUXpresso_11.1.0_3209/workspace/MK22FN512xxx12_Project/source" -I"/home/vladorf/Documents/MCUXpresso_11.1.0_3209/workspace/MK22FN512xxx12_Project" -I"/home/vladorf/Documents/MCUXpresso_11.1.0_3209/workspace/MK22FN512xxx12_Project/drivers" -I"/home/vladorf/Documents/MCUXpresso_11.1.0_3209/workspace/MK22FN512xxx12_Project/device" -I"/home/vladorf/Documents/MCUXpresso_11.1.0_3209/workspace/MK22FN512xxx12_Project/CMSIS" -I"/home/vladorf/Documents/MCUXpresso_11.1.0_3209/workspace/MK22FN512xxx12_Project/component/uart" -I"/home/vladorf/Documents/MCUXpresso_11.1.0_3209/workspace/MK22FN512xxx12_Project/utilities" -I"/home/vladorf/Documents/MCUXpresso_11.1.0_3209/workspace/MK22FN512xxx12_Project/component/serial_manager" -I"/home/vladorf/Documents/MCUXpresso_11.1.0_3209/workspace/MK22FN512xxx12_Project/component/lists" -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


