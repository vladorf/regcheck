from setuptools import setup


with open('requirements.txt') as req:
    install_requires = req.read().splitlines()


setup(
    name="regcheck",
    version="0.0.0",
    description="Regcheck is tool for analyzing RW operations in C code used to control NXP MCUs",
    maintainer='Vladimir Uzik',
    maintainer_email='xuzikv00@stud.fit.vutbr.cz',
    packages=["regcheck", "fake_libc_include"],
    include_package_data=True,
    package_data={"fake_libc_include": ["*.h", "**/*.h"]},
    setup_requires=["wheel"],
    tests_require=["pytest"],
    install_requires=install_requires,
    python_requires='>=3.7',
    license='MIT'
)
