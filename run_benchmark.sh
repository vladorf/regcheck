#!/bin/sh

# Commands used to produce results in benchmarking section of bachelor thesis
# Could be condensed to one command after https://github.com/sharkdp/hyperfine/issues/273 will be solved
# running under root may be needed
printf "\n --- Benchmarking no-cache run --- \n\n"
chrt --fifo 99 hyperfine --runs 100 --parameter-list routine BOARD_InitBootPins,BOARD_InitBootClocks "python -m regcheck {routine} --config examples/LPC845_Project/regcheck.ini --no-cache"
chrt --fifo 99 hyperfine --runs 100 --parameter-list routine BOARD_InitBootPins,BOARD_InitBootClocks "python -m regcheck {routine} --config examples/MK22FN512xxx12_Project/regcheck.ini --no-cache"

printf "\n --- Benchmarking cache run --- \n\n"
chrt --fifo 99 hyperfine --runs 100 --parameter-list routine BOARD_InitBootPins,BOARD_InitBootClocks --warmup 1 "python -m regcheck {routine} --config examples/LPC845_Project/regcheck.ini"
chrt --fifo 99 hyperfine --runs 100 --parameter-list routine BOARD_InitBootPins,BOARD_InitBootClocks --warmup 1 "python -m regcheck {routine} --config examples/MK22FN512xxx12_Project/regcheck.ini"
