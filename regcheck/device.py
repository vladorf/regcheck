"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Module containing mapping of XML manifest file to objects

Class hierarchy:
Device -> Peripheral -> Cluster (optionally) -> Register

"""
from __future__ import annotations
import xml.etree.ElementTree as ET


class PeripheralNotFound(Exception):
    """Given adddress does not have associated any peripheral."""
    def __init__(self, address):
        super().__init__(f"Peripheral belonging to address {hex(address)} was not found")
        self.address = address


class RegisterNotFound(Exception):
    """No register associated with given peripheral, offset combination."""
    def __init__(self, peripheral, offset):
        super().__init__(
            f"Register belonging to {peripheral.name} on offset {offset} was not found"
        )
        self.peripheral = peripheral
        self.offset = offset


def _resolve(element, path):
    """Shortcut function to access subelement value"""
    return element.find(path).text


def _coerce_int(value):
    """Coerce int value from either decimal or hexadecimal."""
    if value.startswith("0x"):
        return int(value, base=16)
    return int(value)


class Device:
    """
    Entrypoint to manifest file, contains list of peripherals.
    """
    def __init__(self, device_file_path):
        tree = ET.parse(device_file_path)
        self.root = tree.getroot()
        self.peripherals = []

        peripherals_path = "peripherals/peripheral"
        for xml_peripheral in self.root.iterfind(peripherals_path):
            self.peripherals.append(Peripheral(xml_peripheral))

    def get_peripheral(self, address):
        """Return peripheral object for given address."""
        for periph in self.peripherals:
            if periph.base_address <= address <= periph.base_address + periph.size:
                return periph

        raise PeripheralNotFound(address)

    def get_register(self, address):
        """Return register object for given address."""
        return self.get_peripheral(address).get_register(address)

    def get_name_parts(self, address):
        """
        List containing names corresponding to given address.
        
        Length of list depends on how much of a nested structure
        is associated with given address. There are at least 2 names
        top-level structure name and field name.
        """
        periph = self.get_peripheral(address)
        register = periph.get_register(address)
        cluster = register.parent_cluster

        name_parts = [periph.name, register.name]
        if cluster:
            name_parts.insert(1, cluster.name)

        return name_parts


class Peripheral:
    """
    Class encapsulating Peripheral element.

    Contains list of registers
    """
    def __init__(self, peripheral_element):
        self.name = peripheral_element.find("name").text
        self.base_address = _coerce_int(_resolve(peripheral_element, "baseAddress"))
        self.offset = _coerce_int(_resolve(peripheral_element, "addressBlock/offset"))
        self.size = _coerce_int(_resolve(peripheral_element, "addressBlock/size")) + self.offset

        self.registers = []
        for xml_register in peripheral_element.iterfind("registers/register"):
            registers = self._create_register_objects(xml_register)
            self.registers.extend(registers)

        # cluster is nested struct
        for xml_cluster in peripheral_element.iterfind("registers/cluster"):

            if xml_cluster.find("dim") is not None:
                for index in range(_coerce_int(xml_cluster.find("dim").text)):
                    
                    cluster_obj = Cluster(xml_cluster, index)
                    for xml_register in xml_cluster.iterfind("register"):
                        registers = self._create_register_objects(xml_register, cluster_obj)
                        self.registers.extend(registers)
            
            else:
                cluster_obj = Cluster(xml_cluster, index)

                for xml_register in xml_cluster.iterfind("register"):
                    registers = self._create_register_objects(xml_register, cluster_obj)
                    self.registers.extend(registers)

    @staticmethod
    def _create_register_objects(xml_register, parent_cluster=None):
        """
        Helper method for creating Register objects, deals with array fields.
        """
        if xml_register.find("dim") is not None:
            for index in range(_coerce_int(xml_register.find("dim").text)):
                yield Register(xml_register, index, parent_cluster)
        else:
            yield Register(xml_register, None, parent_cluster)

    def get_register(self, address):
        """Return register for given address."""
        offset_in_periph = address - self.base_address
        
        for register in self.registers:
            if offset_in_periph == register.offset:
                return register

        raise RegisterNotFound(self, offset_in_periph)


class Register:
    """
    Class encapsulating Register element.

    Contains list of Bitfields.
    """
    def __init__(self, register_element, index=None, parent_cluster=None):

        # index is used when field is Array
        self.index = index
        # parent_cluster is used when register is nested in struct
        self.parent_cluster = parent_cluster

        self._raw_name = _resolve(register_element, "name")
        self._raw_offset = _coerce_int(_resolve(register_element, "addressOffset"))
        
        self.reset_value = _coerce_int(_resolve(register_element, "resetValue"))
        self.size = _coerce_int(_resolve(register_element, "size"))
        self.dim_increment = register_element.find("dimIncrement")
        
        if self.dim_increment is not None:
            self.dim_increment = _coerce_int(self.dim_increment.text)

        self.bitfields = []
        for xml_bitfield in register_element.iterfind("fields/field"):
            self.bitfields.append(Bitfield(xml_bitfield))

    @property
    def name(self):
        """Register field name."""
        if self.index is not None:
            # replace at most 1 occurence
            return self._raw_name.replace("%s", str(self.index), 1)
        return self._raw_name

    @property
    def offset(self):
        """Register offset from peripheral base address."""
        total = self._raw_offset
        
        if self.dim_increment:
            total += self.dim_increment * self.index

        if self.parent_cluster:
            total += self.parent_cluster.offset
        
        return total

    def bitfields_for(self, value: int):
        """Convert value for register into bitfield dictionary."""
        bitfields = {}
        # slice out `0b` prefix and left fill with zeroes to register size
        bits = bin(value)[2:].zfill(self.size)
        for bitfield in self.bitfields:
            # bitfield offset is defined from right side, not from left
            offset = self.size - bitfield.start
            bitfields[bitfield.name] = int(bits[offset - bitfield.size: offset], base=2)
            
        return bitfields


# if nesting is needed it can be added by introducing parent_cluster and iterate on them upwards
class Cluster:
    """
    Cluster describing nested structure.

    Currently supporting only one level of nesting.
    """
    def __init__(self, cluster_element, index=None):
        # index is used when field is Array
        self.index = index

        self.dim_increment = cluster_element.find("dimIncrement")

        self._raw_offset = _coerce_int(_resolve(cluster_element, "addressOffset"))
        self._raw_name = _resolve(cluster_element, "name")

        if self.dim_increment is not None:
            self.dim_increment = _coerce_int(self.dim_increment.text)

    @property
    def offset(self):
        """Cluster offset from peripheral base address."""
        if self.dim_increment:
            return self._raw_offset + self.dim_increment * self.index
        return self._raw_offset

    
    @property
    def name(self):
        """Cluster name, corresponds to field name under which cluster is nested."""
        if self.index is not None:
            # replace at most 1 occurence
            return self._raw_name.replace("%s", str(self.index), 1)
        return self._raw_name


class Bitfield:
    """Object describing one bitfield of Register."""
    def __init__(self, bitfield_element):
        self.name = _resolve(bitfield_element, "name")
        self.start = _coerce_int(_resolve(bitfield_element, "bitOffset"))
        self.size = _coerce_int(_resolve(bitfield_element, "bitWidth"))

