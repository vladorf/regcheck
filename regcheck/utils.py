"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Utils module.
"""
from __future__ import annotations

import hashlib
import operator
import os.path
import pickle
import tempfile
import ctypes
import subprocess
from functools import WRAPPER_ASSIGNMENTS, wraps
from typing import Any, Dict, List

from pycparser import c_ast

import regcheck.state as state
from regcheck.exceptions import NodeNotFoundError, StructFieldNotFoundError
from regcheck.values import Variable

# In `symbol_name_to_node` we are doing something quite awful -
# changing function prototype (subject to change)
# we have to do this in order to not copy annotations when using functools.wraps
WRAPPER_ASSIGNMENTS_NO_ANNOTATIONS = tuple(
    elem for elem in WRAPPER_ASSIGNMENTS if elem != "__annotations__"
)


def unwrap_typedef(node):
    """Return Node wrapped by typedef."""
    return node.type.type


def retrieve_enum_type(node, can_be_typedefed=True):
    """Determine whether given type is enum (or wrapped enum)."""
    if can_be_typedefed and type(node) == c_ast.Typedef:
        enum_node = unwrap_typedef(node)
        return enum_node if type(enum_node) == c_ast.Enum else None
    
    if type(node) == c_ast.Decl:
        return node.type if type(node.type) == c_ast.Enum else None


def retrieve_enum_symbols():
    """Source symbol table with all enum symbols"""
    # Circular import prevention
    from regcheck.interpreter import interpret, declare_var, CONV_TO_TYPE

    for node in state.ast:
        enum_node = retrieve_enum_type(node)
        if enum_node:
            last_value = ctypes.c_uint32(0)
            for enumerator in enum_node.values:
                value = interpret(enumerator.value) if enumerator.value else last_value
                type_ = CONV_TO_TYPE[type(value)] if not isinstance(value, Variable) else value.type_
                value = value if not isinstance(value, Variable) else state.memory[value.address]
                declare_var(enumerator.name, type_, value, is_global=True)
                last_value = value


def retrieve_global_variables():
    """Source symbol table with all global variables"""
    # Circular import prevention
    from regcheck.interpreter import interpret

    for node in state.ast:
        if type(node) == c_ast.Decl and type(node.type) == c_ast.TypeDecl:
            interpret(node)


def get_ast_from_files(
        files: List[str], parser_args: Dict[str, Any], use_cached: bool = True
) -> List[c_ast.Node]:
    """
    Returns AST object, supports on-disk caching.

    Args:
        files: source files from which AST should be built
        parser_args: additional arguments to `pycparser` library
        use_cached: whether to use cached AST representation if possible

    Returns:
        list of AST nodes
    """
    hash_algorithm = hashlib.md5()

    for file in files:
        with open(file, "rb") as f:
            hash_algorithm.update(f.read())

    cached_ast_path = os.path.join(
        tempfile.gettempdir(), f"regcheck_{hash_algorithm.hexdigest()}"
    )

    if use_cached and os.path.exists(cached_ast_path):
        with open(cached_ast_path, "rb") as f:
            return pickle.load(f)
    else:
        ast = []
        for file in files:
            ast.extend(parse_file(file, **parser_args).ext)

        with open(cached_ast_path, "wb") as f:
            pickle.dump(ast, f)

        return ast


def parse_file(filename, cpp_path, cpp_args, parser):
    """Parse given file and return AST."""
    path_list = [cpp_path]
    path_list.extend(cpp_args)
    path_list.append(filename)
    process = subprocess.run(path_list, check=True, capture_output=True)
    output = process.stdout.decode()
    new_out = output.replace("\r", "")  # MinGW adds carriage returns to newlines
    return parser.parse(new_out, filename)


def is_enum(name: str) -> bool:
    """
    Return true if given symbol is enum, False otherwise.

    So far supports typedefed enums only.

    Args:
        name: enum name

    Returns:
        True if name is enum, False otherwise
    """
    for node in state.ast:
        if (
                type(node) == c_ast.Typedef
                and type(node.type.type) == c_ast.Enum
                and node.name == name
            ):
            return True
    return False


def is_struct(name: str) -> bool:
    """
    Return true if given symbol is struct, False otherwise.

    Args:
        name: struct name

    Returns:
        True if name is struct, False otherwise
    """
    try:
        base_symbol, *rest = name.split(";")
        obj = get_struct_object(base_symbol)
        if rest:
            obj.fieldpath(";".join(rest))
        return True
    except TypeError:
        return False


def symbol_name_to_node(*, pass_symbol_name: bool = False):
    """Translate symbol name into subtree with definition of given symbol name.

    To find right subtree we need symbol name and symbol type. Symbol name is given as
    argument. Symbol type is detected from annotation (which produces constraint
    that all functions using this, have to have correctly annotated `node` argument
    which must also be first).
    """

    name_paths = {
        "FuncDef": "decl.name",
        "Enum": "name",
        "Struct": "name",
    }

    def name_path(node: c_ast.Node) -> str:
        """
        Resolve name of given node.

        Different nodes have their names assigned in different
        attributes / substructures. We might need to find them
        in using more than one path - that would require refactor.

        Note:
            Supported nodes are defined in NAME_PATHS,
            raises KeyError if we search for undefined node.
        """
        return operator.attrgetter(name_paths[node.__class__.__name__])(node)

    def decorator(func):
        @wraps(func, assigned=WRAPPER_ASSIGNMENTS_NO_ANNOTATIONS)
        def wrapper(symbol_name, *args, **kwargs):
            desired_type = eval(func.__annotations__["node"])  # pylint: disable=eval-used

            for node in state.ast:
                subtree = None
                if type(node) == c_ast.Typedef:
                    subtree = node.type
                    if type(subtree) == c_ast.TypeDecl and node.name == symbol_name:
                        subtree = subtree.type
                        break

                if type(node) == desired_type and name_path(node) == symbol_name:
                    subtree = node
                    break

            if not subtree:
                raise NodeNotFoundError(symbol_name)

            if type(subtree) != desired_type:
                raise TypeError(
                    f"Found and desired subtree types are mismatched, "
                    f"{type(subtree)} vs {desired_type} with type {symbol_name}"
                )

            if pass_symbol_name:
                return func(subtree, symbol_name, *args, **kwargs)
            return func(subtree, *args, **kwargs)

        return wrapper

    return decorator


@symbol_name_to_node(pass_symbol_name=True)
def get_enum_val_subtree(
        node: c_ast.Enum, symbol_name: str, fieldname: str) -> c_ast.Node:
    """
    Return subtree with definition of value of given field in given enum symbol name.

    Note:
        check symbol_name_to_node docstring for explanation where
        the `node` argument is coming from

    Args:
        node: enum node
        symbol_name: symbol name of enum (passed from decorator)
        fieldname: field name we want subtree for

    Returns:
        Subtree for given field in given enum
    """
    for field in node.values.enumerators:
        if field.name == fieldname:
            return field.value

    raise StructFieldNotFoundError(symbol_name, fieldname)


@symbol_name_to_node()
def get_function_def_subtree(node: c_ast.FuncDef) -> c_ast.FuncDef:
    """
    Return `FuncDef` subtree with given symbol name.

    Note:
        check symbol_name_to_node docstring for explanation where
        the `node` argument is coming from

    Args:
        node: FuncDef node

    Returns:
        Subtree of function definition for given function name
    """
    return node


@symbol_name_to_node(pass_symbol_name=True)
def get_struct_object(node: c_ast.Struct, symbol_name: str) -> Struct:
    """
    Return Struct object for given struct name

    Note:
        check symbol_name_to_node docstring for explanation where
        the `node` argument is coming from

    Args:
        node: Struct node
        symbol_name: symbol name of enum (passed from decorator)

    Returns:
        Struct object for given struct name
    """
    # Circular import prevention
    from regcheck.structs import Struct  # pylint: disable=import-outside-toplevel
    return Struct(node, symbol_name)


def resolve_implicit_conversion(type1, type2):
    """
    Resolve which type we should convert given 2 types as arguments

    https://en.cppreference.com/w/c/language/conversion
    """
    rules = (
        ({ctypes.c_int, ctypes.c_uint}, ctypes.c_uint),
        ({ctypes.c_uint, ctypes.c_ulong}, ctypes.c_ulong),
        ({ctypes.c_uint, ctypes.c_ubyte}, ctypes.c_uint),
    )

    if isinstance(type1, Variable):
        type1 = state.memory[type1.address]

    if isinstance(type2, Variable):
        type2 = state.memory[type2.address]

    if type1.__name__ == type2.__name__:
        return type1

    for (rule, resulting_type) in rules:
        if {type1, type2} == rule:
            return resulting_type
    
    raise Exception(f"Rule for implicit conversion of {type1} and {type2} was not given")


def with_coords(func):
    """Decorator that enables tracking of coordinates (filename, line) in analyzed source code"""
    @wraps(func)
    def wrapper(*args, **kwargs):
        coord_backup = state.coord
        
        node = kwargs["node"].coords if kwargs and "node" in kwargs else args[0]
        state.coord = node.coord
        
        try:
            return func(*args, **kwargs)
        finally:
            state.coord = coord_backup
    
    return wrapper
