"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Main module.
"""
from __future__ import annotations
import argparse
import configparser
import ctypes
import os.path
import sys
from contextlib import contextmanager
from typing import Iterator, List
from subprocess import CalledProcessError

import pkg_resources
from pycparserext.ext_c_parser import GnuCParser

import regcheck.state as state
from regcheck.device import Device, PeripheralNotFound, RegisterNotFound
from regcheck.formatter import MemoryFormatter, RwLogFormatter
from regcheck.interpreter import CONV_TO_CTYPE, declare_var, interpret
from regcheck.utils import (get_ast_from_files, get_function_def_subtree,
                            retrieve_enum_symbols, retrieve_global_variables)

from regcheck.memory import Symtable, Memory


@contextmanager
def cwd(path: str) -> Iterator[None]:
    """
    Temporary changes directory.

    Args:
        path: path to desired working directory
    """
    cwd_backup = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(cwd_backup)


def prepare_argparser() -> argparse.ArgumentParser:
    """
    Initialize ArgumentParser with CLI argument definitions.

    Returns:
        Initialized parser
    """
    parser = argparse.ArgumentParser(
        prog="regcheck", description="Static analysis of heap writes"
    )
    parser.add_argument("function", help="Name of function to be processed")
    parser.add_argument(
        "-c", "--config", required=True,
        type=argparse.FileType("r"), help="configuration file"
    )
    parser.add_argument(
        "-d", "--diff-only", action="store_true",
        help="whether to print only difference in memory"
    )
    parser.add_argument(
        "--output-memory", type=argparse.FileType('w'), default=False,
        help="Memory state after interpretation, doesn't produce accurate results with --all-paths option"
    )
    parser.add_argument(
        "--output-rw-logs", type=argparse.FileType('w'), default=False,
        help="Logs of memory access during interpretation"
    )
    parser.add_argument(
        "--all-paths", action="store_true", default=False,
        help="Whether to interpret all paths"
    )
    parser.add_argument(
        "--no-cache", action="store_true", default=False,
        help="build new AST regardless of cache"
    )

    return parser


def configparser_list_converter(value: str) -> List[str]:
    """
    Takes comma delimited values and transfom them to list.

    Args:
        value: comma delimited string

    Returns:
        list of stripped values
    """
    return [subvalue.strip() for subvalue in value.split(",")]


def main(args) -> None:
    """Parse CLI arguments and print the result."""

    configparser_converters = {"list": configparser_list_converter}
    config = configparser.ConfigParser(
        inline_comment_prefixes=("#"), converters=configparser_converters
    )
    # hook for transforming options, by defaualt they are converted to lowercase
    config.optionxform = str
    config.read_file(args.config)

    files_dir = config["files"]["directory"]
    gcc_path = config["files"].get("gcc_path", "gcc")

    with cwd(files_dir):
        state.device = Device(config["files"]["device_file"])
        
        files = config.getlist("files", "files")
        filepaths = [file.strip() for file in files]

        fake_libc_include_path = pkg_resources.resource_filename("fake_libc_include", "")

        state.ast = get_ast_from_files(
            filepaths,
            parser_args={
                "cpp_path": gcc_path,
                "cpp_args": [
                    "-E",
                    "-D__attribute__(x)=",
                    "-D__extension__",
                    f"-I{fake_libc_include_path}",
                ] + config.getlist("files", "cpp_args", fallback=[]),
                "parser": GnuCParser(),
            },
            use_cached=not args.no_cache
        )

    state.memory = Memory()
    state.symtable = Symtable()

    # initializing memory to reset values of peripherals
    state.memory.enable_logging = False
    
    for peripheral in state.device.peripherals:
        for register in peripheral.registers:
            address = peripheral.base_address + register.offset
            state.memory[address] = ctypes.c_uint32(register.reset_value)
    
    state.memory.enable_logging = True

    state.all_paths = args.all_paths

    if "params" in config:
        for name, param_val in config.items("params"):
            type_, raw_value = (val.strip() for val in param_val.split(","))
            value = CONV_TO_CTYPE[type_](int(raw_value))
            declare_var(name, type_, value, is_global=True)
    
    retrieve_enum_symbols()
    retrieve_global_variables()
    func = get_function_def_subtree(args.function)
    state.coord = func.coord
    interpret(func.body)

    memory_dump = state.memory.dump_memory()

    if args.diff_only:

        def different_value(entry):
            address, value = entry
            try:
                return state.device.get_register(address).reset_value != value.value
            except PeripheralNotFound as exc:
                print(f"Address {hex(exc.address)} was written outside "
                      "local allocation but was not found in device file")
            except RegisterNotFound as exc:
                print(exc)

        memory_dump = dict(filter(different_value, memory_dump.items()))

    if args.output_memory:
        MemoryFormatter.write_csv(args.output_memory, memory_dump)

    if args.output_rw_logs:
        RwLogFormatter.write_csv(args.output_rw_logs, state.memory.rw_log)


if __name__ == "__main__":
    parser = prepare_argparser()
    args = parser.parse_args()
    try:
        main(args)
    except CalledProcessError as exc:
        error = (
            f"Preprocessing Error\n"
            f"Called command:\n{exc.cmd}\n\n"
            f"Command stderr:\n{exc.stderr.decode()}"
        )

        print(error, file=sys.stderr)
