"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Exception definitions used throghout application
"""


class NodeNotFoundError(Exception):
    """Attempted search for node failed."""
    def __init__(self, node_name: str):
        super().__init__(f"Requested node {node_name} was not found in AST")


class StructFieldNotFoundError(Exception):
    """Attempted search for field in struct failed."""
    def __init__(self, struct_name, field_name: str):
        super().__init__(f"Requested field {field_name} was not found in {struct_name}")


class VariableNotFoundError(Exception):
    """Attempted search for variable failed."""
    def __init__(self, symbol_name: str):
        super().__init__(f"Requested symbol {symbol_name} was not found in symtable")


class TypeSizeNotDefinedError(Exception):
    """Type with undefined size was encountered."""
    def __init__(self, type_name: str):
        super().__init__(f"Unable to calculate type size for {type_name}")
