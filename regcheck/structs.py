"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Intepreter functionality

Module dealing with structs and unions.

There are separate classes for both union and structs and also
for primitive field like int or int[].

Rough C struct to it's object representation:

struct {                    Struct:
    int x                       x: PrimitveField
    struct a {                  a: Struct
        int x                      x: PrimitiveField
        int y                      y: PrimitiveField
    }               -->         b: Union
    union b {                      x: PrimitiveField
        int x                      y: PrimitiveField
        int y
    }
}

Nested fields are denoted like `outermost_type;fieldname;fieldname`
"""
from __future__ import annotations

from dataclasses import dataclass
from typing import TYPE_CHECKING, Optional
from typing import Union as UnionT
from collections import OrderedDict

from pycparser import c_ast

from regcheck.exceptions import StructFieldNotFoundError
from regcheck.interpreter import interpret, get_type_size

if TYPE_CHECKING:
    # Pylint would recognize Union as class and use class-rgx but we already
    # have Union name taken by our own union class
    # Pylint also didn't detect we are using postponed evaluation (PEP563)
    # for the same reason as above
    FieldObj = UnionT[PrimitiveField, Struct, Union]  # pylint: disable=invalid-name, used-before-assignment


@dataclass
class PrimitiveField:
    """
    Representation of primitive field like int or int[].

    `int` vs `int[x]` can be determined by comparison of `type_size`
    and `total_size` -> `int[x]` means `total_size` = `x` * `type_size`
    """
    type_: str
    type_size: int
    total_size: int
    bitsize: Optional[int] = None


class Struct:
    """
    C Struct representation.
    """

    ANON_STRUCTS_PREFIX = "__ANON_STRUCT_"
    ANON_FIELDS_PREFIX = "__ANON_FIELDS_"

    def __init__(self, node: c_ast.Struct, type_: str):
        """
        Create object hierarchy from given node.

        Args:
            node: struct node
            type_: type of struct (e.g SYSCON_Type or SYSCON_Type;field1)
        """

        self.fields = OrderedDict()
        self.type_ = type_
        self._total_size = None
        self._anon_structs_counter = 1
        self._anon_fields_counter = 1
        self._bitfield_buffer = None
        self.bitsize = None  # keeping compatibility with PrimitiveField

        for field in node.decls:
            if type(field.type) == c_ast.TypeDecl:
                field_obj = self.type_decl_to_field(field.type, field.bitsize)
                fieldname = field.name
                if not field.name:
                    fieldname = self.ANON_FIELDS_PREFIX + str(self._anon_fields_counter)
                    self._anon_fields_counter += 1
                self.fields[fieldname] = field_obj

            elif type(field.type) == c_ast.ArrayDecl:

                field_obj = self.type_decl_to_field(field.type.type)
                field_obj.total_size *= interpret(field.type.dim).value
                self.fields[field.name] = field_obj

            elif type(field.type) == c_ast.Union:
                if field.name:
                    raise Exception("Should have been catched by TypeDecl")

                field_obj = Union(field.type, "")
                fieldname = self.ANON_STRUCTS_PREFIX + str(self._anon_structs_counter)
                self._anon_structs_counter += 1
                self.fields[fieldname] = field_obj

            else:
                raise NotImplementedError("Unknown type of field in struct")

        if any(field.bitsize for field in self.fields.values()) and len(set(field.type_ for field in self.fields.values())) > 1:
            raise NotImplementedError("If bitfields are used all fields must be of same type")

    def type_decl_to_field(self, node: c_ast.TypeDecl, bitsize=None) -> FieldObj:
        """
        Translates TypeDecl to one of field objects (Struct, Union, PrimitiveField)

        Args:
            node: TypeDecl node
            bitsize: either width of bitfield or None if field is not a bitfield

        Returns:
            One of field objects depending on field
        """
        if type(node.type) == c_ast.IdentifierType:
            # IDType has names array which almost always
            # consists of primitve type (e.g. ["uint32_t"])
            type_name = node.type.names[0]
            size = get_type_size(type_name)

            # bitfields - determines whether we have enough space to pack multiple fields
            # into one
            if bitsize:  # TypeDecl in ArrayDecl doesn't have bitsize attribute
                bitsize = int(bitsize.value)
                if self._bitfield_buffer is None or self._bitfield_buffer < bitsize:
                    self._bitfield_buffer = size * 8 - bitsize

                elif self._bitfield_buffer >= bitsize:
                    size = 0
                    self._bitfield_buffer -= bitsize

            return PrimitiveField(type_name, size, size, bitsize)
        else:
            # only primitive fields can share width, zeroing buffer when meeting non-primitive field
            self._bitfield_buffer = None

        if type(node.type) == c_ast.Struct:
            
            return Struct(node.type, self.type_ + ";" + node.declname)

        if type(node.type) == c_ast.Union:
            return Union(node.type, self.type_ + ";" + node.declname)

        raise NotImplementedError("Unknown type of field in struct")

    @property
    def type_size(self):
        """Return type size."""
        return sum(field.total_size for field in self.fields.values())

    @property
    def total_size(self):
        """Return total size, may differ from type_size if field is array."""
        if self._total_size:
            return self._total_size
        return self.type_size

    @total_size.setter
    def total_size(self, value):
        self._total_size = value

    def field(self, fieldname) -> FieldObj:
        """
        Return object representing given field.

        Supports only primitive field name - for support of nested names
        use fieldpath method.

        Args:
            fieldname: primitive field name (without `;`)

        Returns:
            One of field objects depending on field
        """
        if fieldname in self.fields:
            return self.fields[fieldname]

        for name, field in self.fields.items():
            if name.startswith(self.ANON_STRUCTS_PREFIX) and fieldname in field.fields:
                return field.fields[fieldname]

        raise StructFieldNotFoundError(self.type_, fieldname)

    def fieldpath(self, fieldname: str) -> FieldObj:
        """
        Return object representing given field.

        Also supports search for nested field using `;` notation

        Args:
            fieldname: primitive or nested fieldname

        Returns:
            One of field objects depending on field
        """
        if ";" not in fieldname:
            return self.field(fieldname)

        field, *rest = fieldname.split(";")
        next_field = self.field(field)
        return next_field.field(";".join(rest))

    def field_type(self, fieldname: str) -> str:
        """
        Return type of given field

        Args:
            fieldname: primitive or nested fieldname

        Returns:
            Field type (may be primitive or nested)
        """
        return self.fieldpath(fieldname).type_

    def field_type_size(self, fieldname: str) -> int:
        """
        Return type_size of given field

        Args:
            fieldname: primitive or nested fieldname

        Returns:
            Field type_size
        """
        return self.fieldpath(fieldname).type_size

    def field_total_size(self, fieldname: str) -> int:
        """
        Return total_size of given field

        Args:
            fieldname: primitive or nested fieldname

        Returns:
            Field total_size
        """
        return self.fieldpath(fieldname).total_size

    def field_bitfield_size(self, fieldname: str) -> Optional[int]:
        """
        Return bitfield size of given field

        Args:
            fieldname: primitive or nested fieldname

        Returns:
            Field bitfield size or None if bitfield was not used
        """
        return self.fieldpath(fieldname).bitsize

    def field_offset(self, fieldname: str) -> int:
        """
        Return count of bytes from start of struct to given fieldname.

        Args:
            fieldname: primitive or nested fieldname

        Returns:
            offset of field from start of struct
        """
        offset = 0
        prev_offset = 0
        for name, field in self.fields.items():
            if name == fieldname:
                return offset if field.type_size > 0 else prev_offset

            if name.startswith(self.ANON_STRUCTS_PREFIX):
                suboffset = field.field_offset(fieldname)
                if suboffset is not None:  # None means that field isn't in struct
                    return offset + suboffset

            if offset > 0:
                prev_offset += field.total_size

            offset += field.total_size

        raise StructFieldNotFoundError(self.type_, fieldname)

    def field_bitfield_offset(self, fieldname: str) -> Optional[int]:
        """
        Return count of bites preceding current field. Will be None if given field is not a bitfield.

        Args:
            fieldname: primitive or nested fieldname

        Returns:
            offset of field from start of struct
        """
        # doesnt support offsets in nested structures
        offset = 0
        start_counting = False
        for name, field in reversed(self.fields.items()):
            if start_counting:
                offset += field.bitsize
                if field.type_size > 0:
                    return offset

            if name == fieldname:
                if field.bitsize is None:
                    return None
                if field.type_size > 0:
                    return 0
                start_counting = True


class Union(Struct):
    """
    C Union representation.
    """
    @property
    def type_size(self):
        return max(field.total_size for field in self.fields.values())

    def field_offset(self, fieldname):
        if ";" not in fieldname:
            if fieldname in self.fields:
                return 0  # In union there are no offsets
            return None

        field, *rest = fieldname.split(";")
        next_field = self.fields[field]
        return self.field_offset(field) + next_field.field_offset(";".join(rest))
