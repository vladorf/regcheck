"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Module holds implementation of memory state of interpreter

Symtable holds variable name to address translation (LValue) while Memory
holds address to Value translation.
"""
from __future__ import annotations
import ctypes
from typing import Union

from regcheck.values import Variable


class Symtable:
    """
    Symbol table.

    Translates variable names to LValues (which contains variable type and address)
    """
    _global_variables = _current_frame = {}
    _frames = [_current_frame]

    def new_frame(self) -> None:
        """Create and use new frame."""
        self._current_frame = {}
        self._frames.append(self._current_frame)

    def del_frame(self) -> None:
        """Delete current frame and restore last one."""
        self._frames.pop()
        self._current_frame = self._frames[-1]

    def __setitem__(self, key: str, value: Variable) -> None:
        """
        Set new variable.

        Args:
            key: variable name
            value: LValue representation of variable

        Raises:
            TypeError: wrong argument types were supplied
        """
        self._current_frame[key] = value

    def __getitem__(self, key: str) -> Union[Variable, ctypes._SimpleCData]:
        """
        Get variable value.

        Args:
            key: variable name

        Raises:
            VariableNotFoundError: requesting variable that was not yet declared
            TypeError: wrong argument types were supplied

        Returns:
            Value representation of variable
        """
        variable = self.get_variable(key)

        import regcheck.state
        return regcheck.state.memory[variable.address]

    def get_variable(self, key: str) -> Union[Variable, str]:
        
        variable = self._current_frame.get(key, None)
        if not variable:
            variable = self._global_variables.get(key, None)

        if not variable:
            return key  # IDs are also fields in structs which won't be found in symtable

        return variable

    def declare_global(self, key: str, value: Variable) -> None:
        self._global_variables[key] = value


class Memory:
    """Representation of hardware memory"""
    def __init__(self):
        self._memory = {}
        self._stack = {}
        self._allocation_counter = 0
        self.enable_logging = True
        self.rw_log = []

    def allocate(self, size: int) -> int:
        """
        Allocate size in memory for variables.

        Args:
            size: how much bytes to allocate

        Returns:
            address which has allocated `size` bytes for use
        """
        self._allocation_counter += size
        return self._allocation_counter - size

    def _log_operation(self, op, address, value):
        if self.enable_logging and address > self._allocation_counter:
            import regcheck.state
            self.rw_log.append((op, address, value, regcheck.state.coord))

    def __setitem__(self, address: int, value: ctypes._SimpleCData) -> None:
        assert isinstance(value, ctypes._SimpleCData)
        if address <= self._allocation_counter:
            self._stack[address] = value
        else:
            self._log_operation("WRITE", address, value)
            self._memory[address] = value

    def __getitem__(self, address: int) -> ctypes._SimpleCData:
        if address <= self._allocation_counter:
            return self._stack[address]
        
        value = self._memory[address]
        self._log_operation("READ", address, value)
        return value

    def dump_memory(self):
        return dict(filter(lambda item: item[0] > self._allocation_counter, self._memory.items()))
