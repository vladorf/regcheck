"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Module used to transform interpreter state into human/machine readable formats.
"""
from __future__ import annotations
import csv

import regcheck.state as state
from regcheck.device import PeripheralNotFound, RegisterNotFound


class MemoryFormatter:
    """Class is responsible for formatting of memory state into human/machine readable formats."""

    @staticmethod
    def write_csv(file_handle, memory_dump):
        """Write csv representation of memory to given handle."""
        fieldnames = ["address", "name", "value", "bitfields"]
        writer = csv.DictWriter(file_handle, fieldnames=fieldnames)

        writer.writeheader()
        for address, value in memory_dump.items():

            try:
                register = state.device.get_register(address)
                bitfields_dict = register.bitfields_for(value.value)
                
                register_name = "->".join(state.device.get_name_parts(address))
                bitfields = ";".join((k + "=" + str(v) for k, v in bitfields_dict.items()))
            
            except (PeripheralNotFound, RegisterNotFound):
                register_name, bitfields = "", ""

            writer.writerow({
                "address": hex(address),
                "name": register_name,
                "value": hex(value.value),
                "bitfields": bitfields
                })


class RwLogFormatter:
    """Class is responsible for formatting of read-write logs into human/machine readable formats."""

    @staticmethod
    def write_csv(file_handle, memory_rw_log):
        """Write csv representation of rw_log to given handle."""
        fieldnames = ["operation", "address", "name", "coords", "value", "bitfields"]
        writer = csv.DictWriter(file_handle, fieldnames=fieldnames)

        writer.writeheader()
        for op, address, value, coords in memory_rw_log:

            try:
                register = state.device.get_register(address)
                bitfields_dict = register.bitfields_for(value.value)
                
                register_name = "->".join(state.device.get_name_parts(address))
                bitfields = ";".join((k + "=" + str(v) for k, v in bitfields_dict.items()))
            
            except (PeripheralNotFound, RegisterNotFound):
                register_name, bitfields = "", ""

            writer.writerow({
                "operation": op,
                "address": hex(address),
                "name": register_name,
                "coords": coords,
                "value": hex(value.value),
                "bitfields": bitfields
            })
