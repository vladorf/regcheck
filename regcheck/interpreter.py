"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Intepreter functionality

Interpreter is implemented in several functions, where each function handles one
type of node.

This wonderful concept is supported via `functools.singledispatch` -
we have one main function `interpret` and its implementations which we `register`
into `interpret` using `interpret.register` decorator.

Singledispatch will take care of `dispatching` the call to right function
depending on type of first argument (type of node class in our case).

"""
from __future__ import annotations

import operator
import ctypes
from functools import singledispatch

from pycparser import c_ast
from pycparserext import ext_c_parser

import regcheck.state as state
from regcheck.exceptions import TypeSizeNotDefinedError
from regcheck.utils import (get_function_def_subtree, get_struct_object, is_enum,
                            is_struct, resolve_implicit_conversion, with_coords)
from regcheck.values import Variable


class Return(Exception):
    def __init__(self, value):
        self.value = value


class AddressToFunctionCast(Exception):
    def __init__(self, address):
        super().__init__()
        self.address = address


IGNORED_FUNCTIONS = {"assert"}

TYPE_OFFSETS = {
    "int32_t": 4,
    "uint32_t": 4,
    "uint64_t": 8,
    "uint16_t": 2,
    "uint8_t": 1,
    "int": 4,
    "uint": 4,
    "bool": 1,
    "ulong": 4,
    "void": 0,
}

CONV_TO_CTYPE = {
    "uint32_t": ctypes.c_uint32,
    "uint16_t": ctypes.c_uint16,
    "uint8_t": ctypes.c_uint8,
    "int": ctypes.c_int,
    "uint": ctypes.c_uint,
    "ulong": ctypes.c_ulong,
    "bool": ctypes.c_bool,
    "int32_t": ctypes.c_int32,
    "int16_t": ctypes.c_int16,
    "int8_t": ctypes.c_int8,
}

CONV_TO_TYPE = {
    ctypes.c_uint8: "uint8_t",
    ctypes.c_uint16: "uint16_t",
    ctypes.c_ulong: "ulong",
    ctypes.c_uint32: "uint32_t",
    ctypes.c_uint: "uint",
    ctypes.c_int: "int",
    ctypes.c_bool: "bool",
    ctypes.c_int32: "int32_t",
    ctypes.c_int16: "int16_t",
    ctypes.c_int8: "int8_t",
}


def get_type_size(typename: str) -> int:
    """Calculate type size for given type name.

    Nested fields in structures can be accessed in `structure_type;field;field;...`
    format where structure type is outermost type of structure.

    Args:
        typename: C type, e.g. uint32_t, flag_enum, struct;field

    Raises:
        NodeNotFoundError: given typename wasn't found in AST (non-primitive types only)
        TypeSizeNotDefined: unknown type was supplied

    Returns:
        typesize in bytes
    """
    if isinstance(typename, ctypes._SimpleCData):
        typename = CONV_TO_TYPE[type(typename)]

    if typename in TYPE_OFFSETS:
        return TYPE_OFFSETS[typename]

    if is_enum(typename):
        return TYPE_OFFSETS["int"]

    if is_struct(typename):
        base_symbol, *rest = typename.split(";")
        obj = get_struct_object(base_symbol)
        return obj.field_type_size(";".join(rest)) if rest else obj.type_size

    raise TypeSizeNotDefinedError(typename)


def prepare_var(type_, value=None, pointer=0) -> Variable:
    """Prepare Variable object for declaration."""
    type_size = get_type_size(type_)
    address = state.memory.allocate(type_size)

    if value is not None:
        state.memory[address] = value
    
    return Variable(type_, address, pointer)


def declare_var(name: str, type_, value=None, is_global=False) -> None:
    """
    Declare new variable with given name having give value.

    Convenience function which basically implements c_ast.Decl

    Args:
        name: variable name
        type_: type of variable
        value: variable value
        is_global: whether variable is global
    """
    variable = prepare_var(type_, value)

    if is_global:
        state.symtable.declare_global(name, variable)
    else:    
        state.symtable[name] = variable


def interpret_init_list(node, obj, address):
    """
    Interpret init-list type of structure declaration.

    https://en.cppreference.com/w/c/language/array_initialization

    Args:
        node: InitList node to be interpreted
        obj: Struct instance of interpreted struct
        address: where in emulated address space is struct located

    """
    for idx, expr in enumerate(node.exprs):
        if type(expr) == c_ast.NamedInitializer:
            fieldname = expr.name[0].name
            expr = expr.expr
        else:
            fieldname = list(obj.fields.keys())[idx]
        
        # Initializing nested struct
        if type(expr) == c_ast.InitList:
            type_ = obj.field_type(fieldname)
            offset = obj.field_offset(fieldname)
            obj = get_struct_object(type_)
            interpret_init_list(expr, obj, address + offset)
            return

        offset = obj.field_offset(fieldname)
        fieldtype = obj.field_type(fieldname)
        # If it is not known type, it is placeholder which will be overwritten later
        value = CONV_TO_CTYPE.get(fieldtype, ctypes.c_int)(interpret(expr).value)
        bitfield_offset = obj.field_bitfield_offset(fieldname)
        value_type = type(value)
        
        if bitfield_offset is not None:
            tmp_val = value.value << (16 - bitfield_offset - value.value.bit_length())
            state.memory[address + offset] = value_type(state.memory[address + offset].value | tmp_val)
        else:
            state.memory[address + offset] = value


@singledispatch
def interpret(node: c_ast.Node, lvalue=False):
    """
    Interpret given node, populates `symtable` and `memory` during run

    Args:
        node: any c_ast class representing ASTs node
        lvalue: internal argument, you probably shouldn't be setting this
    """
    raise NotImplementedError(f"Handle for node {type(node)} was not defined")


@interpret.register
@with_coords
def _(node: c_ast.Compound, lvalue=False):  # pylint: disable=unused-argument
    if not node.block_items:
        return
        
    for item in node.block_items:
        interpret(item)


@interpret.register
@with_coords
def _(node: c_ast.BinaryOp, lvalue=False):  # pylint: disable=unused-argument
    left = interpret(node.left)
    right = interpret(node.right)

    ops = {
        ">>": operator.rshift,
        "<<": operator.lshift,
        "+": operator.add,
        "-": operator.sub,
        "&": operator.and_,
        "|": operator.or_,
        "^": operator.xor,
        "!=": operator.ne,
        "==": operator.eq,
        "<=": operator.le,
        ">=": operator.ge,
        ">": operator.gt,
        "<": operator.lt,
        "/": operator.floordiv,
        "%": operator.mod,
        "*": operator.mul,
        "&&": lambda x, y: bool(x and y),
        "||": lambda x, y: bool(x or y),
    }

    if isinstance(left, Variable):
        left = state.memory[left.address]

    if isinstance(right, Variable):
        right = state.memory[right.address]

    resulting_type = resolve_implicit_conversion(left.__class__, right.__class__)
    return resulting_type(ops[node.op](left.value, right.value))


@interpret.register
@with_coords
def _(node: c_ast.Constant, lvalue=False):  # pylint: disable=unused-argument
    base = 16 if node.value.startswith("0x") else 10
    value = node.value.lower()

    if value.endswith("ul"):
        return ctypes.c_ulong(int(value.rstrip("ul"), base))

    if value.endswith("u"):
        return ctypes.c_uint(int(value.rstrip("u"), base))

    return ctypes.c_int(int(value, base))


@interpret.register
@with_coords
def _(node: c_ast.ID, lvalue=False):
    variable = state.symtable.get_variable(node.name)
    return variable if lvalue else state.memory[variable.address]


@interpret.register
@with_coords
def _(node: c_ast.Decl, lvalue=False):  # pylint: disable=unused-argument
    _type = node.type.type.names[0]
    if type(node.init) == c_ast.InitList:
        obj = get_struct_object(_type)
        address = state.memory.allocate(obj.type_size)
        lval = Variable(obj.type_, address)
        state.symtable[node.name] = lval
        state.memory[address] = ctypes.c_uint32(0)
        interpret_init_list(node.init, obj, address)
    else:
        value = interpret(node.init) if node.init else None
        # if is is not known type nor enum it got to be struct so type doesn't matter, currently it is placeholder
        dst_type = ctypes.c_int if is_enum(_type) else CONV_TO_CTYPE.get(_type, ctypes.c_int)
        if isinstance(value, Variable):
            state.symtable[node.name] = value
            state.memory[value.address] = dst_type(state.symtable[node.name].value)
        else:
            declare_var(node.name, _type, dst_type(value.value) if value is not None else None)


@interpret.register
@with_coords
def _(node: c_ast.Cast, lvalue=False):  
    variable = interpret(node.expr, lvalue)

    if type(node.to_type.type) == c_ast.PtrDecl:
        param_type = node.to_type.type.type

        if type(param_type) == c_ast.TypeDecl:
            param_type = param_type.type
    
    else:
        param_type = node.to_type.type.type

    if type(param_type) == c_ast.IdentifierType:
        param_type = param_type.names[0]
    
    # Cast to function, only time we support casting to function is when
    # we cast address and we have definition in external file supplied to regcheck
    elif type(param_type) == ext_c_parser.FuncDeclExt and type(node.expr) == c_ast.Constant:
        raise AddressToFunctionCast(interpret(node.expr).value)
    
    if not isinstance(variable, Variable):
        variable = prepare_var(param_type, variable)
    
    if type(node.to_type.type) == c_ast.PtrDecl:
        variable.pointer = 1
    return variable


@interpret.register
@with_coords
def _(node: c_ast.UnaryOp, lvalue=False):
    if node.op == "*":
        if lvalue:
            if type(node.expr) == c_ast.ID:  # IDs are already pointers to something
                address = interpret(node.expr, True)
            else:
                address = interpret(node.expr, False)
            value = state.memory[address.value if isinstance(address, ctypes._SimpleCData) else address.address]
            return Variable(CONV_TO_TYPE[type(value)], value.value)

        elif type(node.expr) == c_ast.ID:  # IDs are already pointers to something
            # return interpret(node.expr, False)
            address = interpret(node.expr, False)  # probabley should be this, pointer handling is wobbly
        else:
            address = interpret(node.expr, lvalue)
            assert address.pointer > 0
            address = state.memory[address.address] 
            
        return state.memory[address.value if isinstance(address, ctypes._SimpleCData) else address.address]

    if node.op == "&":
        lval = interpret(node.expr, True)
        return prepare_var(lval.type_, ctypes.c_uint32(lval.address))

    if node.op == "~":
        result = interpret(node.expr)

        if isinstance(result, Variable):
            result = state.memory[result.address]

        return type(result)(~result.value)

    if node.op == "!":
        return not interpret(node.expr)

    if node.op == "-":
        result = interpret(node.expr)
        return type(result)(-result.value)

    if node.op == "p++":
        result = interpret(node.expr, True)
        state.memory[result.address].value += 1
        return result

    if node.op == "p--":
        result = interpret(node.expr, True)
        state.memory[result.address].value -= 1
        return result

    raise NotImplementedError(f"Operator {node.op} is not implemented")


@interpret.register
@with_coords
def _(node: c_ast.StructRef, lvalue=False):
    address_raw = None
    if type(node.name) == c_ast.ID:  # we know it by looking at Value object
        lval = state.symtable.get_variable(node.name.name)
        struct_type = lval.type_
        if lval.pointer:  # we need raw address of struct
            lval = Variable(struct_type, state.memory[lval.address].value)
        address_raw = lval.address
    elif type(node.name) == c_ast.Cast:
        struct_type = node.name.to_type.type.type.type.names[
            0
        ]  # we hope its typecasted in place
        address = interpret(node.name, True)
    elif type(node.name) in (c_ast.ArrayRef, c_ast.StructRef):
        address = interpret(node.name, True)
        address_raw = address.address
        struct_type = address.type_
    else:
        raise NotImplementedError(
            f"Struct type detection for {type(node.name)} is not implemented"
        )

    base_struct, *rest = struct_type.split(";")

    obj = get_struct_object(base_struct)
    if rest:
        obj = obj.fieldpath(";".join(rest))

    if address_raw is None:
        address_raw = state.memory[address.address].value

    field_name = node.field.name
    field_offset = obj.field_offset(field_name)
    type_ = obj.field_type(field_name)
    bitsize = obj.field_bitfield_size(field_name)
    bitoffset = obj.field_bitfield_offset(field_name)

    if lvalue:
        return Variable(type_, address_raw + field_offset, bitsize=bitsize, bitoffset=bitoffset)
    else:
        value = state.memory[address_raw + field_offset]
        if bitsize is not None:
            return value.transform_to_bitfield(bitsize, bitoffset)
        return value


@interpret.register
@with_coords
def _(node: c_ast.Assignment, lvalue=False):
    var_name = interpret(node.lvalue, lvalue=True)

    assert not isinstance(var_name, str)

    if node.op == "=":
        result = interpret(node.rvalue)
    else:
        expr = c_ast.BinaryOp(node.op.rstrip("="), node.lvalue, node.rvalue)
        result = interpret(expr)

    if isinstance(result, Variable):
        result = state.memory[result.address]

    state.memory[var_name.address] = CONV_TO_CTYPE[var_name.type_](result.value)


@interpret.register
@with_coords
def _(node: c_ast.FuncCall, lvalue=False):  # pylint: disable=unused-argument
    fnc_name = node.name

    if type(fnc_name) == c_ast.ID:
        fnc_name = fnc_name.name
    else:
        try:
            interpret(fnc_name)            
        except AddressToFunctionCast as exc:
            fnc_name = "_" + str(hex(exc.address))

    if fnc_name in IGNORED_FUNCTIONS:
        return
    fnc_def = get_function_def_subtree(fnc_name)
    tmp_frame = (
        {}
    )  # we might need stuff from current frame, so we store args in temporary one

    if node.args:
        if not fnc_def.decl.type.args:
            raise Exception(f"Custom function {fnc_name} expects no arguments but was passed {len(node.args.exprs)} ({state.coord})")

        for param, arg in zip(fnc_def.decl.type.args.params, node.args.exprs):
            param_type = (
                param.type.type.names[0]
                if type(param.type) != c_ast.PtrDecl
                else param.type.type.type.names[0]
            )
            if is_enum(param_type):  # these are precomputed
                tmp_frame[param.name] = interpret(arg, True)
            
            else:
                variable = interpret(arg, True)
                
                # it can happen that variable is not a Variable (e.g. expression)
                if not isinstance(variable, Variable):
                    type_ = CONV_TO_TYPE[type(variable)]
                    variable = prepare_var(type_, variable)

                if type(param.type) == c_ast.PtrDecl:
                    variable.pointer = 1
                
                tmp_frame[param.name] = variable

    state.symtable.new_frame()
    
    for name, val, in tmp_frame.items():
        state.symtable[name] = val
    
    try:
        interpret(get_function_def_subtree(fnc_name).body)
    except Return as exc:
        return exc.value
    finally:    
        state.symtable.del_frame()


@interpret.register
@with_coords
def _(node: c_ast.If, lvalue=False):  # pylint: disable=unused-argument
    if not state.all_paths:
        if interpret(node.cond):
            interpret(node.iftrue)
        else:
            if node.iffalse:
                interpret(node.iffalse)
    else:
        interpret(node.iftrue)
        if node.iffalse:
            interpret(node.iffalse)


@interpret.register
@with_coords
def _(node: c_ast.While, lvalue=False):
    if state.all_paths:  # on all_paths run we evaluate only once
        if node.stmt.block_items is not None:
            interpret(node.stmt)
    else:
        if node.stmt.block_items is not None:
            raise NotImplementedError("Empty while loops only")


@interpret.register
@with_coords
def _(node: c_ast.ArrayRef, lvalue=False):
    start = interpret(node.name, True)
    offset = interpret(node.subscript).value * get_type_size(start.type_)
    val = start.address + offset
    if lvalue:
        return Variable(start.type_, val)
    return state.memory[val]


@interpret.register
@with_coords
def _(node: c_ast.Return, lvalue=False):
    raise Return(interpret(node.expr))


@interpret.register
@with_coords
def _(node: c_ast.Switch, lvalue=False):
    cases = {}
    
    for case in node.stmt.block_items:
        if type(case) == c_ast.Default:
            case_key = "DEFAULT"
        else:
            case_key = interpret(case.expr).value
        
        cases[case_key] = case.stmts[:-1]
        assert type(case.stmts[-1]) == c_ast.Break

    cond = interpret(node.cond).value
    exprs = cases[cond] if cond in cases else cases["DEFAULT"]

    for expr in exprs:
        interpret(expr)

# it is this way because of pyinstaller
# for some reason it can't be resolved if used only as typehint
@interpret.register(ext_c_parser.Asm)  
@with_coords
def _(node: ext_c_parser.Asm, lvalue=False):
    pass  # not interpreting asm


@interpret.register
@with_coords
def _(node: c_ast.EmptyStatement, lvalue=False):
    pass


@interpret.register
@with_coords
def _(node: c_ast.TernaryOp, lvalue=False):
    return interpret(node.iftrue) if interpret(node.cond) else interpret(node.iffalse)
