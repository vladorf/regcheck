FROM python:3.7 AS builder

WORKDIR /app

COPY fake_libc_include /app/fake_libc_include
COPY requirements.txt /app
COPY setup.py /app
COPY regcheck /app/regcheck

RUN python setup.py bdist_wheel

FROM python:3.7 AS runtime

COPY --from=builder /app/dist/ /app
RUN pip install /app/*.whl

ENTRYPOINT ["python3", "-m", "regcheck"]
