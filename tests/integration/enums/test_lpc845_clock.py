"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
import pytest

from regcheck.utils import retrieve_enum_symbols


@pytest.mark.parametrize("name, expected", [
    ("clock_select_t", {
        "kCAPT_Clk_From_Fro": 24576,
        "kCAPT_Clk_From_MainClk": 24577,
        "kCAPT_Clk_From_SysPll": 24578,
        "kCAPT_Clk_From_Fro_Div": 24579,
        "kCAPT_Clk_From_WdtOsc": 24580,

        "kADC_Clk_From_Fro": 25600,
        "kADC_Clk_From_SysPll": 25601,

        "kSCT_Clk_From_Fro": 27648,
        "kSCT_Clk_From_MainClk": 27649,
        "kSCT_Clk_From_SysPll": 27650,

        "kEXT_Clk_From_SysOsc": 29696,
        "kEXT_Clk_From_ClkIn": 29697,

        "kUART0_Clk_From_Fro": 36864,
        "kUART0_Clk_From_MainClk": 36865,
        "kUART0_Clk_From_Frg0Clk": 36866,
        "kUART0_Clk_From_Frg1Clk": 36867,
        "kUART0_Clk_From_Fro_Div": 36868,

        "kUART1_Clk_From_Fro": 37888,
        "kUART1_Clk_From_MainClk": 37889,
        "kUART1_Clk_From_Frg0Clk": 37890,
        "kUART1_Clk_From_Frg1Clk": 37891,
        "kUART1_Clk_From_Fro_Div": 37892,
        
        "kUART2_Clk_From_Fro": 38912,
        "kUART2_Clk_From_MainClk": 38913,
        "kUART2_Clk_From_Frg0Clk": 38914,
        "kUART2_Clk_From_Frg1Clk": 38915,
        "kUART2_Clk_From_Fro_Div": 38916,
        
        "kUART3_Clk_From_Fro": 39936,
        "kUART3_Clk_From_MainClk": 39937,
        "kUART3_Clk_From_Frg0Clk": 39938,
        "kUART3_Clk_From_Frg1Clk": 39939,
        "kUART3_Clk_From_Fro_Div": 39940,
        
        "kUART4_Clk_From_Fro": 40960,
        "kUART4_Clk_From_MainClk": 40961,
        "kUART4_Clk_From_Frg0Clk": 40962,
        "kUART4_Clk_From_Frg1Clk": 40963,
        "kUART4_Clk_From_Fro_Div": 40964,
        
        "kI2C0_Clk_From_Fro": 41984,
        "kI2C0_Clk_From_MainClk": 41985,
        "kI2C0_Clk_From_Frg0Clk": 41986,
        "kI2C0_Clk_From_Frg1Clk": 41987,
        "kI2C0_Clk_From_Fro_Div": 41988,
        
        "kI2C1_Clk_From_Fro": 43008,
        "kI2C1_Clk_From_MainClk": 43009,
        "kI2C1_Clk_From_Frg0Clk": 43010,
        "kI2C1_Clk_From_Frg1Clk": 43011,
        "kI2C1_Clk_From_Fro_Div": 43012,
        
        "kI2C2_Clk_From_Fro": 44032,
        "kI2C2_Clk_From_MainClk": 44033,
        "kI2C2_Clk_From_Frg0Clk": 44034,
        "kI2C2_Clk_From_Frg1Clk": 44035,
        "kI2C2_Clk_From_Fro_Div": 44036,
        
        "kI2C3_Clk_From_Fro": 45056,
        "kI2C3_Clk_From_MainClk": 45057,
        "kI2C3_Clk_From_Frg0Clk": 45058,
        "kI2C3_Clk_From_Frg1Clk": 45059,
        "kI2C3_Clk_From_Fro_Div": 45060,
        
        "kSPI0_Clk_From_Fro": 46080,
        "kSPI0_Clk_From_MainClk": 46081,
        "kSPI0_Clk_From_Frg0Clk": 46082,
        "kSPI0_Clk_From_Frg1Clk": 46083,
        "kSPI0_Clk_From_Fro_Div": 46084,
        
        "kSPI1_Clk_From_Fro": 47104,
        "kSPI1_Clk_From_MainClk": 47105,
        "kSPI1_Clk_From_Frg0Clk": 47106,
        "kSPI1_Clk_From_Frg1Clk": 47107,
        "kSPI1_Clk_From_Fro_Div": 47108,
        
        "kFRG0_Clk_From_Fro": 55296,
        "kFRG0_Clk_From_MainClk": 55297,
        "kFRG0_Clk_From_SysPll": 55298,
        
        "kFRG1_Clk_From_Fro": 59392,
        "kFRG1_Clk_From_MainClk": 59393,
        "kFRG1_Clk_From_SysPll": 59394,
        
        "kCLKOUT_From_Fro": 61440,
        "kCLKOUT_From_MainClk": 61441,
        "kCLKOUT_From_SysPll": 61442,
        "kCLKOUT_From_ExtClk": 61443,
        "kCLKOUT_From_WdtOsc": 61444,
    }),
    ("clock_fro_osc_freq_t", {
        "kCLOCK_FroOscOut18M": 18000,
        "kCLOCK_FroOscOut24M": 24000,
        "kCLOCK_FroOscOut30M": 30000,
    }),
    ("clock_fro_src_t", {
        "kCLOCK_FroSrcLpwrBootValue": 0,
        "kCLOCK_FroSrcFroOsc": 131072,
    }),
    ("clock_main_clk_src_t", {
        "kCLOCK_MainClkSrcFro": 0,
        "kCLOCK_MainClkSrcExtClk": 256,
        "kCLOCK_MainClkSrcWdtOsc": 512,
        "kCLOCK_MainClkSrcFroDiv": 768,
        "kCLOCK_MainClkSrcSysPll": 1,
    }),
    ("clock_ip_name_t", {
        "kCLOCK_IpInvalid": 0,
        "kCLOCK_Rom": 1,
        "kCLOCK_Ram0_1": 2,
        "kCLOCK_I2c0": 5,
        "kCLOCK_Gpio0": 6,
        "kCLOCK_Swm": 7,
        "kCLOCK_Sct": 8,
        "kCLOCK_Wkt": 9,
        "kCLOCK_Mrt": 10,
        "kCLOCK_Spi0": 11,
        "kCLOCK_Spi1": 12,
        "kCLOCK_Crc": 13,
        "kCLOCK_Uart0": 14,
        "kCLOCK_Uart1": 15,
        "kCLOCK_Uart2": 16,
        "kCLOCK_Wwdt": 17,
        "kCLOCK_Iocon": 18,
        "kCLOCK_Acmp": 19,
        "kCLOCK_Gpio1": 20,
        "kCLOCK_I2c1": 21,
        "kCLOCK_I2c2": 22,
        "kCLOCK_I2c3": 23,
        "kCLOCK_Adc": 24,
        "kCLOCK_Ctimer0": 25,
        "kCLOCK_Mtb": 26,
        "kCLOCK_Dac0": 27,
        "kCLOCK_GpioInt": 28,
        "kCLOCK_Dma": 29,
        "kCLOCK_Uart3": 30,
        "kCLOCK_Uart4": 31,
        "kCLOCK_Capt": 1024,
        "kCLOCK_Dac1": 1025,
    })

])
def test_enums(mocker, name, expected, lpc845_ast, lpc845_memory, symtable):
    mocker.patch("regcheck.state.ast", lpc845_ast, create=True)
    mocker.patch("regcheck.state.memory", lpc845_memory)
    mocker.patch("regcheck.state.symtable", symtable)
    
    retrieve_enum_symbols()
    
    for fieldname, expected_value in expected.items():
        assert symtable[fieldname].value == expected_value
