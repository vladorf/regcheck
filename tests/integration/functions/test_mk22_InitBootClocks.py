"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
from ctypes import c_int, c_uint32

from conftest import Case

from regcheck.interpreter import interpret, declare_var, prepare_var, Return
from regcheck.values import Variable
from regcheck.utils import retrieve_enum_symbols, retrieve_global_variables

TEST_CASES = (
    Case("SMC_SetPowerModeProtection",
        {
            "base": Variable("SMC_Type", int("0x4007e000", 16), pointer=1),
            "allowedModes": c_int(170)
        }, {
            int("0x4007e000", 16): c_uint32(int("0xaa", 16)),
        }, "clock_config.c:128"),

    Case("SMC_SetPowerModeHsrun",
        {
            "base": Variable("SMC_Type", int("0x4007e000", 16), pointer=1),
        }, {
            int("0x4007e001", 16): c_uint32(int("0x60", 16)),
        }, "clock_config.c:128"),

    Case("CLOCK_SetSimSafeDivs",
        {}, {
            int("0x40048044", 16): c_uint32(int("0x1230000", 16)),
        }, "clock_config.c:128"),

    Case("CLOCK_CONFIG_SetRtcClock",
        {
            "capLoad": c_uint32(int("0x1800", 16)),
            "enableOutPeriph": c_uint32(1),
        }, {
            int("0x4003d010", 16): c_uint32(int("0x1900", 16)),
        }, "clock_config.c:128"),

    Case("CLOCK_InitOsc0",
        {
            # address of oscConfig_BOARD_BootClockHSRUN, got after retrieve_global_variables
            "config": Variable("osc_config_t", 7031, pointer=1),
        }, {
            # differs from debugger, cause is different resetValue, device file says 0x80, debugger 0xc0
            int("0x40064001", 16): c_uint32(int("0x94", 16)),
        }, "clock_config.c:128"),

    Case("BOARD_BootClockHSRUN",
        {
        }, {
            int("0x4007e000", 16): c_uint32(int("0xaa", 16)),
            int("0x4007e001", 16): c_uint32(int("0x60", 16)),
            # differs from debugger, in debugger it is set to 0x80089010 from the start,
            # meanwhile its reset value is 0x80000000
            int("0x40047000", 16): c_uint32(int("0x80080000", 16)),  
            int("0x40048004", 16): c_uint32(int("0x51010", 16)),
            int("0x40048034", 16): c_uint32(int("0xf0140030", 16)),
            int("0x40048044", 16): c_uint32(int("0x1340000", 16)),
            int("0x40048048", 16): c_uint32(int("0x9", 16)),
            int("0x40064000", 16): c_uint32(int("0x2", 16)),
            # differs from debugger, cause is different resetValue, device file says 0x80, debugger 0xc0
            int("0x40064001", 16): c_uint32(int("0x94", 16)),
            int("0x40064004", 16): c_uint32(int("0x42", 16)),
            int("0x40064005", 16): c_uint32(int("0x55", 16)),
            int("0x40064008", 16): c_uint32(int("0x0", 16)),
            int("0x4003d010", 16): c_uint32(int("0x1900", 16)),
        }, "clock_config.c:128"),
)


def test(mocker, mk22_function_subtree, args, heap_diff, mk22_ast, mk22_memory, symtable):
    mocker.patch("regcheck.state.ast", mk22_ast, create=True)
    mocker.patch("regcheck.state.memory", mk22_memory)
    mocker.patch("regcheck.state.symtable", symtable)
    heap_backup = mk22_memory._memory.copy()

    retrieve_enum_symbols()  # TAKING LONG, OPTIMIZE
    retrieve_global_variables()

    for name, value in args.items():
        if isinstance(value, Variable):
            variable = prepare_var(value.type_, c_uint32(value.address), value.pointer)
            symtable[name] = variable
        else:
            declare_var(name, "uint32_t", value)

    declare_var("g_xtal32Freq", "uint32_t", is_global=True)

    try:
        interpret(mk22_function_subtree)
    except Return:
        pass

    # comparing real state with combination of initial state and expected difference
    heap_backup.update(heap_diff)
    
    assert heap_backup.keys() == mk22_memory._memory.keys()
    for address in heap_backup:
        assert mk22_memory._memory[address].value == heap_backup[address].value
