"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
from dataclasses import dataclass
from typing import Dict, Union, Any

import pytest

from regcheck.utils import get_function_def_subtree
from regcheck.values import Variable


@dataclass
class Case:
    """Holding one test case. """

    name: str  # name of C function
    args: Dict[str, Union[Variable, Dict[str, Any]]]  # args to be supplied to it, optionally can be global
    heap_diff: Dict[int, Variable]  # which changes were made to memory
    source: str  # where in source code it was called, used to generate test ids


def pytest_generate_tests(metafunc):
    """
    More powerful alternative of pytest.mark.parametrize. 

    Examples can be found at: https://pytest.readthedocs.io/en/latest/example/parametrize.html
    """
    test_cases = metafunc.module.TEST_CASES
    _, board, _ = metafunc.module.__name__.split("_")
    args = (board + "_function_subtree", "args", "heap_diff")
    metafunc.parametrize(
        ",".join(args),
        [(case.name, case.args, case.heap_diff) for case in test_cases],
        indirect=[args[0]],
        ids=[case.name + " " + case.source for case in test_cases],
    )


@pytest.fixture(scope="function")
def lpc845_function_subtree(request, mocker, lpc845_ast):
    # we are taking function name as parameter
    function_name = request.param
    # returning body, function_def wouldn't do us any good, just remember to pass parameters
    mocker.patch("regcheck.state.ast", lpc845_ast, create=True)
    fnc_ast = get_function_def_subtree(function_name).body
    if not fnc_ast:
        raise Exception(
            "Function not found, make sure test name is in form test_<tested_function_name>"
        )

    return fnc_ast


@pytest.fixture(scope="function")
def mk22_function_subtree(request, mocker, mk22_ast):
    # we are taking function name as parameter
    function_name = request.param
    # returning body, function_def wouldn't do us any good, just remember to pass parameters
    mocker.patch("regcheck.state.ast", mk22_ast, create=True)
    fnc_ast = get_function_def_subtree(function_name).body
    if not fnc_ast:
        raise Exception(
            "Function not found, make sure test name is in form test_<tested_function_name>"
        )

    return fnc_ast
