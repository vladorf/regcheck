"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
from ctypes import c_uint32

from conftest import Case

from regcheck.interpreter import interpret, declare_var
from regcheck.utils import retrieve_enum_symbols

TEST_CASES = (
    Case("POWER_DisablePD", {"en": c_uint32(1)}, {}, "clock_config.c:125"),
    Case("POWER_DisablePD", {"en": c_uint32(2)}, {}, "clock_config.c:126"),
    Case("POWER_DisablePD",
        {
            "en": c_uint32(32)
        }, {
            int("0x40048238", 16): c_uint32(int("0xedd8", 16))
        }, "clock_config.c:128"),
        
    Case("CLOCK_SetFroOutClkSrc",
        {
            "src": c_uint32(131072)
        }, {
            int("0x40048028", 16): c_uint32(int("0x20000", 16)),
            int("0x40048030", 16): c_uint32(int("0x1", 16)),
        }, "clock_config.c:128"),

    Case("CLOCK_Select",
        {
            "sel": c_uint32(29696)
        }, {}, "clock_config.c:130"),

    Case("CLOCK_SetMainClkSrc",
        {
            "src": c_uint32(0)
        }, {}, "clock_config.c:131"),

    Case("CLOCK_SetCoreSysClkDiv",
        {
            "value": c_uint32(1)
        }, {}, "clock_config.c:132"),

    Case("BOARD_InitBootClocks",
        {
            "SystemCoreClock": {"value": c_uint32(12000000), "global": True}
        }, {
            int("0x40048028", 16): c_uint32(int("0x20000", 16)),
            int("0x40048030", 16): c_uint32(int("0x1", 16)),
            int("0x40048238", 16): c_uint32(int("0xedd8", 16)),
        }, "clock_config.c:45"),
)


def test(mocker, lpc845_function_subtree, args, heap_diff, lpc845_ast, lpc845_memory, symtable):
    mocker.patch("regcheck.state.ast", lpc845_ast, create=True)
    mocker.patch("regcheck.state.memory", lpc845_memory)
    mocker.patch("regcheck.state.symtable", symtable)
    heap_backup = lpc845_memory._memory.copy()

    for name, value in args.items():
        is_global = False
        
        if isinstance(value, dict):
            is_global = value.get("global", False)
            value = value["value"]
        
        declare_var(name, "uint32_t", value, is_global)

    retrieve_enum_symbols()
    interpret(lpc845_function_subtree)
    
    # comparing real state with combination of initial state and expected difference
    heap_backup.update(heap_diff)
    
    assert heap_backup.keys() == lpc845_memory._memory.keys()
    for address in heap_backup:
        assert lpc845_memory._memory[address].value == heap_backup[address].value
