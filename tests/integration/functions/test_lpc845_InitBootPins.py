"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
from ctypes import c_uint32, c_uint8, c_bool, c_int

from conftest import Case

from regcheck.interpreter import interpret, declare_var, prepare_var
from regcheck.values import Variable
from regcheck.utils import retrieve_enum_symbols

TEST_CASES = (
    Case("CLOCK_EnableClock", {"clk": c_uint32(18)},
        {
            int("0x40048080", 16): c_uint32(int("0x40017", 16))
        }, "pin_mux.c:73"),
    Case("CLOCK_EnableClock", {"clk": c_uint32(7)},
        {
            int("0x40048080", 16): c_uint32(int("0x97", 16))
        }, "pin_mux.c:75"),
    Case("IOCON_PinMuxSet",
        {
            "base": Variable("IOCON_Type", int("0x40044000", 16), pointer=1),
            "ionumber": c_uint8(6),
            "modefunc": c_uint32(48),
        },
        {
            int("0x40044018", 16): c_uint32(int("0x30", 16))
        }, "pin_mux.c:90"),
    Case("IOCON_PinMuxSet",
        {
            "base": Variable("IOCON_Type", int("0x40044000", 16), pointer=1),
            "ionumber": c_uint8(5),
            "modefunc": c_uint32(48),
        },
        {
            int("0x40044014", 16): c_uint32(int("0x30", 16))
        }, "pin_mux.c:105"),
    Case("SWM_SetFixedPinSelect",
        {
            "base": Variable("SWM_Type", int("0x4000c000", 16), pointer=1),
            "func": c_int(32),
            "enable": c_bool(True),
        },
        {
        }, "pin_mux.c:108"),
    Case("SWM_SetFixedPinSelect",
        {
            "base": Variable("SWM_Type", int("0x4000c000", 16), pointer=1),
            "func": c_int(64),
            "enable": c_bool(True),
        },
        {
        }, "pin_mux.c:111"),
    Case("SWM_SetMovablePinSelect",
        {
            "base": Variable("SWM_Type", int("0x4000c000", 16), pointer=1),
            "func": c_int(0),
            "swm_port_pin": c_int(25),
        },
        {
            int("0x4000c000", 16): c_uint32(int("0xffffff19", 16))
        }, "pin_mux.c:296"),
    Case("SWM_SetMovablePinSelect",
        {
            "base": Variable("SWM_Type", int("0x4000c000", 16), pointer=1),
            "func": c_int(1),
            "swm_port_pin": c_int(24),
        },
        {
            int("0x4000c000", 16): c_uint32(int("0xffff18ff", 16))
        }, "pin_mux.c:299"),
     Case("BOARD_InitSWD_DEBUGPins", {},
        {
            int("0x40048080", 16): c_uint32(int("0x40017", 16)),
            int("0x40044014", 16): c_uint32(int("0x30", 16)),
            int("0x40044018", 16): c_uint32(int("0x30", 16)),
            int("0x4000c034", 16): c_uint32(int("0xff08ffff", 16))
        }, "pin_mux.c:70"),
    Case("BOARD_InitBUTTONsPins", {},
        {
            int("0x40048080", 16): c_uint32(int("0x40017", 16)),
            int("0x40044010", 16): c_uint32(int("0x30", 16)),
        }, "pin_mux.c:136"),
    Case("BOARD_InitLEDsPins", {},
        {
            int("0x40048080", 16): c_uint32(int("0x40017", 16)),
            int("0x40044090", 16): c_uint32(int("0x30", 16)),
            int("0x40044094", 16): c_uint32(int("0x30", 16)),
            int("0x40044098", 16): c_uint32(int("0x30", 16)),
        }, "pin_mux.c:136"),
    Case("BOARD_InitDEBUG_UARTPins", {},
        {
            int("0x40048080", 16): c_uint32(int("0x40017", 16)),
            int("0x40044060", 16): c_uint32(int("0x30", 16)),
            int("0x4004405c", 16): c_uint32(int("0x30", 16)),
            int("0x4000c000", 16): c_uint32(int("0xffff1819", 16))
        }, "pin_mux.c:136"),
    Case("BOARD_InitBootPins", {},
        {
            int("0x40048080", 16): c_uint32(int("0x40017", 16)),
            int("0x40044010", 16): c_uint32(int("0x30", 16)),
            int("0x40044014", 16): c_uint32(int("0x30", 16)),
            int("0x40044018", 16): c_uint32(int("0x30", 16)),
            int("0x40044090", 16): c_uint32(int("0x30", 16)),
            int("0x40044094", 16): c_uint32(int("0x30", 16)),
            int("0x40044098", 16): c_uint32(int("0x30", 16)),
            int("0x40044060", 16): c_uint32(int("0x30", 16)),
            int("0x4004405c", 16): c_uint32(int("0x30", 16)),
            int("0x4000c000", 16): c_uint32(int("0xffff1819", 16)),
            int("0x4000c034", 16): c_uint32(int("0xff08ffff", 16))
        }, "pin_mux.c:136"),
)


def test(mocker, lpc845_function_subtree, args, heap_diff, lpc845_ast, lpc845_memory, symtable):
    mocker.patch("regcheck.state.ast", lpc845_ast, create=True)
    mocker.patch("regcheck.state.memory", lpc845_memory)
    mocker.patch("regcheck.state.symtable", symtable)
    heap_backup = lpc845_memory._memory.copy()

    for name, value in args.items():
        if isinstance(value, Variable):
            variable = prepare_var(value.type_, c_uint32(value.address), value.pointer)
            symtable[name] = variable
        else:
            declare_var(name, "uint32_t", value)

    retrieve_enum_symbols()
    interpret(lpc845_function_subtree)
    
    # comparing real state with combination of initial state and expected difference
    heap_backup.update(heap_diff)
    
    assert heap_backup.keys() == lpc845_memory._memory.keys()
    for address in heap_backup:
        assert lpc845_memory._memory[address].value == heap_backup[address].value
