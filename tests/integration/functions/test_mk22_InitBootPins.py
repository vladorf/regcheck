"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
from ctypes import c_int

from conftest import Case

from regcheck.interpreter import interpret, declare_var
from regcheck.values import Variable
from regcheck.utils import retrieve_enum_symbols

TEST_CASES = (
    Case("CLOCK_EnableClock", {"name": c_int(272105481)},
        {
            int("0x40048038", 16): c_int(int("0x40382", 16))
        }, "pin_mux.c:125"),

    Case("BOARD_InitPins",
        {},
        {
            # looks like hw side effect, it is set to 0x743 when reset value is 0x703 
            int("0x40049008", 16): c_int(int("0x700", 16)),
            int("0x40048038", 16): c_int(int("0x40382", 16)),
        }, "pin_mux.c:122"),
    
    Case("CLOCK_EnableClock", {"name": c_int(272105485)},
        {
            int("0x40048038", 16): c_int(int("0x42182", 16))
        }, "pin_mux.c:343"),
    
    Case("BOARD_InitDEBUG_UARTPins",
        {},
        {
            int("0x40048038", 16): c_int(int("0x42182", 16)),
            int("0x4004d000", 16): c_int(int("0x300", 16)),
            int("0x4004d004", 16): c_int(int("0x300", 16)),
        }, "pin_mux.c:102"),
)


def test(mocker, mk22_function_subtree, args, heap_diff, mk22_ast, mk22_memory, symtable):
    mocker.patch("regcheck.state.ast", mk22_ast, create=True)
    mocker.patch("regcheck.state.memory", mk22_memory)
    mocker.patch("regcheck.state.symtable", symtable)
    heap_backup = mk22_memory._memory.copy()

    for name, value in args.items():
        if isinstance(value, Variable):
            symtable[name] = value
        else:
            declare_var(name, "uint32_t", value)

    retrieve_enum_symbols()
    interpret(mk22_function_subtree)
    
    # comparing real state with combination of initial state and expected difference
    heap_backup.update(heap_diff)
    
    assert heap_backup.keys() == mk22_memory._memory.keys()
    for address in heap_backup:
        assert mk22_memory._memory[address].value == heap_backup[address].value
