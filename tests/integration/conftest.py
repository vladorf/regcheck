"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
import os.path
import pickle
import ctypes

import pytest

from regcheck.memory import Memory, Symtable
from regcheck.device import Device


@pytest.fixture(scope="session")
def assets_dir():
    dirname = os.path.dirname(__file__)
    return os.path.join(dirname, "assets")


@pytest.fixture(scope="session")
def lpc845_ast(assets_dir):
    filepath = os.path.join(assets_dir, "lpc845_ast.pickled")
    with open(filepath, "rb") as f:
        return pickle.load(f)


@pytest.fixture(scope="session")
def mk22_ast(assets_dir):
    filepath = os.path.join(assets_dir, "mk22_ast.pickled")
    with open(filepath, "rb") as f:
        return pickle.load(f)


def memory_from_filepath(filepath):
    """Initialize memory object from given filepath of device file"""
    memory = Memory()
    memory.enable_logging = False
    device = Device(filepath)
    with open(filepath, "r") as f:
        for peripheral in device.peripherals:
            for register in peripheral.registers:
                address = peripheral.base_address + register.offset
                memory[address] = ctypes.c_uint32(register.reset_value)

    return memory


@pytest.fixture(scope="function")  # we are using it also for func params
def lpc845_memory(assets_dir):
    filepath = os.path.join(assets_dir, "lpc845.xml")
    return memory_from_filepath(filepath)


@pytest.fixture(scope="function")  # we are using it also for func params
def mk22_memory(assets_dir):
    filepath = os.path.join(assets_dir, "mk22.xml")
    return memory_from_filepath(filepath)


@pytest.fixture(scope="function")
def symtable():
    return Symtable()
