"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
from regcheck.utils import get_struct_object


def test_syscon(mocker, lpc845_ast):
    mocker.patch("regcheck.state.ast", lpc845_ast, create=True)

    obj = get_struct_object("SYSCON_Type")
    
    assert hex(obj.field_offset('IOCONCLKDIV3')) == '0x140'
    assert obj.field_type('IOCONCLKDIV3') == 'uint32_t'
    assert obj.field_type_size('IOCONCLKDIV3') == 4
    assert obj.field_total_size('IOCONCLKDIV3') == 4

    assert hex(obj.field_offset('RESERVED_0')) == '0x4'
    assert obj.field_type('RESERVED_0') == 'uint8_t'
    assert obj.field_type_size('RESERVED_0') == 1
    assert obj.field_total_size('RESERVED_0') == 4

    assert hex(obj.field_offset('FRG')) == '0xd0'
    assert obj.field_type('FRG') == 'SYSCON_Type;FRG'
    assert obj.field_type_size('FRG') == 16
    assert obj.field_total_size('FRG') == 32

    assert hex(obj.field('FRG').field_offset('FRGMULT')) == '0x4'
    assert obj.field_type('FRG;FRGMULT') == 'uint32_t'
    assert obj.field_type_size('FRG;FRGMULT') == 4
    assert obj.field_total_size('FRG;FRGMULT') == 4


def test_swm(mocker, lpc845_ast):
    mocker.patch("regcheck.state.ast", lpc845_ast, create=True)

    obj = get_struct_object("SWM_Type")
    
    assert hex(obj.field_offset('PINASSIGN;PINASSIGN0')) == '0x0'
    assert hex(obj.field_offset('PINASSIGN;PINASSIGN5')) == '0x14'
    assert obj.field_type_size('PINASSIGN;PINASSIGN5') == 4
    assert obj.field_total_size('PINASSIGN;PINASSIGN5') == 4
    
    assert hex(obj.field_offset('PINASSIGN_DATA')) == '0x0'
    assert obj.field_type_size('PINASSIGN_DATA') == 4
    assert obj.field_total_size('PINASSIGN_DATA') == 60
    
    assert hex(obj.field_offset('PINENABLE0')) == '0x1c0'
    assert hex(obj.field_offset('PINENABLE1')) == '0x1c4'
    



