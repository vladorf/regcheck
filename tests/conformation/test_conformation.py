"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from regcheck.interpreter import interpret
from regcheck.utils import get_function_def_subtree


def test_binary_operations(mocker, ast_from_file, memory, symtable):
    mocker.patch("regcheck.state.memory", memory)
    mocker.patch("regcheck.state.symtable", symtable)
    mocker.patch("regcheck.state.ast", ast_from_file("binary_op.c"), create=True)

    interpret(get_function_def_subtree("main").body)

    assert symtable["a"].value == 11
    assert symtable["b"].value == 4
    assert symtable["c"].value == 15


def test_unary_operations(mocker, ast_from_file, memory, symtable):
    mocker.patch("regcheck.state.memory", memory)
    mocker.patch("regcheck.state.symtable", symtable)
    mocker.patch("regcheck.state.ast", ast_from_file("unary_op.c"), create=True)

    interpret(get_function_def_subtree("main").body)

    assert symtable.get_variable("a").address == 0
    assert symtable["a"].value == 42
    assert symtable["b"].value == 0
    assert symtable["c"].value == 2
    assert symtable["d"].value == 4294967291


def test_structs(mocker, ast_from_file, memory, symtable):
    mocker.patch("regcheck.state.memory", memory)
    mocker.patch("regcheck.state.symtable", symtable)
    mocker.patch("regcheck.state.ast", ast_from_file("structs.c"), create=True)

    interpret(get_function_def_subtree("main").body)

    assert symtable.get_variable("a").address == 0
    assert symtable.get_variable("b").address == 4

    assert memory[0].value == 1
    assert memory[1].value == 0  # 256 overflowed to 0
    assert memory[2].value == 65534  # -2 underflowed to 65534

    assert memory[4].value == 7  # b.a
    assert symtable["c"].value == 6  # address of b.c (4 (base_address) + 1 (uint8_t) + 1 (uint8_t))
