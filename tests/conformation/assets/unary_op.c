/* faking stdint.h */
typedef int uint;
typedef int uint8_t;

int main() {
    int a = 1234;
    int b = &a;
    *b = 42;

    int c = 1;
    c++;

    uint d = -5;
}