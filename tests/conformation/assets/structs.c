/* faking stdint.h */
typedef int uint8_t;
typedef int uint16_t;

typedef struct {
    uint8_t a;
    uint8_t b;
    uint16_t c;
} x_t;

int main() {
    x_t a = {1, 256, -2};
    x_t b;
    b.a = 7;
    int c = &b.c;

}