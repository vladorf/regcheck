.PHONY: tests build

tests:
	pytest -v tests

build:
	python setup.py bdist_wheel
