## Regcheck

Tool for analysis of heap writes through interpretation of abstract syntax tree.

Source files are preprocessed by compiler, then parsed by [pycparser](https://github.com/eliben/pycparser) 
with help of [pycparserext](https://github.com/inducer/pycparserext).
Result is abstract syntax tree which is cached in OS temporary directory (as returned by `tempfile.gettempdir()` 
with filename `regcheck_<hash>`) unless `--no-cache` option is used. In the next step, tree will be interpreted
with aim to produce read / write logs to predefined addresses and capture final values of those addresses.
State is dumped to user defined filepaths (or stdout) in CSV format.

## Installation
Needs python >= 3.7 and C compiler for preprocessing
```bash
git clone https://gitlab.com/vladorf/regcheck.git
cd regcheck
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
```

Test run can be done using either of these two examples (assuming cloned repository is current working directory):
```bash
python3 -m regcheck BOARD_InitBootClocks --config examples/LPC845_Project/regcheck.ini --output-memory - --no-cache
python3 -m regcheck BOARD_InitBootClocks --config examples/MK22FN512xxx12_Project/regcheck.ini --output-memory - --no-cache
```

## Usage
Supported options can be printed using `--help`
```bash
python3 -m regcheck --help
```

Mandatory arguments are `--config` and name of function to be interpreted.
Config is ini format configuration file which is used to describe project (like the two in examples folder)

Results are two 
- final state of memory under `--output-memory`
- read / write logs under `--output-rw-logs`

Memory config can be filtered to print only registers which changed their addresses from initial state with `--diff-only`
There is also `--all-path` option which fill interpret both true and false path of condition (doesn't apply to ternary operator though).

As said before - ASTs are cached between runs and that can cause trouble (it is notable when changing configuration file).
Regeneration of new image can be forced with `--no-cache` option. 


## Config

```ini
# global variables
[params]
# can be defined in format <name>=<type>, <value>
SystemCoreClock=uint32_t, 12000000         
 
[files]
# directory with project, can be either relative or absolute
directory=examples/LPC845_Project
# files with defintions of functions called from interpreted one (and defintion of interpreted one too)
files=board/boards/clock_config.c, drivers/fsl_clock.c, board/boards/pin_mux.c, drivers/fsl_swm.c, custom.c
# xml file describing MCU, contains initial state of memory
device_file=LPC845.xml
# additional arguments to preprocessor
cpp_args=-DCPU_LPC845M301JBD48, -Iboard, -Idrivers, -Idevice, -ICMSIS
# custom path to preprocessor, either absolute, relative or name which can be found in $PATH 
gcc_path=gcc
```

## Graphical user interface
[regcheck-gui](https://gitlab.com/vladorf/regcheck-gui) is sibling repository
containing user interface for interaction with regcheck

## Functions defined in MCUs ROM
Some functions are not defined in C files like usual but in memory - memory is
dereferenced to function. For that we can define custom c file where function
name will be address in hexadecimal (no leading zeros allowed) prepended with `_`

Example can be found in [LPC845 custom.c](examples/LPC845_Project/custom.c)

## Regcheck project structure
+ **examples** - examples to test regcheck on
+ **regcheck** - regcheck implementation
+ **fake_libc_include** - mocked standart library, see [explanation](https://github.com/eliben/pycparser#32what-about-the-standard-c-library-headers)
+ **utils** - utility scripts for recheck
+ **requirements(-dev).txt** - project dependencies (production / development)
+ **setup.py** - package definition
+ **Dockerfile** - docker image build definition


## Docker
Regcheck is dockerized and can be built and run with following commands:
```bash
# Wheel package is built as part of first stage of build
docker build -t regcheck . 
docker run --rm -v "$(pwd)/examples/:/examples" regcheck --config /examples/LPC845_Project/regcheck.ini BOARD_InitBootPins --output-memory -
```


## Examples

+ Print read/write logs of memory access outside of local memory allocations
    ```python
    python3 -m regcheck --config examples/LPC845_Project/regcheck.ini BOARD_InitBootClocks --output-rw-logs -
    ```
+ Print read/write logs from all paths - in if statement both paths will be interpreted outputing memory doesn't make sense
as it will be influenced by branches that weren't intended to run
     ```python
    python3 -m regcheck --config examples/LPC845_Project/regcheck.ini BOARD_InitBootPins --output-rw-logs - --all-paths
    ```
    
+ Print only registers that ended up with different value then their reset value
     ```python
    python3 -m regcheck --config examples/LPC845_Project/regcheck.ini BOARD_InitBootClocks --output-memory - --diff-only
    ```
    
+ Print that to file memory.csv (`-` as argument outputs to stdout)
    ```python
    python3 -m regcheck --config examples/LPC845_Project/regcheck.ini BOARD_InitBootClocks --output-memory memory.csv --diff-only
    ```
    
## Tests
If you have already activated virtual environment (see `Installation`) you need to install development requirements. Then you can run tests using make utility
```bash
pip3 install -r requirements-dev.txt
make tests
```
