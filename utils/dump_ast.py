#!/usr/bin/env python3
"""
Copyright (c) 2020 Vladimír Užík

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or other materials provided
with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Converts C source files into pickled AST.
"""
import pickle
import argparse
import sys

from pycparser import parse_file
from pycparserext.ext_c_parser import GnuCParser


arg_parser = argparse.ArgumentParser(description="Utility for converting C source code into pickled AST")
output = arg_parser.add_mutually_exclusive_group(required=True)
output.add_argument("--out-txt", type=argparse.FileType("w"), default=False, help="name of output file, text")
output.add_argument("--out-binary", type=argparse.FileType("wb", 0), default=False, help="name of output file, binary")
arg_parser.add_argument("-f", "--files", nargs="+", metavar="ARG", default=[], required=True, help="input files")
arg_parser.add_argument("--gcc-args", nargs=argparse.REMAINDER, metavar="ARG", default=[],
                        help="args for GCC preprocessor (other than `-E`, thats hardcoded)")
args = arg_parser.parse_args()

ast = []
for file_ in args.files:
    ast.extend(
        parse_file(file_, use_cpp=True, cpp_path="gcc",
                   cpp_args=["-E", "-D__attribute__(x)=", "-D__extension__"] + args.gcc_args, parser=GnuCParser()).ext
    )


if args.out_txt:
    args.out_txt.write(repr(ast) + "\n")
elif args.out_binary:
    pickle.dump(ast, args.out_binary)
